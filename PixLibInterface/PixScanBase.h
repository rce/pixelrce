/*
 * PixScanBase.h
 *
 *  Created on: Jul 29, 2011
 * 
 * This PixLib class contains the scan configuration.
 * Keep this file in sync with PixLib 
 */


#ifndef PIXSCANBASE_H_
#define PIXSCANBASE_H_
#include <string>
#include <vector>
#include <assert.h>
#include <iostream>
#include "PixEnumBase.h"
#define MAX_GROUPS 4
#define MAX_SGROUPS 2
#define MAX_LOOPS 3
namespace PixLib {
  
  class PixScanBase : public EnumRunType,public  EnumScanParam ,public  EnumDspHistoCode,
    public  EnumHistoDepth ,public EnumEndLoopAction,public  EnumHistogramType,public  EnumMaskStageMode, public EnumMaskSteps,
    public  EnumMccBandwidth, public EnumModConfigType, public EnumVoltageMode, public EnumScanType,public EnumFEflavour
   {
public:
	PixScanBase():m_actualFE(-1) {
	}

	~PixScanBase() {};
 protected:
	enum ScanType m_scanTypeEnum;
	ModConfigType m_modConfig;
	bool m_modScanConcurrent;
	MaskStageMode m_maskStageMode;
	int m_maskStageSteps;
	bool m_tuningStartsFromConfigValues;
	int m_repetitions;
	bool m_selfTrigger;
	bool m_strobeLVL1DelayOverride;
	int m_strobeLVL1Delay;
	int m_LVL1Latency;
	int m_strobeDuration;
	int m_strobeMCCDelay;
	int m_strobeMCCDelayRange;
	unsigned int m_moduleMask[MAX_GROUPS];
	bool m_configEnabled[MAX_GROUPS];
	bool m_triggerEnabled[MAX_GROUPS];
	bool m_strobeEnabled[MAX_GROUPS];
	bool m_readoutEnabled[MAX_GROUPS];
	int m_superGroupTrigDelay;
	int m_consecutiveLvl1TrigA[MAX_SGROUPS];
	int m_consecutiveLvl1TrigB[MAX_SGROUPS];
	int m_trigABDelay[MAX_SGROUPS];
	bool m_lvl1HistoBinned;
	bool m_histogramFilled[MAX_HISTO_TYPES];
	bool m_histogramKept[MAX_HISTO_TYPES];
	DspHistoCode m_dspHistogrammingCode;
	DspHistoDepth m_dspHistoDepth;
	MccBandwidth m_mccBandwidth;
	int m_histoTotMin;
	int m_histoTotMax;
	int m_histoTotNbin;
	bool m_digitalInjection;
	bool m_chargeInjCapHigh;
	int m_columnROFreq;
	int m_feVCal;
	int m_feHitbus;
	int m_totThrMode;
	int m_totMin;
	int m_totDHThr;
	int m_totTimeStampMode;
	VoltageMode m_biasMode;
	bool m_saveModulesConfig;
	bool m_saveModGroupConfig;
	int m_bocMode;
	int m_TxLaserCurrentBase;
	int m_TxLaserCurrentProbe;
	bool m_cloneCfgTag;
	bool m_cloneModCfgTag;
	std::string m_tagSuffix;
	
	float m_biasVoltage;
	VoltageMode m_analogMode;
	float m_analogVoltage;
	VoltageMode m_digitalMode;
	float m_digitalVoltage; 
	bool m_restoreModuleConfig;
	RunType m_runType;
	bool m_loopActive[MAX_LOOPS];
	ScanParam m_loopParam[MAX_LOOPS];
	bool m_dspProcessing[MAX_LOOPS];
	bool m_dspFEbyFE;
	bool m_dspMaskStaging;
	bool m_innerLoopSwap;
	std::vector<float> m_loopVarValues[MAX_LOOPS];
	int m_loopVarNSteps[MAX_LOOPS];
	int m_maskLoop;
	float m_loopVarMin[MAX_LOOPS];
	float m_loopVarMax[MAX_LOOPS];
	float m_loopVarOrig[MAX_LOOPS];
	bool m_loopVarUniform[MAX_LOOPS];
	bool m_loopVarValuesFree[MAX_LOOPS];
	EndLoopAction m_loopAction[MAX_LOOPS];
	bool m_dspLoopAction[MAX_LOOPS];
	MaskStageSteps m_maskStageTotalSteps;
	// Scan execution
	int m_loopIndex[MAX_LOOPS];
	int m_maskStageIndex;
	bool m_newMaskStage;
	bool m_newScanStep;
	bool m_newFE;
	bool m_loopTerminating[MAX_LOOPS];
	bool m_loopEnded[MAX_LOOPS];
	bool m_FEbyFE;
	int m_FECounter;
	int m_actualFE;
	unsigned int m_FEbyFEMask;
	// Scan specific parameters
	int m_thresholdTargetValue;
	int m_dynamicThresholdTargetValue;
	int m_totTargetValue;
	int m_totTargetCharge;
	unsigned int m_useAltModLinks;
	unsigned int m_bocScanDecodeMode;
	unsigned int m_bocScanPattern;
	unsigned int m_bocScanNHits;
	std::string m_swapInLinks;
	std::string m_swapOutLinks;
	unsigned int m_modScurveFit;
	bool m_histoOverwrite;
      
	//! Scan attributes
 public:

	void setScanTypeEnum(ScanType scanType){
	  m_scanTypeEnum=scanType;
	}

	ScanType getScanType(){
	  return m_scanTypeEnum;
	}

	ModConfigType getModConfig() {
	  return m_modConfig;
	};
	void setModConfig(ModConfigType cfgTyp) {
	  m_modConfig = cfgTyp;
	};
	bool getModScanConcurrent() {
	  return m_modScanConcurrent;
	};
	void setModScanConcurrent(bool concurr) {
	  m_modScanConcurrent = concurr;
	};
	MaskStageMode getMaskStageMode() {
	  return m_maskStageMode;
	};
	void setMaskStageMode(MaskStageMode stageMode) {
	  m_maskStageMode = stageMode;
	};
	MaskStageSteps getMaskStageTotalSteps() {
	  return m_maskStageTotalSteps;
	};
	void setMaskStageTotalSteps(MaskStageSteps nSteps) {
	  m_maskStageTotalSteps = nSteps;
	};
	int getMaskStageSteps() {
	  return m_maskStageSteps;
	};
	void setMaskStageSteps(int nSteps) {
	  m_maskStageSteps = nSteps;
	};
	bool tuningStartsFromCfgValues() {
	  return m_tuningStartsFromConfigValues;
	};
	void tuningStartsFromCfgValues(bool isFromCfg) {
	  m_tuningStartsFromConfigValues = isFromCfg;
	};
	int getRepetitions() {
	  return m_repetitions;
	};
	void setRepetitions(int nRep) {
	  m_repetitions = nRep;
	};
	bool getSelfTrigger() {
	  return m_selfTrigger;
	};
	void setSelfTrigger(bool self) {
	  m_selfTrigger = self;
	};
	bool getStrobeLVL1DelayOverride() {
	  return m_strobeLVL1DelayOverride;
	};
	void setStrobeLVL1DelayOveride(bool lvl1StrDelayOverride) {
	  m_strobeLVL1DelayOverride = lvl1StrDelayOverride;
	};
	int getStrobeLVL1Delay() {
	  return m_strobeLVL1Delay;
	};
	void setStrobeLVL1Delay(int lvl1StrDelay) {
	  m_strobeLVL1Delay = lvl1StrDelay;
	};
	int getLVL1Latency() {
	  return m_LVL1Latency;
	};
	void setLVL1Latency(int lvl1Latency) {
	  m_LVL1Latency = lvl1Latency;
	};
	int getStrobeMCCDelay() {
	  return m_strobeMCCDelay;
	};
	void setStrobeMCCDelay(int delay) {
	  m_strobeMCCDelay = delay;
	};
	int getStrobeMCCDelayRange() {
	  return m_strobeMCCDelayRange;
	};
	void setStrobeMCCDelayRange(int delayRange) {
	  m_strobeMCCDelayRange = delayRange;
	};
	int getStrobeDuration() {
	  return m_strobeDuration;
    };
    void setStrobeDuration(int strobeDuration) {
      m_strobeDuration = strobeDuration;
    };
    unsigned int getModuleMask(int group) {
      if (group >=0 && group < MAX_GROUPS) {
	return m_moduleMask[group];
      }
      return 0;
    };
    void setModuleMask(int group, unsigned int mask) {
      if (group >=0 && group < MAX_GROUPS) {
	m_moduleMask[group] = mask;
      }
    }; 
    bool getConfigEnabled(int group) {
      if (group >=0 && group < MAX_GROUPS) {
	return m_configEnabled[group];
      }
      return false;
    };
    void setConfigEnabled(int group, bool ena) {
      if (group >=0 && group < MAX_GROUPS) {
	m_configEnabled[group] = ena;
      }
    };
    bool getTriggerEnabled(int group) {
      if (group >=0 && group < MAX_GROUPS) {
	return m_triggerEnabled[group];
      }
      return false;
    };
    void setTriggerEnabled(int group, bool ena) {
      if (group >=0 && group < MAX_GROUPS) {
	m_triggerEnabled[group] = ena;
      }
    };
    bool getStrobeEnabled(int group) {
      if (group >=0 && group < MAX_GROUPS) {
	return m_strobeEnabled[group];
      }
      return false;
    };
    void setStrobeEnabled(int group, bool ena) {
      if (group >=0 && group < MAX_GROUPS) {
	m_strobeEnabled[group] = ena;
      }
    };
    bool getReadoutEnabled(int group) {
      if (group >=0 && group < MAX_GROUPS) {
	return m_readoutEnabled[group];
      }
      return false;
    };
    void setReadoutEnabled(int group, bool ena) {
      if (group >=0 && group < MAX_GROUPS) {
	m_readoutEnabled[group] = ena;
      }
    };
    int getSuperGroupTrigDelay() {
      return m_superGroupTrigDelay;
    };
    void setSuperGroupTrigDelay(int delay) {
      m_superGroupTrigDelay = delay;
    };
    int getConsecutiveLvl1TrigA(int superGroup) {
      if (superGroup >=0 && superGroup < MAX_SGROUPS) {
	return m_consecutiveLvl1TrigA[superGroup];
      }
      return 0;
    };
    void setConsecutiveLvl1TrigA(int superGroup, int nLvl1) {
      if (superGroup >=0 && superGroup < MAX_SGROUPS) {
	m_consecutiveLvl1TrigA[superGroup] = nLvl1;
      }
    };
    int getConsecutiveLvl1TrigB(int superGroup) {
      if (superGroup >=0 && superGroup < MAX_SGROUPS) {
	return m_consecutiveLvl1TrigB[superGroup];
      }
      return 0;
    };
    void setConsecutiveLvl1TrigB(int superGroup, int nLvl1) {
      if (superGroup >=0 && superGroup < MAX_SGROUPS) {
	m_consecutiveLvl1TrigB[superGroup] = nLvl1;
      }
    }; 
    bool getLvl1HistoBinned() {
      return m_lvl1HistoBinned;
    };
    void setLvl1HistoBinned(bool bin) {
      m_lvl1HistoBinned = bin;
    };
    int getTrigABDelay(int superGroup) {
      if (superGroup >=0 && superGroup < MAX_SGROUPS) {
	return m_trigABDelay[superGroup];
      }
      return 0;
    };
    void setTrigABDelay(int superGroup, int delay) {
      if (superGroup >=0 && superGroup < MAX_SGROUPS) {
	m_trigABDelay[superGroup] = delay;
      }
    };
    DspHistoCode getDspHistogrammingCode() {
      return m_dspHistogrammingCode;
    };
    void setDspHistogrammingCode(DspHistoCode code) {
      m_dspHistogrammingCode = code;
    };
    DspHistoDepth getDspHistoDepth() {
      return m_dspHistoDepth;
    };
    void setDspHistoDepth(DspHistoDepth dep) {
      m_dspHistoDepth = dep;
    };
    MccBandwidth getMccBandwidth() {
      return m_mccBandwidth;
    };
    void setMccBandwidth(MccBandwidth bw) {
      m_mccBandwidth = bw;
    };
    bool getHistogramFilled(HistogramType type) {
      return m_histogramFilled[type];
    };
    void setHistogramFilled(HistogramType type, bool fill) {
      m_histogramFilled[type] = fill;
    };
    bool getHistogramKept(HistogramType type) {
      return m_histogramKept[type];
    };
    void setHistogramKept(HistogramType type, bool keep) {
      m_histogramKept[type] = keep;
    };
    int getHistoTotMin() {
      return m_histoTotMin;
    };
    void setHistoTobtMin(int totMin) {
      m_histoTotMin = totMin;
    };
    int getHistoTotMax() {
      return m_histoTotMax;
    };
    void setHistoTotMax(int totMax) {
      m_histoTotMax = totMax;
    };
    int getHistoTotNbin() {
      return m_histoTotNbin;
    };
    void setHistoTotNbin(int totNbin) {
      m_histoTotNbin = totNbin;
    };
    bool getDigitalInjection() {
      return m_digitalInjection;
    };
    void setDigitalInjection(bool digiInj) {
      m_digitalInjection = digiInj;
    };
    bool getChargeInjCapHigh() {
      return m_chargeInjCapHigh;
    };
    void setChargeInjCapHigh(bool capHigh) {
      m_chargeInjCapHigh = capHigh;
    };
    int getColumnROFreq() {
      return m_columnROFreq;
    };
    void setColumnROFreq(int colROFreq) {
      m_columnROFreq = colROFreq;
    };
    int getFeVCal() {
      return m_feVCal;
    };
    void setFeVCal(int vcal) {
      m_feVCal = vcal;
    };
    int getFeHitbus() {
      return m_feHitbus;
    };
    void setFeHitbus(int hitbus) {
      m_feHitbus = hitbus;
    };
    int getTotThrMode() {
      return m_totThrMode;
    };
    void setTotThrMode(int totMode) {
      m_totThrMode = totMode;
    };
    int getTotMin() {
      return m_totMin;
    };
    void setTotMin(int totMin) {
      m_totMin = totMin;
    };
    int getTotDHThr() {
      return m_totDHThr;
    };
    void setTotDHThr(int totDHThr) {
      m_totDHThr = totDHThr;
    };
    int getTotTimeStampMode() {
      return m_totTimeStampMode;
    };
    void setTotTimeStampMode(int totMode) {
      m_totTimeStampMode = totMode;
    };
    VoltageMode getBiasMode() {
      return m_biasMode;
    };
    void setBiasMode(VoltageMode mode) {
      m_biasMode = mode;
    };
    double getBiasVoltage() {
      return m_biasVoltage;
    };
    void setBiasVoltage(double voltage) {
      m_biasVoltage = voltage;
    };
    VoltageMode getAnalogMode() {
      return m_analogMode;
    };
    void setAnalogMode(VoltageMode mode) {
      m_analogMode = mode;
    };
    double getAnalogVoltage() {
      return m_analogVoltage;
    };
    void setAnalogVoltage(double voltage) {
      m_analogVoltage = voltage;
    };
    VoltageMode getDigitalMode() {
      return m_digitalMode;
    };
    void setDigitalMode(VoltageMode mode) {
      m_digitalMode = mode;
    };
    double getDigitalVoltage() {
      return m_digitalVoltage;
    };
    void setDigitalVoltage(double voltage) {
      m_digitalVoltage = voltage;
    };
   bool getRestoreModuleConfig() {
      return m_restoreModuleConfig;
    }
    void setRestoreModuleConfig(bool restore) {
      m_restoreModuleConfig = restore;
    }
    RunType getRunType() {
    return m_runType;
    }
    void setRunType(RunType run) {
      m_runType = run;
    }
    std::vector<unsigned int>& getPatternSeeds() {
      return m_patternSeeds;
    };
    void setPatternSeeds(std::vector<unsigned int> values) {
      m_patternSeeds = values;
    };
    bool getSaveModulesConfig() {
      return m_saveModulesConfig;
    }
    void setSaveModulesConfig(bool mc) {
      m_saveModulesConfig = mc;
    }
    bool getSaveModGroupConfig() {
      return m_saveModGroupConfig;
    }
    void setSaveModGroupConfig(bool mc) {
      m_saveModGroupConfig = mc;
    }
    int getBocMode() {
      return m_bocMode;
    }
    void setBocMode(int bm) {
      m_bocMode = bm;
    }
    int getTxLaserCurrentBase(){
      return m_TxLaserCurrentBase;
    }
    void setTxLaserCurrentBase(int txCurr){
      m_TxLaserCurrentBase = txCurr;
    } 
    int getTxLaserCurrentProbe(){
      return m_TxLaserCurrentProbe;
    }
    void setTxLaserCurrentProbe(int txCurr){
      m_TxLaserCurrentProbe = txCurr;
    }
    bool getCloneCfgTag() {
      return m_cloneCfgTag;
    }
    bool getCloneModCfgTag() {
      return m_cloneModCfgTag;
    }
    std::string getTagSuffix() {
      return m_tagSuffix;
    }
    void setTagSuffix(std::string suff){
      m_tagSuffix = suff;
    }
    //! Loop attributes
    bool getLoopActive(int index) {
      if (index >=0 && index < MAX_LOOPS) {
	return m_loopActive[index];
      }
      return false;
    };
    void setLoopActive(int index, bool active) {
      if (index >=0 && index < MAX_LOOPS) {
	m_loopActive[index] = active;
      }
    };
    ScanParam getLoopParam(int index) {
      if (index >=0 && index < MAX_LOOPS) {
	return m_loopParam[index];
      }
      return NO_PAR;
    };
    void setLoopParam(int index, ScanParam par) {
      if (index >=0 && index < MAX_LOOPS) {
	m_loopParam[index] = par;
      }
    };
    void setMaskLoop(int maskloop) {
      m_maskLoop = maskloop;
    };
    int getMaskLoop() {
      return m_maskLoop;
    } 
    bool getDspProcessing(int index) {
      if (index >=0 && index < MAX_LOOPS) {
	return m_dspProcessing[index];
      }
      return false;
    };
    void setDspProcessing(int index, bool dsp) {
      if (index >=0 && index < MAX_LOOPS) {
	m_dspProcessing[index] = dsp;
      }
    };
    bool getDspMaskStaging() {
      return m_dspMaskStaging;
    };
    void setDspMaskStaging(bool dsp) { 
      m_dspMaskStaging = dsp; 
    };
    bool getInnerLoopSwap() {
      return m_innerLoopSwap;
    };
    void setInnerLoopSwap(bool swp) {
      m_innerLoopSwap = swp;
    };
    std::vector<float>& getLoopVarValues(int index) {
      static std::vector<float> tmp;
      if (index >=0 && index < MAX_LOOPS) {
	return m_loopVarValues[index];
      }
      //      throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");
      assert(0);
      return tmp;
    }; 
    bool preset(ScanType presetName, FEflavour feFlavour);
    bool presetRCE(ScanType presetName, FEflavour feFlavour);
    

    void setLoopVarValues(int index, double startVal, double endVal, int nSteps) {
      if (index >=0 && index < MAX_LOOPS) {
	m_loopVarNSteps[index] = nSteps;
	m_loopVarMin[index] = startVal;
	m_loopVarMax[index] = endVal;
	m_loopVarValues[index].clear();
	double v = startVal;
	double incr = (endVal - startVal)/(nSteps -1);
	for (int iv=0; iv<nSteps; iv++) {
	  m_loopVarValues[index].push_back(v);
	  v += incr;
	}
	m_loopVarUniform[index] = true;
	m_loopVarValuesFree[index] = false;
      } else {
	//	throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");        
	assert(0);
      }
    };
    void setLoopVarValues(int index, std::vector<float> values) {
      if (index >=0 && index < MAX_LOOPS) {
	m_loopVarValues[index] = values;
	m_loopVarNSteps[index] = values.size();
	m_loopVarMin[index] = m_loopVarMax[index] = values[0];
	for (unsigned int i=0; i<values.size(); i++) {
	  if (values[i] < m_loopVarMin[index]) m_loopVarMin[index] = values[i];
	  if (values[i] > m_loopVarMax[index]) m_loopVarMax[index] = values[i];
	}
	m_loopVarUniform[index] = false;
	m_loopVarValuesFree[index] = false;
      } else {
	//	throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");       
	assert(0);
      }
    };
    int getLoopVarNSteps(int index) {
      if (index >=0 && index < MAX_LOOPS) {
	return m_loopVarNSteps[index];
      }
      //      throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");
      assert(0);
      return 0;
    }
    float getLoopVarMin(int index) {
      if (index >=0 && index < MAX_LOOPS) {
	return m_loopVarMin[index];
      }
      //      throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");
      assert(0);
      return 0;
    }
    float getLoopVarMax(int index) {
    if (index >=0 && index < MAX_LOOPS) {
      return m_loopVarMax[index];
    }
    //    throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");
    assert(0);
    return 0;
    }
    void setLoopVarOrig(int index, float v) {
      if (index >=0 && index < MAX_LOOPS) {
	m_loopVarOrig[index] = v;
      } else {
	//	throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");
	assert(0);
      }
    }
    float getLoopVarOrig(int index) {
      if (index >=0 && index < MAX_LOOPS) {
	return m_loopVarOrig[index];
      }
      //      throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");
      assert(0);
      return 0;
    }
    bool getLoopVarValuesFree(int index) {
      if (index >=0 && index < MAX_LOOPS) {
	return m_loopVarValuesFree[index];
      }
      //      throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");
      assert(0);
      return false;
    };
    void setLoopVarValuesFree(int index) {
      if (index >=0 && index < MAX_LOOPS) {
	m_loopVarValuesFree[index] = true;
      } else
	//	throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");
	assert(0);
    };
    bool getLoopVarUniform(int index) {
      if (index >=0 && index < MAX_LOOPS) {
	return m_loopVarUniform[index];
      }
      //      throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");
      assert(0);
      return false;
    };
    EndLoopAction getLoopAction(int index) {
      if (index >=0 && index < MAX_LOOPS) {
	return m_loopAction[index];
      }
      //      throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");
      assert(0);
      return NO_ACTION;
    };
    void setLoopAction(int index, EndLoopAction action) {
      if (index >=0 && index < MAX_LOOPS) {
	m_loopAction[index] = action;
      } else
	//	throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");
	assert(0);
    };
    bool getDspLoopAction(int index) {
      if (index >=0 && index < MAX_LOOPS) {
	return m_dspLoopAction[index];
      }
      //      throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");
      assert(0);
      return false;
    };
    void setDspLoopAction(int index, bool dsp) {
      if (index >=0 && index < MAX_LOOPS) {
	m_dspLoopAction[index] = dsp;
      } else
	//	throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");
	assert(0);
    };
    unsigned int getBocScanPattern(){
      return m_bocScanPattern;
    }
    void setBocScanPattern(unsigned int iv){
      m_bocScanPattern=iv;
    }
    unsigned int getbocScanNHits(){
      return m_bocScanNHits;
    }
    void setbocScanNHits(unsigned int iv){
      m_bocScanNHits=iv;
    }
       // Scan specific parameters
    void setThresholdTargetValue(int val) {
      m_thresholdTargetValue = val;
    };
    int getThresholdTargetValue() {
      return m_thresholdTargetValue;
    };
    void setDynamicThresholdTargetValue(int val) {
      m_dynamicThresholdTargetValue = val;
    };
    int getDynamicThresholdTargetValue() {
      return m_dynamicThresholdTargetValue;
    };
    void setTotTargetValue(int val) {
      m_totTargetValue = val;
    };
    int getTotTargetValue() {
      return m_totTargetValue;
    };
    void setTotTargetCharge(int val) {
      m_totTargetCharge = val;
    };
    int getTotTargetCharge() {
      return m_totTargetCharge;
    };
    unsigned int getUseAltModLinks() { 
      return m_useAltModLinks; 
    };
    void setUseAltModLinks(unsigned int alt) { 
      m_useAltModLinks = alt; 
    };
    unsigned int getBocScanDecodeMode() { 
      return m_bocScanDecodeMode; 
    };
    void setBocScanDecodeMode(unsigned int mode) { 
      m_bocScanDecodeMode = mode; 
    };
    std::string getSwapInLinks() { 
      return m_swapInLinks; 
    };
    void setSwapInLinks(std::string swin) { 
      m_swapInLinks = swin; 
    };
    std::string getSwapOutLinks() { 
      return m_swapOutLinks; 
    };
    void setSwapOutLinks(std::string swout) { 
      m_swapOutLinks = swout; 
    };
    void setFE(int val) {
      m_actualFE = val;
    };
    int getFE() { 
      return m_actualFE; 
    };
  
    std::vector<unsigned int> m_patternSeeds;
    bool newMaskStep() {
      return m_newMaskStage;
    }
    bool newScanStep() {
      return m_newScanStep;
    }
    bool newFE() {
      return m_newFE;
    }
    void setFEbyFE(bool val) {
      m_FEbyFE = val;
    };
    bool getFEbyFE()  {
      return m_FEbyFE;
    };
    void setFEbyFEMask(unsigned int feMask) {   
      m_FEbyFEMask = feMask;
    };  
    unsigned int getFEbyFEMask() {
      return m_FEbyFEMask;
    };
    void setFECounter(int val) {
      m_FECounter = val;
    };
    int getFECounter() {
      return m_FECounter;
    };
    // include presets
    /* return false if scan does not exist for a certain FE flavour */



   };
}

#endif /* PIXSCANBASE_H_ */

