#include "eudaq/Event.hh"
#include "eudaq/DetectorEvent.hh"
#include "eudaq/RawDataEvent.hh"
#include "eudaq/FileSerializer.hh"
#include "eudaq/counted_ptr.hh"


int main(){
  char data[]="bla";
  eudaq::DetectorEvent dev(100,1,0);
  eudaq::RawDataEvent* rev=new eudaq::RawDataEvent("CTEL",100,1);
  counted_ptr<eudaq::Event> cp(rev);
  rev->AddBlock(0,data,3);
  dev.AddEvent(cp);
  dev.Print(std::cout);
  eudaq::FileSerializer fs("test.out");
  dev.Serialize(fs);
  
}
