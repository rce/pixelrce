#ifndef CONFIGUREMODULESNEACTION_HH 
#define CONFIGUREMODULESNEACTION_HH 

#include "scanctrl/LoopAction.hh"
#include "config/ConfigIF.hh"

class ConfigureModulesNEAction: public LoopAction{
public:
  ConfigureModulesNEAction(std::string name, ConfigIF* cif):
    LoopAction(name),m_configIF(cif){}
  int execute(int i){
    m_configIF->configureModulesHW(false);
    return 0;
  }
private:
  ConfigIF* m_configIF;
};

#endif
