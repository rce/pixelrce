#ifndef ENABLETRIGGERACTION_HH 
#define ENABLETRIGGERACTION_HH 

#include "scanctrl/LoopAction.hh"
#include "config/ConfigIF.hh"

class EnableTriggerAction: public LoopAction{
public:
  EnableTriggerAction(std::string name, ConfigIF* cif):
    LoopAction(name),m_configIF(cif){}
  int execute(int i){
    m_configIF->sendHWcommand(17); // tell HSIO trigger logic that this RCE is in control.
    m_configIF->enableTrigger();
    return 0;
  }
private:
  ConfigIF* m_configIF;
};

#endif
