#ifndef RCFRCECALLBACK_HH
#define RCFRCECALLBACK_HH

#include <RCF/RCF.hpp>
#include "scanctrl/RceCallback.hh"

class RCFRceCallback: public RceCallback{
public:
  RCFRceCallback(RCF::RcfServer &server);
  ~RCFRceCallback();
  static void onCallbackConnectionCreated(RCF::RcfSessionPtr sessionPtr,
					  RCF::ClientTransportAutoPtr clientTransportPtr);
  void SendMsg(ipc::Priority pr, const ipc::CallbackParams *msg);
  void RCFConfigureCallback(ipc::Priority pr);
  void Shutdown();
private:
  RCF::RcfServer& m_server;
  static RcfClient<I_RCFCallback>* m_cb;
  
};

#endif
