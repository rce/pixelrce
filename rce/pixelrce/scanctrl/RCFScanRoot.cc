
#include "scanctrl/RCFScanRoot.hh"
#include <boost/property_tree/ptree.hpp>
#include "dataproc/AbsDataProc.hh"
#include "dataproc/AbsReceiver.hh"
#include "config/ConfigIF.hh"
#include "scanctrl/Scan.hh"
#include "util/HistoManager.hh"
#include "scanctrl/RCFRceCallback.hh"

#include <string>
#include <iostream>


RCFScanRoot::RCFScanRoot(RCF::RcfServer &server, ConfigIF* cif, Scan* scan): 
  ScanRoot(cif,scan), m_cb(new RCFRceCallback(server)){
}

RCFScanRoot::~RCFScanRoot(){
}
  
int32_t RCFScanRoot::RCFconfigureScan(ipc::ScanOptions options){
  //PixScan::dump(options);
  //std::cout<<"Start IPCScanRoot "<<std::endl;
  //convert struct into property tree
  boost::property_tree::ptree *pt=new boost::property_tree::ptree;
  //Top level 
  pt->put("Name",options.name); // scan name. Can be used as a file name, for example.
  pt->put("scanType",options.scanType); // scan type. Determines what loop setup to use
  pt->put("Receiver",options.receiver); 
  pt->put("DataHandler",options.dataHandler);
  pt->put("DataProc",options.dataProc);
  pt->put("Timeout.Seconds",options.timeout.seconds);
  pt->put("Timeout.Nanoseconds",options.timeout.nanoseconds);
  pt->put("Timeout.AllowedTimeouts",options.timeout.allowedTimeouts);
  pt->put("runNumber", options.runNumber);
  pt->put("stagingMode",options.stagingMode);
  //maskStages is not used in NewDsp, only nMaskStages
  pt->put("maskStages", options.maskStages);
  pt->put("nMaskStages",options.nMaskStages);
  pt->put("firstStage",options.firstStage);
  pt->put("stepStage",options.stepStage);
  pt->put("nLoops",options.nLoops);
  //Loops
  for(int i=0;i<options.nLoops;i++){
    char loopname[128];
    sprintf (loopname,"scanLoop_%d.",i);
    pt->put(std::string(loopname)+"scanParameter", options.scanLoop[i].scanParameter);
    pt->put(std::string(loopname)+"nPoints",options.scanLoop[i].nPoints);
    pt->put(std::string(loopname)+"endofLoopAction.Action", options.scanLoop[i].endofLoopAction.Action);
    pt->put(std::string(loopname)+"endofLoopAction.fitFunction", options.scanLoop[i].endofLoopAction.fitFunction);
    pt->put(std::string(loopname)+"endofLoopAction.targetThreshold", options.scanLoop[i].endofLoopAction.targetThreshold);
    if(options.scanLoop[i].dataPoints.size()>0){
      char pointname[10];
      for(unsigned int j=0;j<options.scanLoop[i].dataPoints.size();j++){
	sprintf(pointname,"P_%d",j);
	pt->put(std::string(loopname)+"dataPoints."+pointname,options.scanLoop[i].dataPoints[j]);
      }
    }
  }
  //Trigger options
  pt->put("trigOpt.nEvents",options.trigOpt.nEvents);
  pt->put("trigOpt.triggerMask",options.trigOpt.triggerMask);
  pt->put("trigOpt.moduleTrgMask",options.trigOpt.moduleTrgMask);
  pt->put("trigOpt.triggerDataOn",options.trigOpt.triggerDataOn);
  pt->put("trigOpt.deadtime",options.trigOpt.deadtime);
  pt->put("trigOpt.nL1AperEvent",options.trigOpt.nL1AperEvent);
  pt->put("trigOpt.nTriggersPerGroup",options.trigOpt.nTriggersPerGroup);
  pt->put("trigOpt.nTriggers",options.trigOpt.nTriggers);
  pt->put("trigOpt.Lvl1_Latency",options.trigOpt.Lvl1_Latency);
  pt->put("trigOpt.Lvl1_Latency_Secondary",options.trigOpt.Lvl1_Latency_Secondary);
  pt->put("trigOpt.strobeDuration",options.trigOpt.strobeDuration);
  pt->put("trigOpt.strobeMCCDelay",options.trigOpt.strobeMCCDelay);
  pt->put("trigOpt.strobeMCCDelayRange",options.trigOpt.strobeMCCDelayRange);
  pt->put("trigOpt.CalL1ADelay",options.trigOpt.CalL1ADelay);
  pt->put("trigOpt.eventInterval",options.trigOpt.eventInterval);
  pt->put("trigOpt.injectForTrigger",options.trigOpt.injectForTrigger);
  pt->put("trigOpt.vcal_charge",options.trigOpt.vcal_charge);
  pt->put("trigOpt.threshold",options.trigOpt.threshold);
  pt->put("trigOpt.hitbusConfig",options.trigOpt.hitbusConfig);

  // each option gets an entry in the tree below optionsMask with value 1
  for (unsigned int i=0;i<options.trigOpt.optionsMask.size();i++){
    pt->put(std::string("trigOpt.optionsMask.")+(const char*)options.trigOpt.optionsMask[i].c_str(),1);
  }
  pt->put("trigOpt.triggerMode", options.trigOpt.triggerMode);

  // front-end options
  pt->put("feOpt.phi",options.feOpt.phi);
  pt->put("feOpt.totThresholdMode",options.feOpt.totThresholdMode);
  pt->put("feOpt.totMinimum",options.feOpt.totMinimum);
  pt->put("feOpt.totTwalk",options.feOpt.totTwalk);
  pt->put("feOpt.totLeMode",options.feOpt.totLeMode);
  pt->put("feOpt.hitbus",options.feOpt.hitbus);

  pt->put("nHistoNames",options.histos.size()); 
  if(options.histos.size()>0){
    char pointname[10];
    for(unsigned int j=0;j<options.histos.size();j++){
      sprintf(pointname,"Name_%d",j);
      pt->put(std::string("HistoNames.")+pointname,options.histos[j].c_str());
    }
  }
  //Trigger options
  //call the real initScan function
  //std::cout<<"Calling ScanRoot "<<std::endl;
  configureScan(pt);
  delete pt;
  return 0;
}

std::vector<std::string> RCFScanRoot::RCFgetHistoNames(std::string reg){
  std::vector<std::string> strvec=HistoManager::instance()->getHistoNames(reg.c_str());
  return strvec;
}
std::vector<std::string> RCFScanRoot::RCFgetPublishedHistoNames(){
  std::vector<std::string> strvec=HistoManager::instance()->getPublishedHistoNames();
  return strvec;
}

uint32_t RCFScanRoot::RCFnEvents(){
  if(m_dataProc)
    return m_dataProc->nEvents();
  else
    return 0;
}
void RCFScanRoot::RCFresynch(){
  if(m_receiver)
    m_receiver->resynch();
}
void RCFScanRoot::RCFsetRceNumber(int32_t rce){
  char name[32];
  sprintf(name, "%d", rce);
  setenv("RCE_NAME", name, false);
}

void RCFScanRoot::RCFconfigureCallback(ipc::Priority pr){
  m_cb->RCFConfigureCallback(pr);
}


