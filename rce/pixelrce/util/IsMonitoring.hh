// 
// IS monitoring
// 
// Martin Kocian, SLAC, 2/11/2016
//

#ifndef RCEISMONITORING_HH
#define RCEISMONITORING_HH

#include "util/Monitoring.hh"
#include <vector>
class ISInfoDictionary;
class IPCPartition;
class RceMonitoring;

class IsMonitoring: public Monitoring{ 
public:
  IsMonitoring(IPCPartition &p, const char* servername, int rce);
  virtual ~IsMonitoring();
  enum fenum{MAXFE=16};
  virtual void Publish();
  virtual void Reset();
  virtual void SetOccupancy(int fe, float occ);
  virtual void SetAverageOccupancy(float occ);
  virtual void SetNumberOfEvents(unsigned nevt);
  virtual void SetNumberOfCleanEvents(unsigned nevt);
  virtual void SetNMissed(unsigned nevt);
  virtual void SetDisabledMask(unsigned nevt);
  virtual void SetDisabledInRunMask(unsigned nevt);
  virtual void SetDisabledPermMask(unsigned nevt);
  virtual void SetTtcBusyCounter(unsigned nevt);
  virtual void SetErrorCounter(int fe, unsigned nevt, FWRegisters::EFBCOUNTER counter);
  virtual unsigned GetErrorCounter(int fe, FWRegisters::EFBCOUNTER counter);
  virtual void SetTtcClashCounter(unsigned nevt, FWRegisters::CLASH counter);
  virtual void SetTimingScanInfo(bool enabled, bool running, int step, int nstep);
  virtual void SetHptdcInfo(int hptdc, int reg, int val);
  virtual int GetRce(){return m_rce;}
protected:
  ISInfoDictionary* m_dict;
  RceMonitoring* m_mon;
  char m_monname[128];
  int m_rce;
};
    
#endif
