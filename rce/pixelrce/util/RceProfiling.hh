#ifndef __RCE_PROFILING_HH__
#define __RCE_PROFILING_HH__
#include <iostream>
#include <map>
#include <string>

namespace _profile_ {
void log(const std::string &name,uint64_t clk_ticks,uint64_t bytes_in,uint64_t bytes_out);
void print();
void clear();
}


#ifdef ENABLE_PROFILING
#warning "profiling is enabled"
#define PROFILE_START() auto _profile_start_ = std::chrono::high_resolution_clock::now() 
#define PROFILE_END(name,bytes_in,bytes_out) auto _profile_end_ = std::chrono::high_resolution_clock::now();uint64_t _profile_diff_= (_profile_end_-_profile_start_).count() ;_profile_::log(name,_profile_diff_,bytes_in,bytes_out)
#define PROFILE_PRINT() _profile_::print()
#define PROFILE_CLEAR() _profile_::clear()
#else
#define PROFILE_START()
#define PROFILE_END(name,bytes_in,bytes_out)
#define PROFILE_PRINT()
#define PROFILE_CLEAR()
#endif

#endif
