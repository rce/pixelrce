#ifndef RCE_HISTO1D_HH
#define RCE_HISTO1D_HH

#include "util/AbsRceHisto.hh"

class Provider;


template<typename T, typename TE> class RceHisto1d: public AbsRceHisto{
public:
  RceHisto1d(std::string name, std::string title, unsigned int nbin, float xmin, float xmax, bool hasErrors=false);    //! Constructor 1D
  RceHisto1d(const RceHisto1d<T, TE> &h);                                                                      //! Copy contructor
  virtual ~RceHisto1d();
  RceHisto1d &operator=(const RceHisto1d<T, TE>&h);
  T operator()(unsigned i) const;
  TE getBinError(unsigned i) const;
  inline void setBinError(unsigned int i, TE val);
  inline void setBinErrorFast(unsigned int i, TE val);
  inline void set(unsigned int i, T val);                 //! Bin write access 1d
  inline void setFast(unsigned int i, T val);                 //! Bin write access 1d
  inline void fill(unsigned int i, T val);
  inline void fillFast(unsigned int i, T val);
//  inline void fillByValue(float x, T val);
  inline void increment(unsigned int i);
  inline void incrementFast(unsigned int i);
  void clear();
  void publish(Provider* p);
private:
  T *m_data;
  TE *m_err;
  bool m_hasErrors;
};


#endif
