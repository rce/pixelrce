
#include "util/HistoManager.hh"
#include "util/AbsRceHisto.hh"


#include <regex>

HistoManager* HistoManager::m_manager=0;

HistoManager::HistoManager(Provider* p):m_provider(p){
  m_manager=this;
} 
HistoManager::~HistoManager(){
  m_manager=0;
}
void HistoManager::addToInventory(AbsRceHisto* histo){
  std::string name=histo->name();
  if(m_inventory.find(name)!=m_inventory.end()){
    std::cout<<"Histogram with name "<<name<<" exists already. Replacing..."<<std::endl;
  }
  m_inventory[name]=histo;
}
void HistoManager::removeFromInventory(AbsRceHisto* histo){
  std::map<std::string, AbsRceHisto*>::iterator it;
  it=m_inventory.find(histo->name());
  if(it==m_inventory.end()){
    std::cout<<"Histogram with name "<<histo->name()<<" was not in repository."<<std::endl;
  }else{
    m_inventory.erase(it);
  }
  std::map<std::string, AbsRceHisto*>::iterator it2=m_published.find(histo->name());
  if(it2!=m_published.end()){
    m_published.erase(it2);
  }
}

void HistoManager::publish(const char* rege){
  std::regex re(rege);
  std::map<std::string,AbsRceHisto*>::iterator it;
  for(it=m_inventory.begin();it!=m_inventory.end();it++){
    if(std::regex_match((*it).first,re)){
      (*it).second->publish(m_provider);
      m_published[(*it).first]=(*it).second;
    }
  }
}
std::vector<std::string> HistoManager::getHistoNames(const char *rege){
  std::vector<std::string> retvec;
  std::regex re(rege);
  std::map<std::string,AbsRceHisto*>::iterator it;
  for(it=m_inventory.begin();it!=m_inventory.end();it++){
    if(std::regex_match((*it).first,re)){
      retvec.push_back((*it).first);
    }
  }
  return retvec;
}
std::vector<std::string> HistoManager::getPublishedHistoNames(){
  std::vector<std::string> retvec;
  std::map<std::string,AbsRceHisto*>::iterator it;
  for(it=m_published.begin();it!=m_published.end();it++){
    retvec.push_back((*it).first);
  }
  return retvec;
}
