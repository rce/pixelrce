#ifndef PROVIDER_HH
#define PROVIDER_HH
#include "util/RCFPublisher.hh"
#include "util/RceHistoAxis.hh"
#include <vector>
#include <iostream>

class Provider {
public:
  Provider(RCF::RcfServer& pubserver){
    RCF::PublisherParms pubParms;
    pubParms.setOnSubscriberConnect(Provider::onSubscriberConnect);
    pubParms.setOnSubscriberDisconnect(Provider::onSubscriberDisconnect);
    m_provider=pubserver.createPublisher<I_RCFPublisher>(pubParms);
  }
  ~Provider(){
  }
  static bool onSubscriberConnect(RCF::RcfSession & session, const std::string & topicName){
    std::cout<<"Subscriber connected. Topic name is "<<topicName<<std::endl;
  }
  static bool onSubscriberDisconnect(RCF::RcfSession & session, const std::string & topicName){
    std::cout<<"Subscriber disconnected. Topic name is "<<topicName<<std::endl;
  }
  template<class TC,class TE>
  void publish(const std::string & name,
		const std::string & title,
		RceHistoAxis & xaxis,
		const TC * bins,
		const TE * errors,
		bool hasOverflowAndUnderflow,
		int tag  ,
		const std::vector< std::pair<std::string,std::string> > & annotations) ;
  template<class TC,class TE>
  void publish(const std::string & name,
		const std::string & title,
		RceHistoAxis & xaxis,
		RceHistoAxis & yaxis,
		const TC * bins,
		const TE * errors,
		bool hasOverflowAndUnderflow,
		int tag ,
		const std::vector< std::pair<std::string,std::string> > & annotations ) ;
private:
  boost::shared_ptr<RCF::Publisher<I_RCFPublisher> > m_provider;
};

#include "util/RCFProvider.i"

#endif
