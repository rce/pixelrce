#ifndef MULTITRIGANALYSIS_HH
#define MULTITRIGANALYSIS_HH

#include "analysis/CalibAnalysis.hh"
#include <map>

class ConfigGui;
class TFile;
class TH2;
class TH1D;

namespace RCE{
  class PixScan;
}

class MultiTrigAnalysis: public CalibAnalysis{
public:
  MultiTrigAnalysis(): CalibAnalysis(){}
  ~MultiTrigAnalysis(){}
  void analyze(TFile* file, TFile* anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]);
};


#endif
