#ifndef TDACANALYSIS_HH
#define TDACANALYSIS_HH

#include "analysis/CalibAnalysis.hh"
#include "analysis/CfgFileWriter.hh"
#include <map>

class ConfigGui;
class TFile;
class TH2;
class TH1D;

namespace RCE{
  class PixScan;
}
namespace{
  struct hdata{
    TH2* mean;
    TH2* tdac;
  };
}

class TdacAnalysis: public CalibAnalysis{
public:
  TdacAnalysis(CfgFileWriter* fw): CalibAnalysis(), m_fw(fw){}
  ~TdacAnalysis(){delete m_fw;}
  void analyze(TFile* file, TFile* anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]);
private:
  CfgFileWriter* m_fw;
};


#endif
