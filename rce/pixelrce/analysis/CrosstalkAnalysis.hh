#ifndef CROSSTALKANALYSIS_HH
#define CROSSTALKANALYSIS_HH

#include "analysis/CalibAnalysis.hh"
#include <map>

class ConfigGui;
class TFile;
class TH2;
class TH1D;

namespace RCE{
  class PixScan;
}

class CrosstalkAnalysis: public CalibAnalysis{
public:
  CrosstalkAnalysis(): CalibAnalysis(){}
  ~CrosstalkAnalysis(){}
  void analyze(TFile* file, TFile* anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]);
};


#endif
