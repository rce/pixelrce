#ifndef TDACFASTANALYSIS_HH
#define TDACFASTANALYSIS_HH

#include "analysis/CalibAnalysis.hh"
#include "analysis/CfgFileWriter.hh"
#include <map>

class ConfigGui;
class TFile;
class TH2;
class TH1D;

namespace RCE{
  class PixScan;
}
namespace{
  struct hdatafast{
    TH2* occupancy;
    TH2* tdac;
  };
}

class TdacFastAnalysis: public CalibAnalysis{
public:
  TdacFastAnalysis(CfgFileWriter* fw): CalibAnalysis(), m_fw(fw){}
  ~TdacFastAnalysis(){delete m_fw;}
  void analyze(TFile* file, TFile* anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]);
private:
  CfgFileWriter* m_fw;
};


#endif
