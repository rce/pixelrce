#include "analysis/Iff_Analysis.hh"
#include "config/FEI4/Module.hh"
#include "config/FEI3/Module.hh"

#include <TFile.h>
#include <TH2.h>
#include <TH2D.h>
#include <TH1.h>
#include <TH1F.h>
#include <TKey.h>
#include <regex>
#include <iostream>
#include <fstream>
#include <math.h>

using namespace RCE;

void IffAnalysis::analyze(TFile* file, TFile *anfile, PixScan* scan, int runno, ConfigGui* cfg[]){
  printf("Begin IFF Analysis\n");
  float target = float(scan->getTotTargetValue());
  printf("Target Tot Value: %2.1f\n",target);
  std::vector<float> loopVar=scan->getLoopVarValues(0);
  size_t numvals=loopVar.size();
  //TH1F* iffhis = new TH1F("iff_v_module","Best IF Setting",128,0,127);
  //iffhis->GetXaxis()->SetTitle("Module");
  //iffhis->GetYaxis()->SetTitle("IF Setting");
  std::map<int, odata> *histomap=new std::map<int, odata>[numvals];
  TKey *key;
  for (size_t i=0;i<numvals;i++){
    file->cd("loop1_0");
    TIter nextkey(gDirectory->GetListOfKeys()); 
    while ((key=(TKey*)nextkey())) {
      std::string name(key->GetName());
      std::regex re(Form("_(\\d+)_Occupancy_Point_%03d", i));
      std::cmatch matches;
      if(std::regex_search(name.c_str(), matches, re)){
	assert(matches.size()>1);
	std::string match(matches[1].first, matches[1].second);
	int lnm=strtol(match.c_str(),0,10);
	histomap[i][lnm].occ = (TH2*)key->ReadObj();
	std::regex re2("Occupancy");
	std::string totHistoName = std::regex_replace (name, re2, "ToT");
	histomap[i][lnm].tot=(TH2*)gDirectory->Get(totHistoName.c_str());
      }
    }
  }
  // write out results in a file
  std::string fn(anfile->GetName());
  std::string name="IFscan_results.dat";
  std::regex re("[^/]*\\.root");
  std::string maskfilename = std::regex_replace (fn, re, name);
  std::ofstream maskfile(maskfilename.c_str());
  for(std::map<int, odata>::iterator it=histomap[0].begin();it!=histomap[0].end();it++){ 
    int id=it->first;
    PixelConfig* confb=findConfig(cfg, id);
    int nchip;
    int nrow;
    int ncol;
    int fei;
    if(findFEType(cfg, id)=="FEI3"){
      nchip=FEI3::Module::N_FRONTENDS;
      nrow=FEI3::Frontend::N_ROWS;
      ncol=FEI3::Frontend::N_COLS;
      fei=3;
    }else{
      nchip=FEI4::Module::N_FRONTENDS;
      nrow=FEI4::PixelRegister::N_ROWS;
      ncol=FEI4::PixelRegister::N_COLS;
      fei=4;
    }
    for(int chip=0;chip<nchip;chip++){
      TH1F* ahis;
      if(findFEType(cfg, id)=="FEI3"){
	ahis = new TH1F(Form("Mod_%i_FE_%i_ToT_v_IFF",id, chip),
			      Form("ToT vs. IFF Mod %d FE %d at %s", id, chip, findFieldName(cfg, id)), 
			      256,-.5,255.5);
      }else{
	ahis = new TH1F(Form("Mod_%i_ToT_v_IFF",id),
			    Form("ToT vs. IFF Mod %d at %s", id, findFieldName(cfg, id)), 
			      256,-.5,255.5);	
      }
      ahis->GetXaxis()->SetTitle("IF Setting");
      ahis->GetYaxis()->SetTitle("Time over Threshold");
      int value = 0;
      float min = 999999.9;
      std::vector<float> vals;
      for(unsigned i = 0; i < numvals; i++){
	double tot = 0.0;
	double tot2 = 0.0;
	float numBadPixels = 0.0;
	for(int j = chip*ncol+1; j <= (chip+1)*ncol; j++){
	  //for(int j = 1; j <= histomap[i][id].occ->GetNbinsX(); j++){
	  for(int k = 1; k <= nrow; k++){
	    int nhits = int(histomap[i][id].occ->GetBinContent(j,k));
	    if(nhits == scan->getRepetitions()) {
	      double totval=histomap[i][id].tot->GetBinContent(j,k) / nhits;
	      tot += totval;
	      tot2 += totval*totval;
	    } else numBadPixels++;
	  }
	}
	float norm= float(nrow*ncol - numBadPixels);
	if(norm!=0){
	  tot/=norm;
	  tot2=sqrt(tot2/norm-tot*tot);
	}else{
	  tot=0;
	  tot2=0;
	}
	int bin=ahis->FindBin(loopVar[i]);
	ahis->SetBinContent(bin,tot);
	vals.push_back(tot);
	ahis->SetBinError(bin,tot2);
	if(fabs(tot - target) < min){
	  min = fabs(tot - target);
	  value = i;
	}
      }
      // interpolate
      int intval=int(loopVar[value]+.1);
      //below found value
      if(value!=0 && vals[value-1]!=vals[value] && loopVar[value]!=loopVar[value-1]){
	float m=(vals[value]-vals[value-1])/(loopVar[value]-loopVar[value-1]);
	float n=vals[value]-m*loopVar[value];
	for (int i=int(loopVar[value-1]+1.1);i<int(loopVar[value]+0.1);i++){
	  if(fabs(m*i+n-target)<min){
	    min=fabs(m*i+n-target);
	    intval=i;
	  }
	}
      }
      //above found value
      if((unsigned)value!=numvals-1 && vals[value+1]!=vals[value] && loopVar[value]!=loopVar[value+1]){
	float m=(vals[value]-vals[value+1])/(loopVar[value]-loopVar[value+1]);
	float n=vals[value]-m*loopVar[value];
	for (int i=int(loopVar[value]+1.1);i<int(loopVar[value+1]+0.1);i++){
	  if(fabs(m*i+n-target)<min){
	    min=fabs(m*i+n-target);
	    intval=i;
	  }
	}
      }
      printf("Module ID: %d  Closest Setting: %d \nDistance From Target: %4.3f\n",id, intval,min);
      char line[512];
      if(fei==4) sprintf(line, "Module ID: %d   Best setting: %d\n", id, intval);
      else sprintf(line, "Module ID: %d Chip: %d  Best setting: %d\n", id, chip, intval);
      maskfile<<line;
      anfile->cd();
      ahis->Write();
      ahis->SetDirectory(gDirectory);
      confb->setIf(chip, intval);
    }
    for(unsigned i = 0; i < numvals; i++){
      delete histomap[i][id].occ;
      delete histomap[i][id].tot;
      delete histomap[i][id].tot2;
    }
    writeConfig(anfile, runno);
  }
  if(configUpdate())writeTopFile(cfg, anfile, runno);
  printf("Done Analysis\n");
  delete [] histomap;
}

