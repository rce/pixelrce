#include "analysis/Fei4CfgFileWriter.hh"

#include <iostream>
#include <fstream>
#include <regex>
#include <TFile.h>
#include <TH2.h>

void Fei4CfgFileWriter::writeDacFile(const char* filename, TH2* tdac){
  std::ofstream maskfile(Form("%s.dat",filename));
  char line[512];
  for (int i=0;i<674;i++){
    if(i>1){
      sprintf(line, "%3d",i/2);
      maskfile<<line;
      if(i%2==0)maskfile<<"a  ";
      else maskfile<<"b  ";
    }else{
      maskfile<<"###   ";
    }
    for (int j=1;j<=40;j++){
      if(i==0)sprintf(line, "%2d ",j);
      else if(i==1)sprintf(line, "%2d ",j+40);
      else {
	int val = int(tdac->GetBinContent((i%2)*40+j, (i-2)/2+1)+0.1);
	sprintf(line, "%2d ",val);
      }
      maskfile<<line;
      if(j%10==0)maskfile<<"  ";
    }
    maskfile<<std::endl;
  }
}


void Fei4CfgFileWriter::writeMaskFile(const char* filename, TH2* histo){
  std::ofstream maskfile(Form("%s.dat",filename));
  maskfile<<"###  1     6     11    16     21    26     31    36     41    46     51    56     61    66     71    76"<<std::endl;
  char linenr[128];
  for(int i=1;i<=336;i++){
    sprintf(linenr,"%3d  ", i);
    maskfile<<linenr;
    for (int j=1;j<=80;j++){
      if(histo->GetBinContent(j,i)==1)maskfile<<0;
      else maskfile<<1;
      if(j%10==0)maskfile<<"  ";
      else if(j%5==0)maskfile<<"-";
    }
    maskfile<<std::endl;
  }
}
