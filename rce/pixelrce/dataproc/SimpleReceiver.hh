#ifndef SIMPLERECEIVER_HH
#define SIMPLERECEIVER_HH

#include "dataproc/AbsReceiver.hh"
#include "config/TriggerReceiverIF.hh"

class SimpleReceiver: public AbsReceiver, public TriggerReceiverIF{
public:
  SimpleReceiver(AbsDataHandler* handler):AbsReceiver(handler), TriggerReceiverIF(){}
  void Receive(unsigned *data, int size);
};
#endif
