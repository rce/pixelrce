#ifndef ATLASDATAHANDLER_HH
#define ATLASDATAHANDLER_HH

#include "dataproc/AbsDataHandler.hh"
#include <boost/property_tree/ptree_fwd.hpp>

class AbsFormatter;
class DataCond;

class AtlasDataHandler: public AbsDataHandler{
public:
  AtlasDataHandler(AbsDataProc* dataproc, DataCond &datacond, ConfigIF* cif, boost::property_tree::ptree* scanOptions);
  virtual ~AtlasDataHandler();
  void handle(unsigned desc, unsigned* data, int size);
};
#endif
