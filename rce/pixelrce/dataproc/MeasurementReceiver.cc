#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <iostream>
#include <string.h>
#ifdef __rtems__
#include "HW/RCDImaster.hh"
#else
#include "HW/RCDImasterL.hh"
#endif
#include <regex>
#include <boost/property_tree/ptree.hpp>
#include <sys/socket.h>
#include <arpa/inet.h>

#include <arpa/inet.h> // for htonl

#include "dataproc/MeasurementReceiver.hh"


MeasurementReceiver::MeasurementReceiver(AbsDataHandler* handler, boost::property_tree::ptree* scanOptions):
  PgpReceiver(handler), TriggerReceiverIF(){
  PgpTrans::RCDImaster::instance()->setReceiver(this);

  //the name is "ipAddr:port"
  std::string name=scanOptions->get<std::string>("Name");
  std::cmatch matches;
  std::regex re("(^.*):(\\d+)");
  std::string ipaddr, port;
  if(std::regex_search(name.c_str(), matches, re)){
    assert(matches.size()>2);
    ipaddr=std::string(matches[1].first, matches[1].second);
    port=std::string(matches[2].first, matches[2].second);
  }else{
    std::cout<<"No ip addr:port given"<<std::endl;
    assert(0);
  }
  m_addr.sin_addr.s_addr=inet_addr(ipaddr.c_str());
  m_addr.sin_family = AF_INET;
  int porti=atoi(port.c_str());
  m_addr.sin_port = htons(porti);

  std::cout<<"Created Measurement receiver"<<std::endl;
  
}


void MeasurementReceiver::Receive(unsigned *data, int size){
  std::string vals=sendMeasurementCommand();
  float val;
  char* end;
  val=strtof(vals.c_str(), &end);
  if(end-vals.c_str()!=(int)vals.size()){
    std::cout<<"Bad value "<<vals<<std::endl;
    assert(0);
  }
  m_handler->handle(0, (unsigned*)&val, 1);
  return;
}
const char* MeasurementReceiver::sendMeasurementCommand(){
  std::string inpline("measure");
  int sock;
  sock=socket(AF_INET, SOCK_STREAM, 0);
  assert(sock!=-1);
  struct timeval timeout;
  timeout.tv_sec=1;
  timeout.tv_usec=0;
  setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(struct timeval));
  int retval=connect(sock, (struct sockaddr *)&m_addr, sizeof(m_addr));
  send(sock, inpline.c_str(), inpline.size(), 0);
  static char line[128];
  line[0]=0;
  int bytes=recv(sock, line,128, 0);
  if(bytes>=0) line[bytes]=0;
  close(sock);
  return line;
}

