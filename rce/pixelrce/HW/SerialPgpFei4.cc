#include "HW/SerialPgpFei4.hh"
#include <iostream>

SerialPgpFei4::SerialPgpFei4(): SerialPgpBw(){}

void SerialPgpFei4::DisableOutput(){
  WriteRegister(13,0); 
}

void SerialPgpFei4::SetOutputMode(unsigned mode){
  unsigned m= mode ? 0 : 1;
  WriteRegister(16, m);
}
void SerialPgpFei4::SetDataRate(Rate rate){
  std::cout<<"Setting Data Rate (reg 10) to "<<rate<<std::endl;
  WriteRegister(10,rate);
}

