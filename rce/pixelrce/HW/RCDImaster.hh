// 
// Implements the master RCDI interface
// 
// Martin Kocian, SLAC, 6/6/2009
//

#ifndef RCDIMASTER_H
#define RCDIMASTER_H

#include <omnithread.h>
#include "rce/pgp/Handler.hh"
#include <list>
#include <vector>
#include <omnithread.h>

namespace RcePic{
  class Pool;
};


namespace PgpTrans {

  class Receiver;

  class RCDImaster: public RcePgp::Handler {
  public:
    enum ERRCODES{SENDFAILED=666, RECEIVEFAILED=667};
    enum TIMEOUTS{RECEIVETIMEOUT=100000000}; //in ns
    static RCDImaster* instance();
    void setPool(RcePic::Pool *p);
    void setDriver(RcePgp::Driver *pgpd);
    unsigned int writeRegister(unsigned address, unsigned value);
    unsigned int readRegister(unsigned address, unsigned& value);
    unsigned int blockWrite(unsigned* data, int size, bool handshake, bool byteswap);
    unsigned int blockWrite(RcePic::Tds* tds);
    unsigned int blockRead(unsigned* data, int size, std::vector<unsigned>& retvec);
    unsigned int readBuffers(std::vector<unsigned char>& retvec);
    unsigned int sendCommand(unsigned char opcode, unsigned context=0);
    void setReceiver(Receiver* receiver);
    Receiver* receiver();
    unsigned nBuffers();
    int currentBuffer(unsigned char*& header, unsigned &headerSize, unsigned char*&payload, unsigned &payloadSize);
    int discardCurrentBuffer();
  protected:
    RCDImaster();
    virtual void completed(RcePic::Tds *tds, RcePgp::Driver *pgpd);
    virtual void received(RcePic::Tds *tds, RcePgp::Driver *pgpd);
    unsigned transferred(RcePic::Tds* tds);
  private:
    RcePgp::Driver *_pgp_driver;
    Receiver* _receiver;
    RcePic::Pool * _pool;
    unsigned int _tid;
    omni_mutex _data_mutex;
    omni_condition _data_cond;
    unsigned _status;
    unsigned _data;
    std::list<RcePic::Tds*> _buffers;
    bool _handshake;
    bool _blockread;
    int m_counter;
    static omni_mutex _guard;
    static RCDImaster* _instance;
  };
    
}//namespace

#endif
