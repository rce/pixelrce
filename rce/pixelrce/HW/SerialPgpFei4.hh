#ifndef SERIALPGPFEI4_HH
#define SERIALPGPFEI4_HH

#include "HW/SerialPgpBw.hh"
#include <vector>

class SerialPgpFei4: public SerialPgpBw {
public:
  SerialPgpFei4();
protected:
  void DisableOutput();
  void SetOutputMode(unsigned mode);
  virtual void SetDataRate(Rate rate);
};


#endif
