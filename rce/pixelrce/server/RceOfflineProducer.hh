#ifndef RCEPRODUCER_HH
#define RCEPRODUCER_HH

#include "eudaq/Producer.hh"

#include "server/AbsController.hh"
#include <vector>

class ModuleInfo;
class CosmicDataReceiver;

class RceOfflineProducer : public eudaq::Producer {
public:
  RceOfflineProducer(const std::string & name, const std::string & runcontrol, AbsController *c, int rce);
  virtual ~RceOfflineProducer();
  virtual void OnConfigure(const eudaq::Configuration & config) ;
  virtual void OnStartRun(unsigned param) ;
  virtual void OnStopRun() ;
  virtual void OnTerminate() ;

private:
  unsigned m_run;
  unsigned m_PlaneMask;
  unsigned m_conseq;
  int m_numboards;
  int m_rce;
  std::vector<int> m_sensor;
  std::vector<int> m_pos;
  std::vector<int> m_link;
  std::vector<int> m_id;
  std::map<int, int> m_stype;
  AbsController* m_controller;
  ipc::ScanOptions* m_options;
  std::vector<ModuleInfo*> m_moduleinfo;
  CosmicDataReceiver *m_datareceiver;
};

#endif
