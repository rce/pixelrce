/*
FEI4MaskMaker for masking pixels in the enable and hitbus
map. 

Authors:
Ivan Lopez (ilopez@ifae.es)
Fabian Foerster (forster@ifae.es)
Martin Kocian (kocian@slac.stanford.edu)

*/


#include <TGClient.h>
#include <TCanvas.h>
#include <TGButton.h>
#include <TGLabel.h>
#include <TStyle.h>
#include <TGFileDialog.h>
#include <TGFrame.h>
#include <TGNumberEntry.h>
#include <TGLabel.h>
#include <TGFileDialog.h>
#include <TRootEmbeddedCanvas.h>
#include <RQ_OBJECT.h>
#include <TH2I.h>
#include <iostream>
#include <fstream>
#include <TString.h>
#include <string>

//Using percentage size of frame
#define FrameXSize 100
#define FrameYSize 100

class FEI4MaskMaker : public TGMainFrame {
  
public:
//private:

  TRootEmbeddedCanvas  *fEcanvas;
  TGNumberEntry        *Ecolumn;
  TGNumberEntry        *Erow;
  TGNumberEntry        *Evalue;
  TGTextButton         *save;
  TGCheckButton        *bena, *bhit;
  TH2I                 *fhmask;
  std::string          inPath;
  std::string          outPath;
  int                   setRow;
  int                   setCol;
  bool                  setValue;
  std::string           m_enablefile;
  std::string           m_hitbusfile;
  
  //public: 

  FEI4MaskMaker(std::string ef, std::string hf, const TGWindow *p, UInt_t w, UInt_t h);
  virtual ~FEI4MaskMaker();
  void DrawHisto();
  void LoadMask ();
  void SaveMask ();
  void SetPixel ();
  void SetAll   ();
  void SetCol   ();
  void SetRow   ();
  void Open     ();
  void Save     ();
  void SaveAs   ();
  void Exit     ();
  void ChangeTarget();//change target pixel and value
  //  void Reload   (); 

  ClassDef(FEI4MaskMaker,0);
};

