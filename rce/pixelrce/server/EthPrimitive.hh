#ifndef ETH_PRIMITIVE_H
#define ETH_PRIMITIVE_H

#define PACKETSIZE 300 
#include <sys/types.h>
#include "sys/socket.h"
#include <netinet/in.h>
#include "rce/net/SocketTcp.hh"
#include "rce/net/IpAddress.hh"
#include "rce/service/Procedure.hh"
class CmdDecoder;

class EthPrimitive {
 public:
  EthPrimitive(unsigned short port);
  const char* receiveCommand();
  const char* receiveModule(unsigned char* buffer, int size);
  void reply(const char* msg);
  void reply(char* buffer, int len);
 private:
  RceNet::Socket *m_connection;
  RceNet::SocketTcp m_socket;
};

#endif
