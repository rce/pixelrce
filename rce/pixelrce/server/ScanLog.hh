#ifndef SCANLOG_HH
#define SCANLOG_HH

#include <string>
#include <vector>


class LogData{
public:
  std::string comment;
  bool exported;
  bool saved;
  std::string datapath;
  std::vector<std::string> configs;
};

class ScanLog{
public:
  ScanLog();
  ~ScanLog(){}
  void logScan(LogData&);
  void finishLogFile(std::string runstatus, std::string lstatus);
  void writeToElog();
  void updateElog(std::string lstatus);
  void finishLogToDB();
  void startLogToDB();
  void setFile(std::string f){m_infofilename=f;}
  void logOldFile();
  void setAuthor(const char* name){
    m_author=name;
  }
  void setScanName(const char* name){
    m_scanname=name;
  }
  void setRunNumber(int runnumber){
    m_runnumber=runnumber;
  }
  void setDebugging(bool db){
    m_debugging=db;
  }
private:
  std::string m_infofilename;
  const char* m_user;
  std::string m_author;
  std::string m_scanname;
  int m_runnumber;
  bool m_debugging;
  std::string formattedCurrentTime();
};
  
#endif
