#ifndef CONFIGGUI_HH
#define CONFIGGUI_HH

#include <TGButton.h>
#include <TGTextEntry.h>
#include <TGNumberEntry.h>
#include <TGLabel.h>
#include <string>
#include <fstream>

class PixelConfig;

class ConfigGui: public TGVerticalFrame{
public:
  enum constants{MAX_MODULES=64};
  ConfigGui(const char* name, const TGWindow *p, UInt_t w, UInt_t h, UInt_t options, const char* prefix="Frontend ");
  virtual ~ConfigGui();
  void setConfig();
  void openFileWindow();
  void openMaskEditor();
  void setIncluded(bool on);
  void setValid(bool on);
  void resetConfig();
  void setType(const char* type);
  void dummy(bool on);
  bool isValid();
  const std::string getType();
  void enableControls(bool on);
  bool isIncluded();
  PixelConfig* getModuleConfig(){return m_cfg;}
  void setInLink(int link){m_inlink->SetIntNumber(link);}
  int getInLink(){return m_inlink->GetIntNumber();}
  void setOutLink(int link){m_outlink->SetIntNumber(link);}
  int getOutLink(){return m_outlink->GetIntNumber();}
  void setRce(int link){m_rce->SetIntNumber(link);setModuleName();}
  int getRce(){return m_rce->GetIntNumber();}
  void setPhase(int phase){m_phase->SetIntNumber(phase);}
  int getPhase(){return m_phase->GetIntNumber();}
  void setId(int id);
  int getId(){return m_id;}
  const char* getModuleName(){return m_modulename.c_str();}
  const char* getConfdir(){return m_confdir->GetText()->Data();}
  const char* getKey(){return m_key->GetText()->Data();}
  const char* getConfigName(){return m_configname->GetText()->Data();}
  const char* getModuleId(){return m_modid->GetText()->Data();}
  std::string getPath(){return m_path;}
  void setModuleName();
  void updateText(TGLabel* label, const char* newtext);
  void readConfig(std::ifstream &ifile);
  void writeConfig(std::ofstream &ofile);
  void copyConfig(std::ofstream &ofile, const char* filename);
  void copyConfig(const char* dirname);
  void setFilename(const char* filename){m_filename=filename;}
  const char* getFilename(){return m_filename.c_str();}
  const char* getName(){return m_modName.c_str();}
  static void setRootDir(std::string s){
    m_rootdir=s;
  }
  static std::string getRootDir(){
    return m_rootdir;
  }
private:
  unsigned parseModuleId(const char* module, unsigned fe);
  TGLabel *m_name, *m_configname, *m_idl, *m_key, *m_confdir, *m_modid, *m_configtype;
  TGTextButton *m_choosebutton, *m_maskbutton;
  TGCheckButton *m_configvalid, *m_included;
  TGNumberEntry *m_inlink, *m_outlink, *m_rce, *m_phase;
  std::string m_modName;
  bool m_valid;
  PixelConfig *m_cfg;
  std::string m_filename;
  std::string m_modulename;
  std::string m_path;
  int m_id;

  static std::string m_rootdir;

  ClassDef(ConfigGui,0);
};

#endif
