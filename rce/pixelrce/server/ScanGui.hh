#ifndef SCANGUI_HH
#define SCANGUI_HH

#include <TGButton.h>
#include <TGTextEntry.h>
#include <TGNumberEntry.h>
#include <TGLabel.h>
#include <TGComboBox.h>
#include <fstream>
#include <string>
#include <map>


namespace ipc{
  class ScanOptions;
}
namespace RCE {
class PixScan;
}

class ScanGui: public TGVerticalFrame{
public:
  ScanGui(const char* name, const TGWindow *p, UInt_t w, UInt_t h, UInt_t options);
  virtual ~ScanGui();
  void print(std::ostream &os);
  void enableControls(bool on);
  std::string getTypeString(){return m_typestring;}
  ipc::ScanOptions* getScanConfig();
  RCE::PixScan* getPixScan();
  //void selected(int i);
  void dummy(bool);
  void dummyAct(bool);
  void setupScan(std::string flavor, int runnum);
  static std::map<int, std::string> getScanTypes() {return m_scantypes;}
private:
  void initScanTypes();
  TGLabel *m_name;
  TGLabel *m_maskmode, *m_maskstages;
  TGLabel *m_dig, *m_rep;
  TGComboBox *m_scan;
  TGCheckButton *m_active[3];
  TGCheckButton *m_rce[3];
  TGLabel *m_par[3], *m_steps[3], *m_low[3], *m_high[3];
  TGNumberEntry *m_thresholdTarget, *m_targetCharge, *m_targetValue;
  bool m_rceb[3];
  ipc::ScanOptions *m_scanopt;
  RCE::PixScan *m_scn;
  void updateText(TGLabel* label, const char* newtext);
  void updateText(TGLabel* label, std::string newtext);
  static std::map<int, std::string> m_scantypes;
  static bool m_initialized;
  std::string m_typestring;
  int m_thresholdTargetC, m_targetChargeC, m_targetValueC;
  ClassDef(ScanGui,0);
};

#endif

