#include "server/RCFController.hh"
#include "RCFConfigIFAdapter.hh"

#include <iostream>
#include <stdlib.h>
#include <getopt.h>

void print_usage() {
  std::cout << "usage rebootHSIO --rce,-r <rce_number> --help,-h" <<std::endl;
  exit(EXIT_FAILURE);
}

int main( int argc, char ** argv ){
  int rce=-1;
  int opt=0;
  static struct option long_options[] = {
    {"rce",      required_argument,  0,  'r' },
    {"help",      no_argument,       0,  'h' },
    {0,           0,                 0,  0   }
  };
  int long_index =0;
  while ((opt = getopt_long(argc, argv,"r:h", 
			    long_options, &long_index )) != -1) {
    switch (opt) {
    case 'r' : rce = atoi(optarg);
      break;
    case 'h':
      
    default: print_usage(); 
    }
  }
  if(rce<0)  {
    print_usage();
  }

  RCF::RcfInitDeinit rcfInit;
  RCFController controller;
  // How many RCEs are in the partition?
  std::cout<<"Rebooting HSIO on RCE "<<rce<<std::endl;
  controller.addRce(rce);
  controller.sendHWcommand(rce,8); //reboot
}
