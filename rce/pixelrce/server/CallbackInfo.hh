#ifndef CALLBACKINFO_HH
#define CALLBACKINFO_HH

class TGLabel;

class CallbackInfo{
public:
  enum guistate {FAILED, DONE, VERIFY, WAIT, ANALYZE, SAVE, STOP, DOWNLOAD, FIT, NEWSTAGE, RUNNING, NBITS};
  CallbackInfo(TGLabel* label);
  void addToMask(int val){m_mask|=(1<<val);}
  void setStage(int stage){m_stage=stage;}
  void updateGui();
  void clear(){m_mask=0;}
  bool failed(){return ((m_mask&(1<<FAILED))!=0);}
private:
  TGLabel* m_label;
  int m_stage;
  unsigned m_mask;
};
#endif
