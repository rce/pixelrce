

#include "server/RCFController.hh"
#include "server/PixScan.hh"
#include "ScanOptions.hh"
#include "config/FEI4/FECommands.hh"
#include "config/FEI4/FEI4ARecord.hh"
#include "config/FEI4/FEI4AFormatter.hh"
#include "config/FormattedRecord.hh"
#include "config/FEI4/FEI4AConfigFile.hh"

//#include <boost/program_options.hpp>
#include <iostream>
#include <stdio.h>


typedef struct {
  uint16_t     DB0;      // (bitmask, one bit set)
  uint16_t     DB1;      // (bitmask, one bit set)
  uint16_t     DB2;      // (bitmask, one bit set)
  uint16_t     DB3;      // (bitmask, one bit set)
  uint16_t     DB4;      // (bitmask, one bit set)
  uint16_t     DB5;      // (bitmask, one bit set)
  uint16_t     DB6;      // (bitmask, one bit set)
  uint16_t     DB7;      // (bitmask, one bit set)
  uint16_t     RS;       // (bitmask, one bit set)
  uint16_t     RW;       // (bitmask, one bit set)
  uint16_t     EN;       // (bitmask, one bit set)
  int rce;
} HD44780_t;
static HD44780_t lcd     = { 1 <<  8,
                             1 <<  9,
                             1 << 10,
                             1 << 11,
                             1 << 12,
                             1 << 13,
                             1 << 14,
                             1 << 15, 
                             1 << 0,
                             1 << 1,
                             1 << 2,
                             0};

HD44780_t* lcdp = &lcd;

#define PCA9535_REG_INPUT_0   0
#define PCA9535_REG_INPUT_1   1
#define PCA9535_REG_OUTPUT_0  2
#define PCA9535_REG_OUTPUT_1  3
#define PCA9535_REG_POLINV_0  4
#define PCA9535_REG_POLINV_1  5
#define PCA9535_REG_CFG_0     6
#define PCA9535_REG_CFG_1     7

#define HD44780_COMMAND 0
#define HD44780_DATA    1

#define HD44780_MINDELAY      43          // minimum delay between commands
#define HD44780_EN_UP         1           //Time for EN bit to be up (us)
                                          //Spec has either 480 or 460 ns

#define DEL_INIT_POWERON      40000       // Max poweron delay from [2] and [3]
#define DEL_INIT_1            5000        // From [2] -- larger than [3]
#define DEL_INIT_2            100         // from [2]
#define INIT_8_BIT            0x30
#define INIT_4_BIT            0x20

// Specific LCD commands, their arguments and their required delays (in us)
#define CMD_CLEARDISPLAY      (1 << 0)    //0b00000001
#define DEL_CLEARDISPLAY      1520        // 1.52ms

#define CMD_RETURNHOME        (1 << 1)    //0b00000010
#define DEL_RETURNHOME        1520        // 1.52ms
#define NWR_RETURNHOME        HD44780_MINWRITE

#define CMD_ENTRYMODESET      (1 << 2)    //0b00000100
#define DEL_ENTRYMODESET      HD44780_MINDELAY
#define EMS_INCREMENT         (1 << 1)    //0b00000010
#define EMS_DECREMENT         0 
#define EMS_DISPLAYSHIFTON    (1 << 0)    //0b00000001
#define EMS_DISPLAYSHIFTOFF   0


#define CMD_DISPLAYCONTROL    (1 << 3)    //0b00001000
#define DEL_DISPLAYCONTROL    HD44780_MINDELAY
#define DSC_DISPLAYON         (1 << 2)    //0b00000100
#define DSC_DISPLAYOFF        0 
#define DSC_CURSORON          (1 << 1)    //0b00000010
#define DSC_CURSOROFF         0 
#define DSC_BLINKON           (1 << 0)    //0b00000001
#define DSC_BLINKOFF          0

#define CMD_CURSORDISPSHIFT   (1 << 4)    //0b00010000
#define DEL_CURSORDISPSHIFT   HD44780_MINDELAY
#define CDS_DISPLAYSHIFT      (1 << 3)    //0b00001000
#define CDS_CURSORMOVE        0 
#define CDS_SHIFTRIGHT        (1 << 2)    //0b00000100
#define CDS_SHIFTLEFT         0

#define CMD_FUNCTIONSET       (1 << 5)    //0b00100000
#define DEL_FUNCTIONSET       HD44780_MINDELAY
#define FNS_DATAWIDTH8        (1 << 4)    //0b00010000
#define FNS_DATAWIDTH4        0 
#define FNS_DISPLAYLINES2     (1 << 3)    //0b00001000
#define FNS_DISPLAYLINES1     0 
#define FNS_FONT5X10          (1 << 2)    //0b00000100
#define FNS_FONT5X7           0

#define CMD_SETCGRAMADDR      (1 << 6)    //0b01000000
#define DEL_SETCGRAMADDR      HD44780_MINDELAY
#define CGR_ADDRESSMASK       0x3F

#define CMD_SETDDRAMADDR      (1 << 7)    //0b10000000
#define DEL_SETDDRAMADDR      HD44780_MINDELAY
#define DDR_ADDRESSMASK       0x7F

#define DEL_ROTATEDISPLAY     HD44780_MINDELAY  // rotation rate controlled externally

#define GPIO_ALL_LINES_OUTPUT  0x0000
#define GPIO_ALL_LINES_INPUT   0xFFFF

//Display
void HD44780_init(const HD44780_t *device, uint8_t lines);
void HD44780_reset(const HD44780_t *device);
int HD44780_WriteDelay(const HD44780_t* device, const uint16_t data, const uint32_t delayAfter);
int HD44780_WriteSync(const HD44780_t* device, const uint16_t data, const uint32_t delayAfter);
void HD44780_Command(const HD44780_t*, const uint8_t command, const uint32_t delayafter);
void HD44780_Data(const HD44780_t*, const uint8_t);
void HD44780_WriteByteDDRAM(const HD44780_t*, const uint8_t, const uint8_t);
void HD44780_WriteByte(const HD44780_t*, const uint8_t reg, const uint8_t data, const uint32_t delayafter);
void HD44780_GpioWrite(const HD44780_t* device, const uint16_t data);
int i2c_write_reg(const HD44780_t* device, uint8_t reg, uint8_t data);
void HD44780_clear(const HD44780_t* device);
void HD44780_home(const HD44780_t* device);
void HD44780_putChar(const HD44780_t* device, const uint8_t data);
void HD44780_putCharAt(const HD44780_t* device, const uint8_t data, const uint8_t address);
void HD44780_putStr(const HD44780_t* device, const char* str);
void HD44780_putStrN(const HD44780_t* device, const char* str, const uint8_t strlen);
void HD44780_rotate(const HD44780_t* device, const uint8_t direction);

void encode8b10b(unsigned *patbuffer, int patlen, unsigned *encbuffer, int &enclen);
void addBits(unsigned *buffer, unsigned word, unsigned numbits, unsigned bitpos);
unsigned decode(std::vector<unsigned char> inpvec);

int main( int argc, char ** argv ){

  // TODO: add command line parser if needed

//
// Initialize command line parameters with default values
//
  RCF::RcfInitDeinit rcfInit;
  RCFController controller;
  int rce, inlink, outlink;
    std::cout<<"RCE number?"<<std::endl;
    std::cin>>rce;
    std::cout<<"Inlink number?"<<std::endl;
    std::cin>>inlink;
    std::cout<<"Outlink number?"<<std::endl;
    std::cin>>outlink;
    unsigned serstat, val;
    controller.removeAllRces();
    controller.addRce(rce);
    assert(controller.writeHWregister(rce, 3,0)==0); //emulator or loopback
    controller.removeAllModules();
    controller.addModule("module", "FEI4B",0,inlink,outlink,rce, "");

    //setup chip
    char binding[32];
    sprintf(binding, "I_RCFFEI4BAdapter_0");
    serstat=controller.writeHWglobalRegister(binding, 35, 0x5d4);
    serstat=controller.writeHWglobalRegister(binding, 34, 0x10);
    serstat=controller.writeHWglobalRegister(binding, 33, 0x0);
    serstat=controller.writeHWglobalRegister(binding, 32, 0x0);
    serstat=controller.writeHWglobalRegister(binding, 31, 0xf000);
    serstat=controller.writeHWglobalRegister(binding, 30, 0x0);
    serstat=controller.writeHWglobalRegister(binding, 29, 0x7);
    serstat=controller.writeHWglobalRegister(binding, 28, 0x8206);
    serstat=controller.writeHWglobalRegister(binding, 27, 0x8000);
    serstat=controller.writeHWglobalRegister(binding, 26, 0x8);
    serstat=controller.writeHWglobalRegister(binding, 25, 0xe00);
    serstat=controller.writeHWglobalRegister(binding, 24, 0x0);
    serstat=controller.writeHWglobalRegister(binding, 23, 0x0);
    serstat=controller.writeHWglobalRegister(binding, 22, 0xa0);
    serstat=controller.writeHWglobalRegister(binding, 21, 0x7ff);
    serstat=controller.writeHWglobalRegister(binding, 19, 0x600);
    serstat=controller.writeHWglobalRegister(binding, 18, 0xff);
    serstat=controller.writeHWglobalRegister(binding, 17, 0x2d);
    serstat=controller.writeHWglobalRegister(binding, 16, 0xd238);
    serstat=controller.writeHWglobalRegister(binding, 15, 0x1a96);
    serstat=controller.writeHWglobalRegister(binding, 14, 0xd54c);
    serstat=controller.writeHWglobalRegister(binding, 13, 0x0);
    serstat=controller.writeHWglobalRegister(binding, 12, 0x7200);
    serstat=controller.writeHWglobalRegister(binding, 11, 0x56d4);
    serstat=controller.writeHWglobalRegister(binding, 10, 0x284c);
    serstat=controller.writeHWglobalRegister(binding, 9, 0xaa);
    serstat=controller.writeHWglobalRegister(binding, 8, 0xf258);
    serstat=controller.writeHWglobalRegister(binding, 7, 0x6958);
    serstat=controller.writeHWglobalRegister(binding, 6, 0xd4);
    serstat=controller.writeHWglobalRegister(binding, 5, 0xd495);
    serstat=controller.writeHWglobalRegister(binding, 4, 0xc0);
    serstat=controller.writeHWglobalRegister(binding, 3, 0x4600);
    serstat=controller.writeHWglobalRegister(binding, 2, 0x0);
    serstat=controller.writeHWglobalRegister(binding, 1, 0xff);
    
    /* Raw bitstream test with printout
    PixScan scn(PixScan::COSMIC_DATA);
    ipc::ScanOptions options;
    scn.setName("TCP_0000"); // send data over the network rather than to file
    scn.convertScanConfig(options);
    controller.downloadScanConfig("CosmicData", options);
    unsigned encbuffer[1024];
    while(1){
      std::cout<<"Phase: ";
      int phase;
      std::cin >>phase;
      controller.writeHWregister(10,phase);  
     for (int i=0;i<1024;i++)encbuffer[i]=0;
      std::vector<unsigned> bitstream;
      std::string inp;
      bool eof=true;
	do{
	  std::cin>>inp;
	  if(inp=="s")break;
	  if(inp=="q")exit(0);
	  if(inp=="eof" || inp=="EOF"){
	    bitstream.push_back(0x1bc);
	    eof=true;
	  } else if(inp=="sof" || inp=="SOF"){
	    bitstream.push_back(0x1fc);
	    eof=false;
	  } else if(inp=="idle" || inp=="IDLE")bitstream.push_back(0x13c);
	  else bitstream.push_back(strtoul(inp.c_str(),0,16));
	}while (1);
	if(eof==false){
	  std::cout<<"SOF not followed by EOF. Send anyway?"<<std::endl;
	  std::cin>>inp;
	  if(inp!="y" && inp!="Y")continue;
	}
	int enclen;
	encode8b10b(&bitstream[0],bitstream.size(),encbuffer,enclen);
	std::vector<unsigned> bs8b10b;
	for (int i=0;i<enclen;i++){
	  std::cout<<std::hex<<encbuffer[i]<<std::endl;
	  bs8b10b.push_back(encbuffer[i]);
	}
	//serstat=controller.writeHWblockData(bs8b10b);
	serstat=controller.writeHWblockData(bitstream);
	assert(serstat==0);
    }
    */
    // Test of trigger command
    int command;
    unsigned short inp;
    unsigned nbt;
    int reg;
    BitStream *bs;
    std::vector<unsigned char> rep;
    new HistoManager(0);
    FEI4AFormatter fmt(0);
    FEI4::FECommands commands;
    std::vector<unsigned> dcoldata;
    std::vector<unsigned> retv;
    unsigned dcol, bit, xval;
    FEI4AConfigFile fei4file;
    ipc::PixelFEI4AConfig* cfgfei4a;
    bool good;
    commands.setAddr(0);
    bs=new BitStream;
    commands.switchMode(bs, FEI4::FECommands::RUN);
    bs->push_back(0);
    //    for(size_t i=0;i<bs->size();i++)std::cout<<std::hex<<(*bs)[i]<<std::endl;
    
    while(true){
      std::cout<<"Choose a command"<<std::endl;
      std::cout<<"================"<<std::endl;
      std::cout<<" 1) Send command"<<std::endl;
      std::cout<<" 2) Write register"<<std::endl;
      std::cout<<" 3) Read register"<<std::endl;
      std::cout<<" 4) Write block data"<<std::endl;
      std::cout<<" 5) Initialize display"<<std::endl;
      std::cout<<" 6) Write string to display"<<std::endl;
      std::cout<<" 7) Write FE register"<<std::endl;
      std::cout<<" 8) Read FE register"<<std::endl;
      //std::cout<<" 5) Write block data with handshake"<<std::endl;
      std::cin>>std::dec>>command;
      unsigned address, data;
      switch (command){
      case 7:
	std::cout<<"Address?"<<std::endl;
	std::cin>>address;
	std::cout<<"Data?"<<std::endl;
        std::cin>>data;
	serstat=controller.writeHWglobalRegister(binding, address, data);
	if(serstat!=0)std::cout<<"Write FE register failed"<<std::endl;
        break;
      case 8:
	std::cout<<"Address?"<<std::endl;
	std::cin>>address;
	unsigned short val;
	serstat=controller.readHWglobalRegister(binding, address, val);
	if(serstat!=0)std::cout<<"Read FE register failed"<<std::endl;
	else std::cout<<"Value="<<val<<std::endl;
        break;
      case 1:
	std::cout<<"Opcode?"<<std::endl;
        unsigned opcode;
	std::cin>>opcode;
	assert(controller.sendHWcommand(rce, opcode)==0);      
        break;
      case 2:
        unsigned address, data;
	std::cout<<"Address?"<<std::endl;
	std::cin>>std::hex>>address;
	std::cout<<"Data?"<<std::endl;
        std::cin>>std::hex>>data>>std::dec;
	std::cout<<controller.writeHWregister(rce,address, data)<<std::endl;;
        break;
      case 3:
        unsigned addressr, datar;
	std::cout<<"Address?"<<std::endl;
	std::cin>>addressr;
	std::cout<<controller.readHWregister(rce,addressr, datar)<<std::endl;
	std::cout<<"Data "<<std::hex<<datar<<std::dec<<std::endl;
        break;
       case 4:
	bs=new BitStream;
	commands.L1A(bs);
	serstat=controller.writeHWblockData(rce, *bs);	
	if(serstat!=0)std::cout<<"Send Error "<<serstat<<std::endl;
	delete bs;
	break;
      case 5:
	HD44780_reset(lcdp);
	break;
      case 6:
	char bla[128];
	std::cout<<"Enter string to display."<<std::endl;
	std::cin>>bla;
	HD44780_putStr(lcdp, bla);
	break;
      }
    }
}

void encode8b10b(unsigned *patbuffer, int patlen, unsigned *encbuffer, int &enclen){
  unsigned enc5b6b[2][32]={{39,29,45,49,53,41,25,56,57,37,21,52,13,44,28,23,27,35,19,50,11,42,26,58,51,38,22,54,14,46,30,43},
			{24,34,18,49,10,41,25,7,6,37,21,52,13,44,28,40,36,35,19,50,11,42,26,5,12,38,22,9,14,17,33,20}};
  unsigned disp5b6b[32]={1,1,1,0,1,0,0,0,1,0,0,0,0,0,0,1,1,0,0,0,0,0,0,1,1,0,0,1,0,1,1,1};
  unsigned enc3b4b[2][8]={{11,9,5,12,13,10,6,14},{4,9,5,3,2,10,6,1}}; 
  unsigned disp3b4b[8]={1,0,0,0,1,0,0,1};
  enclen=patlen*10/8/4;
  if((patlen*10)%32!=0){
    enclen++;
    std::cout<<"WARNING: last word not fully utilized... contains zeroes"<<std::endl;
  }
  unsigned char *sbuffer;
  sbuffer=(unsigned char*)patbuffer;
  unsigned bitpos=0;
  int disparity=0; //1 is 1, 0 is -1
  for (int i=0;i<patlen;i++){
    unsigned char word=(unsigned char)patbuffer[i]&0xff;
    std::cout<<"Word "<<(unsigned)word<<std::endl;
    if(patbuffer[i]&0x100){ //K symbol
      unsigned kword=0;
      if(word==252){
	if(disparity==0){
	  kword=0xf8;
	  disparity=1;
	} else {
	  kword=0x307;
	  disparity=0;
	}
      }else if(word==188){
	if(disparity==0){
	  kword=0xfa;
	  disparity=1;
	} else {
	  kword=0x305;
	  disparity=0;
	}
      }else if(word==60){
	if(disparity==0){
	  kword=0xf9;
	  disparity=1;
	} else {
	  kword=0x306;
	  disparity=0;
	}
      }
      addBits(encbuffer,kword,10,bitpos);
      bitpos+=10;
    }else{
      unsigned word5b=word&0x1f;
      unsigned encword5b=enc5b6b[disparity][word5b];
      addBits(encbuffer,encword5b,6,bitpos);
      bitpos+=6;
      disparity=disparity ^ disp5b6b[word5b];
      unsigned word3b=word>>5;
      unsigned encword3b=enc3b4b[disparity][word3b];
      addBits(encbuffer,encword3b,4,bitpos);
      bitpos+=4;
      disparity=disparity ^ disp3b4b[word3b];
    }
  }
}

  void addBits(unsigned *buffer, unsigned word, unsigned numbits, unsigned bitpos){
  unsigned current = bitpos/32;
  unsigned bits=bitpos%32;
  if (numbits>32-bits){ // Split over 2 words
    int bitsleft=32-bits;
    buffer[current]|=word>>(numbits-bitsleft);
    buffer[current+1]|=word<<(32-(numbits-bitsleft));
  }else{ // bits fit into same word; 
    buffer[current]|=word<<(32-bits-numbits);
  }
}

unsigned decode(std::vector<unsigned char> inpvec){
  if(inpvec.size()==0)return FEI4::FEI4ARecord::Empty;
  unsigned char* bytepointer=&inpvec[0];
  size_t size=inpvec.size();
  unsigned char* last=bytepointer+size-3;
  FEI4::FEI4ARecord rec;
  bool header=false;
  while(bytepointer<=last){
    rec.setRecord(bytepointer);
    //    std::cout<<"Record "<<std::hex<<rec.getUnsigned()<<" "<<rec.getHeader()<<std::endl;
    if(rec.isAddressRecord()){ // address record
      std::cout<<"Address record for ";
      if(rec.isGlobal())std::cout<<" global register ";
      else std::cout<<" shift register ";
      std::cout<<rec.getAddress()<<std::endl;
      header=false;
    }else if(rec.isValueRecord()){ //val rec without addr rec
      printf("Value record: %04x\n",rec.getValue());
      header=false;
    }else if(rec.isServiceRecord()){
      printf("Service record. Error code: %d. Count: %d \n",rec.getErrorCode(), rec.getErrorCount());
      header=false;
    }else if(rec.isEmptyRecord()){
    // do nothing
      std::cout<<"Empty record"<<std::endl;
      header=false;
    }else if(rec.isDataHeader()){
      std::cout<<"Data Header Lv1ID = "<<std::dec<<rec.getL1id()<<" BXID = "<<rec.getBxid()<<std::endl;
      header=true;
    }else if(rec.isData()){
      // std::cout<<std::hex<<rec.getHeader()<<std::endl;
      std::cout<<"Data = "<<std::hex<<rec.getUnsigned()<<std::dec<<"Data Col = "<<std::dec<<rec.getColumn()<<" Row = "<<rec.getRow()<<" TotTop = "<<rec.getTotTop()<<" TotBottom = "<<rec.getTotBottom()<<std::endl;
    }else{
      std::cout<<"Unexpected record type: "<<std::hex<<rec.getUnsigned()<<std::dec<<std::endl;
    }
    bytepointer+=3;
  }
  return FEI4::FEI4ARecord::OK;
  }

void HD44780_reset(const HD44780_t *device){
  i2c_write_reg(device, PCA9535_REG_CFG_0, 0);
  usleep(1000);
  i2c_write_reg(device, PCA9535_REG_CFG_1, 0);
  usleep(1000);

  i2c_write_reg(device, PCA9535_REG_OUTPUT_0, 0xff);
  usleep(1000);
  i2c_write_reg(device, PCA9535_REG_OUTPUT_1, 0xff);
  usleep(1000);

  i2c_write_reg(device, PCA9535_REG_POLINV_0, 0);
  usleep(1000);
  i2c_write_reg(device, PCA9535_REG_POLINV_1, 0);
  usleep(1000);

  HD44780_WriteByte(device, HD44780_COMMAND, INIT_8_BIT, DEL_INIT_1);
  usleep(1000);
  HD44780_WriteByte(device, HD44780_COMMAND, INIT_8_BIT, DEL_INIT_2);
  usleep(1000);
  HD44780_WriteByte(device, HD44780_COMMAND, INIT_8_BIT, DEL_FUNCTIONSET);
  HD44780_init(device, 2);
}
void
HD44780_WriteByte(const HD44780_t* device, const uint8_t reg, const uint8_t data, const uint32_t delayafter)
{
  // Don't assume that the pins are in any order
  uint16_t toWrite = (( (data & (1<<0)) ? device->DB0 : 0 ) |
                      ( (data & (1<<1)) ? device->DB1 : 0 ) |
                      ( (data & (1<<2)) ? device->DB2 : 0 ) |
                      ( (data & (1<<3)) ? device->DB3 : 0 ) |
                      ( (data & (1<<4)) ? device->DB4 : 0 ) |
                      ( (data & (1<<5)) ? device->DB5 : 0 ) |
                      ( (data & (1<<6)) ? device->DB6 : 0 ) |
                      ( (data & (1<<7)) ? device->DB7 : 0 ));
  if (reg == HD44780_DATA) toWrite |= device->RS;
  HD44780_WriteDelay(device, toWrite, delayafter);  
}
void HD44780_Command(const HD44780_t* device, const uint8_t command, const uint32_t delayafter)
{
  HD44780_WriteByte(device, HD44780_COMMAND, command, delayafter);
}
void
HD44780_Data(const HD44780_t* device, const uint8_t data)
{
  HD44780_WriteByte(device, HD44780_DATA, data, HD44780_MINDELAY);
}
void
HD44780_WriteByteDDRAM(const HD44780_t* device, const uint8_t data, const uint8_t address)
{
  // Write a byte at a specific DDRAM address
  HD44780_Command(device, CMD_SETDDRAMADDR | (address & DDR_ADDRESSMASK),
                  DEL_SETDDRAMADDR);
  HD44780_Data(device, data); 
}
int HD44780_WriteDelay(const HD44780_t* device, const uint16_t data, const uint32_t delayAfter)
{
  int usec = HD44780_WriteSync(device, data, delayAfter);
  usleep(usec);
  return usec;
}
int
HD44780_WriteSync(const HD44780_t* device, const uint16_t data, const uint32_t delayAfter)
{
  uint16_t ldata = data;
  HD44780_GpioWrite(device, ldata);
  usleep(1000);
  ldata |= device->EN;
  std::cout<<"Towrite "<<std::hex<<ldata<<std::dec<<std::endl;
  HD44780_GpioWrite(device, ldata);
  // one microsecond synchronous delay simply to allow risetime
  usleep(HD44780_EN_UP);
  usleep(1000);
  ldata &= ~(device->EN);
  HD44780_GpioWrite(device, ldata);
  usleep(1000);
  return delayAfter > HD44780_MINDELAY ? delayAfter : HD44780_MINDELAY;
}

void HD44780_GpioWrite(const HD44780_t* device, const uint16_t data)
{
  uint8_t tmp0 = data & 0xFF;
  uint8_t tmp1 = (data >> 8) & 0xFF;
  i2c_write_reg(device, PCA9535_REG_OUTPUT_0, tmp0);
  i2c_write_reg(device, PCA9535_REG_OUTPUT_1, tmp1);
}

int i2c_write_reg(const HD44780_t* device, uint8_t reg, uint8_t data){
  //  device->controller->writeHWregister(device->rce, 31, (reg<<8)|data);
  return 0;
}

void
HD44780_clear(const HD44780_t *device)
{
  HD44780_Command(device, CMD_CLEARDISPLAY, DEL_CLEARDISPLAY);
}

void
HD44780_home(const HD44780_t *device)
{
  HD44780_Command(device, CMD_RETURNHOME, DEL_RETURNHOME);
}

void
HD44780_putChar(const HD44780_t *device, const uint8_t data)
{
  HD44780_Data(device, data);
}

void
HD44780_putCharAt(const HD44780_t *device, const uint8_t data, const uint8_t address)
{
  HD44780_WriteByteDDRAM(device, data, address);
}

void
HD44780_putStr(const HD44780_t *device, const char* str)
{
  while (*str) HD44780_putChar(device, *str++);
}

void
HD44780_putStrN(const HD44780_t *device, const char* str, const uint8_t length)
{
  uint8_t toWrite = length;
  while (toWrite--) HD44780_putChar(device, *str++);
}

void
HD44780_rotate(const HD44780_t* device, const uint8_t direction)
{
  HD44780_Command(device, CMD_CURSORDISPSHIFT | CDS_DISPLAYSHIFT |
                  ((direction) ? CDS_SHIFTLEFT : CDS_SHIFTRIGHT),
                  DEL_ROTATEDISPLAY);
}
// Public interface
void
HD44780_init(const HD44780_t *device, const uint8_t lines)
{
  HD44780_Command(device, (CMD_FUNCTIONSET |
                           FNS_DATAWIDTH8
                           | FNS_FONT5X7 |
                           (lines == 0 ? FNS_DISPLAYLINES1 : FNS_DISPLAYLINES2)),
                  DEL_FUNCTIONSET);
  // Turn the display off
  HD44780_Command(device, CMD_DISPLAYCONTROL | DSC_DISPLAYOFF | DSC_CURSOROFF | DSC_BLINKOFF,
                  DEL_FUNCTIONSET);
  // Set the entry mode to increment, noshift
  HD44780_Command(device, CMD_ENTRYMODESET | EMS_INCREMENT | EMS_DISPLAYSHIFTOFF,
                  DEL_ENTRYMODESET);
  // Turn on display, turn on cursor and turn off blinking cursor
  HD44780_Command(device, CMD_DISPLAYCONTROL | DSC_DISPLAYON | DSC_CURSORON | DSC_BLINKOFF,
                  DEL_DISPLAYCONTROL);
  HD44780_clear(device);
}


