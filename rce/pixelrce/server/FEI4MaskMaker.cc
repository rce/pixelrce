#include "server/FEI4MaskMaker.hh"

FEI4MaskMaker::FEI4MaskMaker(std::string enablefile, std::string hitbusfile, const TGWindow *p, UInt_t w, UInt_t h): TGMainFrame(p,w,h), fhmask(0), m_enablefile(enablefile), m_hitbusfile(hitbusfile)
{
  fEcanvas = new TRootEmbeddedCanvas("Ecanvas",this, FrameXSize, FrameYSize*0.8);
  AddFrame(fEcanvas, new TGLayoutHints(kLHintsExpandX |
					      kLHintsExpandY, 10,10,10,1));

  // recusively delete all subframes on exit
  SetCleanup(kDeepCleanup);
  
TGHorizontalFrame *hframe = new TGHorizontalFrame(this,FrameXSize,FrameYSize);

  TGLabel *labelC = new TGLabel(hframe, "col:");
  hframe->AddFrame(labelC, new TGLayoutHints(kLHintsCenterX,5,5,3,4));

  Ecolumn = new TGNumberEntry(hframe, 1,3, 0,
					  TGNumberFormat::kNESInteger,
					  TGNumberFormat::kNEAPositive, 
					  TGNumberFormat::kNELLimitMinMax,1,80);
  Ecolumn->Connect("ValueSet(Long_t)","FEI4MaskMaker",this,"ChangeTarget()");
  hframe->AddFrame(Ecolumn, new TGLayoutHints(kLHintsCenterX,5,5,3,4));

  TGLabel *labelR = new TGLabel(hframe, "row:");
  hframe->AddFrame(labelR, new TGLayoutHints(kLHintsCenterX,5,5,3,4));

  Erow = new TGNumberEntry(hframe, 1,3, 0,
					  TGNumberFormat::kNESInteger,
					  TGNumberFormat::kNEAPositive, 
					  TGNumberFormat::kNELLimitMinMax,1,336);

  Erow   ->Connect("ValueSet(Long_t)","FEI4MaskMaker",this,"ChangeTarget()");
  hframe->AddFrame(Erow, new TGLayoutHints(kLHintsCenterX,5,5,3,4));

  TGLabel *labelV = new TGLabel(hframe, "val:");
  hframe->AddFrame(labelV, new TGLayoutHints(kLHintsCenterX,5,5,3,4));

  Evalue = new TGNumberEntry(hframe, 1,3, 0,
					  TGNumberFormat::kNESInteger,
					  TGNumberFormat::kNEANonNegative, 
					  TGNumberFormat::kNELLimitMinMax,0,1);
  Evalue ->Connect("ValueSet(Long_t)","FEI4MaskMaker",this,"ChangeTarget()");
  hframe->AddFrame(Evalue, new TGLayoutHints(kLHintsCenterX,5,5,3,4));

  
  TGTextButton *setpix = new TGTextButton(hframe,"Set &Pix");
  setpix->Connect("Clicked()","FEI4MaskMaker",this,"SetPixel()");
  hframe->AddFrame(setpix, new TGLayoutHints(kLHintsCenterX,5,5,3,4));

  TGTextButton *setcol = new TGTextButton(hframe,"Set &Col");
  setcol->Connect("Clicked()","FEI4MaskMaker",this,"SetCol()");
  hframe->AddFrame(setcol, new TGLayoutHints(kLHintsCenterX,5,5,3,4));

  TGTextButton *setrow = new TGTextButton(hframe,"Set &Row");
  setrow->Connect("Clicked()","FEI4MaskMaker",this,"SetRow()");
  hframe->AddFrame(setrow, new TGLayoutHints(kLHintsCenterX,5,5,3,4));

  TGTextButton *setall = new TGTextButton(hframe,"Set &All");
  setall->Connect("Clicked()","FEI4MaskMaker",this,"SetAll()");
  hframe->AddFrame(setall, new TGLayoutHints(kLHintsCenterX,5,5,3,4));

  bena = new TGCheckButton(hframe,"Enable");
  hframe->AddFrame(bena, new TGLayoutHints(kLHintsCenterX,15,5,6,4));
  if(m_enablefile==""){
    std::cout<<"This confuration does not include an enable file"<<std::endl;
    bena->SetEnabled(false);
  }else{
    bena->SetOn(true);
  }
  bhit = new TGCheckButton(hframe,"Hitbus");
  hframe->AddFrame(bhit, new TGLayoutHints(kLHintsCenterX,5,5,6,4));
  if(m_hitbusfile==""){
    std::cout<<"This confuration does not include a hitbus file"<<std::endl;
    bhit->SetEnabled(false);
  }else{
    bhit->SetOn(true);
  }

  //  TGTextButton *open = new TGTextButton(hframe,"&Open");
    //open->Connect("Clicked()","FEI4MaskMaker",this,"Open()");
    //hframe->AddFrame(open, new TGLayoutHints(kLHintsCenterX,5,5,3,4));

  //  TGTextButton *saveas = new TGTextButton(hframe,"&Save as");
    //saveas->Connect("Clicked()","FEI4MaskMaker",this,"SaveAs()");
    //hframe->AddFrame(saveas, new TGLayoutHints(kLHintsCenterX,5,5,3,4));

  TGTextButton *exit = new TGTextButton(hframe,"Cancel");
    exit->Connect("Clicked()", "FEI4MaskMaker", this, "Exit()");
  hframe->AddFrame(exit, new TGLayoutHints(kLHintsCenterX,5,5,3,4));

  save = new TGTextButton(hframe,"&Save & Quit");
  save->Connect("Clicked()","FEI4MaskMaker",this,"Save()");
  hframe->AddFrame(save, new TGLayoutHints(kLHintsCenterX,15,5,3,4));
  

  AddFrame(hframe, new TGLayoutHints(kLHintsCenterX,2,2,2,2));
  //  TGHorizontalFrame *hframe = new TGHorizontalFrame(fMain,200,40);

  //  fMain->AddFrame(hframe, new TGLayoutHints(kLHintsCenterX, 2,2,2,2));

  SetWindowName("FEI4MaskMaker");
  MapSubwindows();
  Resize(840,800);

  inPath=m_enablefile;
  outPath=m_enablefile;

  setCol=1;
  setRow=1;
  setValue=1;
  fhmask = new TH2I("fhmask","",80,0.5,80.5,336,0.5,336.5);
  fhmask->SetMinimum(0);
  LoadMask();
  DrawHisto();
  MapWindow();

}

FEI4MaskMaker::~FEI4MaskMaker(){
  Cleanup();
}

void FEI4MaskMaker::Exit(){
  delete fhmask;
  CloseWindow();
}

void FEI4MaskMaker::DrawHisto(){
  TCanvas * fCanvas=fEcanvas->GetCanvas();
  //  fCanvas->SetGrid(1);
  fCanvas->cd();
  fhmask->Draw("colz");
  fCanvas->Update();
}

void FEI4MaskMaker::LoadMask(){
  std::ifstream  inputFile (inPath.c_str()); //"enable_AFP-B1-M11__973.dat");
  std::string line;
  int MaskedPixels=0;
  if (inputFile.is_open()) {
    getline (inputFile,line) ;
    int row=1;
    while ( getline (inputFile,line) ) {
      int col=1;
      for(int ipos = 5; line[ipos]!=0 ; ipos++){
	if((line[ipos]-'0')==0||(line[ipos]-'0')==1){
	  fhmask->SetBinContent(col,row,line[ipos]-'0');
	  //	  cout << fhmask->GetBinContent(col,row);
	  if( fhmask->GetBinContent(col,row) == 0) MaskedPixels++;
	  col++;
	}
      }  
      //      cout << endl;
      row++; 
    }
    inputFile.close();
  }
  std::cout << "Masked Pixels: " << MaskedPixels << std::endl;
  DrawHisto();
}

void FEI4MaskMaker::SetPixel(){
  fhmask->SetBinContent(setCol,setRow,setValue); 
  DrawHisto();
}

void FEI4MaskMaker::SetAll  (){
  for(int icol=1;icol<81;icol++) 
    for (int irow=1;irow<337; irow++)  
      fhmask->SetBinContent(icol,irow,setValue); 
  DrawHisto();
}

void FEI4MaskMaker::SetRow  (){
  for(int icol=1;icol<81;icol++) 
      fhmask->SetBinContent(icol,setRow,setValue); 
  DrawHisto();
}

void FEI4MaskMaker::SetCol  (){
  for (int irow=1;irow<337; irow++)  
      fhmask->SetBinContent(setCol,irow,setValue); 
  DrawHisto();
}

void FEI4MaskMaker::SaveMask(){

  if(bena->IsOn()){
    std::ofstream   outputFile (outPath.c_str()); 
    outputFile <<"###  1     6     11    16     21    26     31    36     41    46     51    56     61    66     71    76\n";
    for(int irow=1; irow<337;irow++){
      if(irow<10) outputFile << "  ";
      else if(irow<100) outputFile << " ";
      outputFile << irow << "  ";
      for(int icol=1;icol<81;icol++){
	outputFile << fhmask->GetBinContent(icol,irow);
	if(!(icol%10)) outputFile << "  ";
	else if(!(icol%5) && icol!=80) outputFile << "-";
      }
      outputFile << '\n';
    }
    outputFile.close();
  }
  if(bhit->IsOn()){
    std::ofstream   outputFile (m_hitbusfile.c_str()); 
    outputFile <<"###  1     6     11    16     21    26     31    36     41    46     51    56     61    66     71    76\n";
    for(int irow=1; irow<337;irow++){
      if(irow<10) outputFile << "  ";
      else if(irow<100) outputFile << " ";
      outputFile << irow << "  ";
      for(int icol=1;icol<81;icol++){
	outputFile << !(fhmask->GetBinContent(icol,irow));
	if(!(icol%10)) outputFile << "  ";
	else if(!(icol%5) && icol!=80) outputFile << "-";
      }
      outputFile << '\n';
    }
    outputFile.close();
    
  }
  
}

void FEI4MaskMaker::Open(){
  TGFileInfo f;
  const char *filetypes[] = {"Mask files", "*.dat", 0, 0};
  f.fFileTypes = filetypes;
  f.fIniDir = StrDup(".");
  new TGFileDialog(gClient->GetRoot(),gClient->GetRoot(),kFDOpen, &f);
  if(f.fFilename){
    std::cout << "Input File:  " << f.fFilename << std::endl;
    inPath= f.fFilename;
    LoadMask();
  }
  save->SetEnabled(kTRUE);
}

void FEI4MaskMaker::Save(){
    SaveMask();
    Emit("Reload()");
    Exit();
}

void FEI4MaskMaker::SaveAs(){
  TGFileInfo f;
  const char *filetypes[] = {"Mask files", "*.dat", 0, 0};
  f.fFileTypes = filetypes;
  f.fIniDir = StrDup(".");
  new TGFileDialog(gClient->GetRoot(),gClient->GetRoot(),kFDSave, &f);
  if(f.fFilename){
    //cout << "Output File:  " << f.fFilename << endl;
    outPath= f.fFilename;
    SaveMask();
    save->SetEnabled(kTRUE);
  }
}

void FEI4MaskMaker::ChangeTarget(){
  setCol   = Ecolumn -> GetIntNumber();
  setRow   = Erow    -> GetIntNumber();
  setValue = Evalue  -> GetIntNumber();
  printf("Selected Pixel: Col %i Row %i \n",setCol,setRow);  
}

