#ifndef PRIMLIST_HH
#define PRIMLIST_HH

#include "PixScan.hh"
#include "ScanOptions.hh"
#include "Callback.hh"
#include "PixScanBase.h"

using namespace ipc;


#include <vector>
#include <list>
#include <map>
#include <string>
#include <assert.h>
#include <sys/time.h>
#include "ScanOptions.hh"


namespace RCE {
  
  class PixScan;
  

  class PrimList //: public PixLib::PixScanBase
{



private:
  std::vector <PixScan*> m_scans; 
  std::vector <ipc::ScanOptions*> m_options; 
  std::vector <std::string> m_scannames;
  std::vector <std::string> m_preScripts; //filepath of shell scripts to be executed before scan
  std::vector <std::string> m_postScripts; //filepath of scripts to be executed after scan
  std::vector <std::vector<std::string> > m_enabled; //enabled frontends
  std::vector <std::vector<std::string> > m_disabled; //disabled frontends
  std::vector <bool> m_updateConfigs;
  std::vector<std::string> m_topConfigs;
  std::vector<int> m_pause;
public:
  //! Constructors
    PrimList() {};

  //! Destructor
  ~PrimList();

  //! Get Functions
  PixScan* getScan(int index);
  ipc::ScanOptions* getScanOptions(int index);
  unsigned nScans(){return m_scans.size();}
  std::string getScanName(int index);
  std::string getPreScript(int index);
  std::string getPostScript(int index);
  std::vector<std::string>& getDisabledList(int index);
  std::vector<std::string>& getEnabledList(int index);
  bool getUpdateConfig(int index);
  void addScan(std::string scanname, PixScan* scan, std::string preScript, std::string postScript, std::vector<std::string> enables, std::vector<std::string> disables, bool updateConfig, std::string topConfig, int pause);
  void clearScans();
  std::string getTopConfig(int index);
  int getPause(int index);
  };

}
#endif

 

