#ifndef FEI4REGISTERTESTTRIGGER_HH
#define FEI4REGISTERTESTTRIGGER_HH

#include <boost/property_tree/ptree_fwd.hpp>
#include "config/AbsTrigger.hh"
#include "HW/BitStream.hh"

namespace FEI4{

  class Fei4RegisterTestTrigger: public AbsTrigger{
  public:
    Fei4RegisterTestTrigger();
    ~Fei4RegisterTestTrigger();
    int configureScan(boost::property_tree::ptree* scanOptions);
    int setupParameter(const char* name, int val);
    int sendTrigger();
    int enableTrigger(bool on);
    int resetCounters();
  private:
    BitStream m_triggerStream;
  };

};

#endif
