#include "config/FEI4/Module.hh"
#include "HW/BitStream.hh"
#include "config/FEI4/FECommands.hh"
#include "config/MaskStageFactory.hh"
#include "config/RceFWRegisters.hh"
#include "HW/SerialIF.hh"
#include <boost/property_tree/ptree.hpp>
#include <iostream>
#include <fstream>
#include <stdio.h>

namespace FEI4{

  Module::Module(const char* name, unsigned id, unsigned inlink, unsigned outlink, AbsFormatter* fmt):
    AbsModule(name, id, inlink, outlink, fmt), m_mode(FRAMED), m_maskStaging(0), m_gAddr(-1), m_oneByOne(false){
    //    std::cout<<"Module"<<std::endl;
  }
  Module::~Module(){
    delete m_maskStaging;
    //m_timer.Print("Module");
  }
  void Module::setupS0S1HitldHW(int s0, int s1, int hitld){
    m_global->setField("S0",s0, GlobalRegister::SW);
    m_global->setField("S1",s1, GlobalRegister::HW); //S0 is in the same register
    if(m_oneByOne)m_global->setField("HITLD_In",hitld, GlobalRegister::HW);
  }
    
  void Module::setupPixelStrobesHW(unsigned bitmask){
    m_global->setField("PxStrobes",bitmask, GlobalRegister::HW);
  }

  //  void Frontend::pixelUsrToRaw(BitStream *bs, unsigned bit){
   // }
  void Module::setVcalCoeff(unsigned i, float val){
    if(i<4)m_vcalCoeff[i]=val;
  }
  void Module::setCapVal(unsigned i, float val){
    if(i<2)m_capVal[i]=val;
  }
  float Module::getVcalCoeff(unsigned i){
    if(i<4)return m_vcalCoeff[i];
    else return -999;
  }
  float Module::getCapVal(unsigned i){
    if(i<2)return m_capVal[i];
    else return -999;
  }

  // TODO: get rid of float 
  const float Module::dacToElectrons(int fe, int dac){
    if(fe!=0)return -1;
    float capacity=6241 * (m_capVal[1]*m_maskStaging->cHigh() + m_capVal[0]*m_maskStaging->cLow());
    float fldac=(float)dac;
    //std::cout<<"dac "<<dac<<" electrons "<<capacity*(m_vcalCoeff[0] + (m_vcalCoeff[1] + (m_vcalCoeff[2] + m_vcalCoeff[3]*fldac)*fldac)*fldac)<<std::endl;
    return capacity*(m_vcalCoeff[0] + (m_vcalCoeff[1] + (m_vcalCoeff[2] + m_vcalCoeff[3]*fldac)*fldac)*fldac);
  }
  const int Module::electronsToDac(float ne) {
    float min=10000.;
    int mini=-1;
    for(int i=0;i<1024;i++) {
      float diff=(ne-dacToElectrons(0, i));
      if(diff<0.) diff=-diff;
      if(diff<min) {
	min=diff;mini=i;
      }
    }   
    if(mini==-1){
      std::cout<<"WARNING:  electronsToDac returning -1 !!"<<std::endl;
    }
    return mini;
  }

  void Module::enableSrHW(bool on){
    //std::cout<<"Switched SR to "<<on<<" for FE "<<getId()<<std::endl;
    m_global->enableSrHW(on);
  }

  void Module::configureHW(){
    //std::cout<<"Configured HW FE "<<getId()<<std::endl;
    //    m_global->printFields(std::cout);
    resetHW();
    m_global->disableThresholdHW(true);
    m_global->writeHW();
    m_global->setField("Colpr_Mode", 0, GlobalRegister::SW); // only write to 1 column pair at a time
    writeDoubleColumnHW(0, PixelRegister::N_PIXEL_REGISTER_BITS-1, 0, PixelRegister::N_COLS/2-1);
    m_global->disableThresholdHW(false);
    //std::ofstream dump("/nfs/cosmicData/out.log");
           //dumpConfig(dump);
            //dump.close();
    //SerialIF::writeRegister(13,1);
  }

  void Module::resetFE(){
    resetHW();
  }
  void Module::resetErrorCountersHW(){
    //std::cout<<"Reset error counters FE "<<getId()<<std::endl;
    BitStream *bs=new BitStream;
    m_commands->globalPulse(bs, FECommands::PULSE_WIDTH);
    setupGlobalPulseHW(GlobalRegister::ReadErrorReq); //Read and clear errors
    SerialIF::send(bs, SerialIF::WAITFORDATA); //global pulse
    setupGlobalPulseHW(0); //Clear
    delete bs;
  }

  void Module::enableDataTakingHW(){
    //std::cout<<"Enabled data taking FE "<<getId()<<std::endl;
    BitStream *bs=new BitStream;
    BitStreamUtils::prependZeros(bs);
    m_commands->switchMode(bs, FECommands::RUN);
    //m_commands->sendECR(bs);
    //m_commands->sendBCR(bs);
    SerialIF::send(bs);
    delete bs;
    SerialIF::writeRegister(35,1);
  }

  void Module::switchToConfigModeHW(){
    //std::cout<<"Switched to Config Mode FE "<<getId()<<std::endl;
    switchModeHW(FECommands::CONF);
  }
    
  void Module::switchModeHW(FECommands::MODE mode){
    BitStream *bs=new BitStream;
    m_commands->switchMode(bs, mode);
    bs->push_back(0);
    SerialIF::send(bs, SerialIF::WAITFORDATA);
    delete bs;
  }  

  void Module::writeDoubleColumnHW(unsigned bitLow, unsigned bitHigh, unsigned dcolLow, unsigned dcolHigh){
    BitStream *bs=new BitStream;
    m_commands->globalPulse(bs, FECommands::PULSE_WIDTH);
    setupS0S1HitldHW(0, 0, 0);
    setupGlobalPulseHW(GlobalRegister::Latch_en); //latch enable
    for (unsigned i=bitLow; i<=bitHigh;i++){
      //set up strobe bit
      setupPixelStrobesHW(1<<i);
      for(unsigned j=dcolLow; j<=dcolHigh; j++){
	//	std::cout<<"Dcol "<<j<<std::endl;
	//set up dcol
	m_global->setField("Colpr_Addr", j, GlobalRegister::HW);
	m_commands->setAddr(m_gAddr|0x8);
	m_pixel->writeDoubleColumnHW(i, j);
	m_commands->setAddr(m_gAddr);
	if(i!=PixelRegisterFields::fieldPos[PixelRegister::diginj]){ //no latching for diginj
	  SerialIF::send(bs, SerialIF::DONT_CLEAR|SerialIF::WAITFORDATA); //global pulse
	}
      }
    }
    setupGlobalPulseHW(0); //clear
    setupPixelStrobesHW(0); //clear
    
    delete bs;
  }


  void Module::setupMaskStageHW(int maskStage) {
    //std::cout<<"Mask stage "<<maskStage<<" FE "<<getId()<<std::endl;
    if(m_oneByOne==false && maskStage==0 && m_global->getField("HITLD_In")==1){ //HITLD_In is a problem with broadcast
      std::cout<<"Module with ID="<<m_id<<" has HITLD_In enabled. Either disable the field or run in one-by-one mode."<<std::endl;
      assert(0);
    }
    //m_global->disableThresholdHW(true);
    m_maskStaging->setupMaskStageHW(maskStage);
    //m_global->disableThresholdHW(false);

  }
    
  //set the value in the register but not in the frontend
  Module::PAR Module::setParameter(const char* name, int val, GlobalRegister::mode t){ 
    std::string par=m_global->lookupParameter(name);
    if(par!=""){ //it's a global variable
      m_global->setField(par.c_str(), val, t);
      return GLOBAL;
    }
    PixelRegister::Field fpar=PixelRegister::lookupParameter(name);
    if(fpar!=PixelRegister::not_found){ //it's a pixel variable
      setPixelRegisterParameterHW(fpar, val, t);
      return PIXEL;
    }
    if(std::string(name)=="CHARGE"){
      m_global->setField("PlsrDAC", electronsToDac(val), t);
      return SPECIAL;
    }
    if (std::string(name)=="NO_PAR"){
      return SPECIAL;
    }
    //we have tested all options
    char error_message[128];
    printf( "parameter %s was not found.",name);
    assert(666==NOT_FOUND);
    return NOT_FOUND;
  }

  void Module::setPixelRegisterParameterHW(PixelRegister::Field field, unsigned val, GlobalRegister::mode t){
    m_pixel->setFieldAll(field, val);
    if(t==GlobalRegister::HW){
      unsigned mode=m_global->getField("Colpr_Mode");
      unsigned addr=m_global->getField("Colpr_Addr");
      m_global->setField("Colpr_Mode", 3, GlobalRegister::SW); // all double columns have the same value, write all at once
      writeDoubleColumnHW(PixelRegisterFields::fieldPos[field], PixelRegisterFields::fieldPos[field+1]-1, 0, 0);
      m_global->setField("Colpr_Mode", mode, GlobalRegister::SW);
      m_global->setField("Colpr_Addr", addr, GlobalRegister::SW);
    }
  }

  // set a parameter (global or MCC) in the frontend
  int Module::setupParameterHW(const char* name, int val){
    //std::cout<<"Setup parameter "<<name<<" val="<<val<<" FE "<<getId()<<std::endl;
    //m_timer.Start();
    int retval=0;
    PAR rv;
    //m_global->disableThresholdHW(true);
    rv=setParameter(name, val, GlobalRegister::HW);
    //m_global->disableThresholdHW(false);
    if(rv==NOT_FOUND)retval=-1;
    //m_timer.Stop();
    return retval;
  }

  int Module::configureScan(boost::property_tree::ptree *scanOptions){
    int retval=0;
    //m_timer.Reset();
    bool isTel=false;
    if(getName().substr(0,9)=="Telescope"){
      std::cout<<"Module "<<getId()<<" is a telescope module"<<std::endl;
      isTel=true;
    }
    try{
      // trigger mode
      std::string triggerMode=scanOptions->get<std::string>("trigOpt.triggerMode");
      //std::cout<<"trigger Mode is "<<triggerMode<<std::endl;
      int xselftrg=0; // 1 for module crosstalk
      if(triggerMode=="MIXED"){
	int moduleTrgMask=scanOptions->get<int>("trigOpt.moduleTrgMask");
	xselftrg=(moduleTrgMask&(1<<getOutLink()))!=0;
	std::cout<<"xselftrg "<<xselftrg<<" "<<moduleTrgMask<<" "<<getOutLink()<<std::endl;
	setParameter("GateHitOr",xselftrg,GlobalRegister::SW);
	if(xselftrg==1)std::cout<<"Configuring module "<<getId()<<" with SELFTRIGGER"<<std::endl;
	else std::cout<<"Configuring module "<<getId()<<" with regular trigger"<<std::endl;
      } else if(triggerMode=="INTERNAL_SELF") setParameter("GateHitOr",1,GlobalRegister::SW);
      else setParameter("GateHitOr",0,GlobalRegister::SW);
      int diginject=scanOptions->get("trigOpt.optionsMask.DIGITAL_INJECT", 0);
      if(diginject)setParameter("DIGHITIN_Sel",1,GlobalRegister::SW);
      else setParameter("DIGHITIN_Sel",0,GlobalRegister::SW);

      std::string type = scanOptions->get<std::string>("scanType");
      if(type=="AtlasData"){
        RceFWRegisters fw;
        fw.setHitDiscConfig(0, getOutLink(), m_global->getField("HitDiscCnfg"));
      }
      int override = scanOptions->get("trigOpt.optionsMask.OVERRIDE_LATENCY", 0);
      if(type!="CosmicData" || override==true){ //For cosmic data the latency comes from a file unless override is set.
	if(!xselftrg) {
	  if(isTel)setParameter("TrigLat",scanOptions->get<int>("trigOpt.Lvl1_Latency_Secondary"),GlobalRegister::SW);
	  else setParameter("TrigLat",scanOptions->get<int>("trigOpt.Lvl1_Latency"),GlobalRegister::SW);
	} else {
	  setParameter("TrigLat",scanOptions->get<int>("trigOpt.Lvl1_Latency_Secondary"),GlobalRegister::SW);
	  std::cout<<"Configured with secondary latency "<<scanOptions->get<int>("trigOpt.Lvl1_Latency_Secondary")<<std::endl;
	}	
	int nev;
	if(isTel)nev=scanOptions->get<int>("trigOpt.nTriggersPerGroup");
	else nev=scanOptions->get<int>("trigOpt.nL1AperEvent");
	if(nev==16)nev=0;
	setParameter("TrigCnt",nev ,GlobalRegister::SW); 
	if(override==true){
	  if(isTel){
	    std::cout<<"Module "<<getId()<<": Overriding latency with "<< 
	      scanOptions->get<int>("trigOpt.Lvl1_Latency_Secondary")<<std::endl;
	    std::cout<<"Module "<<getId()<<": Overriding conseq triggers with "<< 
	      scanOptions->get<int>("trigOpt.nTriggersPerGroup")<<std::endl;
	  }else{
	    std::cout<<"Module "<<getId()<<": Overriding latency with "<< 
	      scanOptions->get<int>("trigOpt.Lvl1_Latency")<<std::endl;
	    std::cout<<"Module "<<getId()<<": Overriding conseq triggers with "<< 
	      scanOptions->get<int>("trigOpt.nL1AperEvent")<<std::endl;
	  }
	}
      }
      setParameter("CMDcnt_delay",scanOptions->get<int>("trigOpt.strobeMCCDelay"),GlobalRegister::SW);
      setParameter("CMDcnt_width",scanOptions->get<int>("trigOpt.strobeDuration"),GlobalRegister::SW);
      //setParameter("enables",0xffff);
      // Global regs
      std::string stagingMode=scanOptions->get<std::string>("stagingMode");
      std::string maskStages=scanOptions->get<std::string>("maskStages");
      MaskStageFactory msf;
      delete m_maskStaging;
      m_maskStaging=msf.createMaskStaging(this, maskStages.c_str(), stagingMode.c_str());
      // set Vcal
      std::string par="PlsrDAC";
      if(scanOptions->get("trigOpt.optionsMask.SPECIFY_CHARGE_NOT_VCAL", 0)==1)par="CHARGE";
      setParameter(par.c_str(),scanOptions->get<int>("trigOpt.vcal_charge"),GlobalRegister::SW);
      m_oneByOne=scanOptions->get("trigOpt.optionsMask.ONE_BY_ONE", 0);
      if(scanOptions->get("trigOpt.optionsMask.SETUP_THRESHOLD", 0)==1){
	unsigned short threshold=scanOptions->get<int>("trigOpt.threshold");
	std::cout<<"Setting threshold to "<<threshold<<std::endl;
	setParameter("Vthin_AltFine", threshold, GlobalRegister::SW);
      }
      if(scanOptions->get<std::string>("DataProc")=="RawFei4Occupancy"){
	std::cout<<m_id<<": RawFei4Occupancy dataproc. Setting HitDiscCnfg to 0"<<std::endl;
	setParameter("HitDiscCnfg", 0, GlobalRegister::SW);
      }
      if(scanOptions->get("trigOpt.optionsMask.CLEAR_MASKS", 0)==1){
	m_pixel->setBitAll(PixelRegister::enable, 1);
	m_pixel->setBitAll(PixelRegister::hitbus, 1);
      }
    }
    catch(boost::property_tree::ptree_bad_path ex){
      retval=1;
      std::cout<<"ERROR: Bad ptree param "<<ex.what()<<std::endl;
    }
    return retval;
  }


  ModuleInfo Module::getModuleInfo(){
    return ModuleInfo(m_name, m_id,m_inLink, m_outLink,Module::N_FRONTENDS,PixelRegister::N_ROWS,PixelRegister::N_COLS, m_formatter);
  }

  void Module::dumpConfig(std::ostream &os){
    m_global->printFields(os);
    m_pixel->dumpPixelRegister(os);
     os<<"Charge injection parameters:"<<std::endl;
     os<<"----------------------------"<<std::endl;
     os<<"vcalCoeff[0]: "<<getVcalCoeff(0)<<std::endl;
     os<<"vcalCoeff[1]: "<<getVcalCoeff(1)<<std::endl;
     os<<"vcalCoeff[2]: "<<getVcalCoeff(2)<<std::endl;
     os<<"vcalCoeff[3]: "<<getVcalCoeff(3)<<std::endl;
     os<<"capValLo: "<<getCapVal(0)<<std::endl;
     os<<"capValHi: "<<getCapVal(1)<<std::endl;
  }

};

