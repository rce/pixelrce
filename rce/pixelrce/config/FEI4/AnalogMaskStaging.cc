#include "config/FEI4/AnalogMaskStaging.hh"
#include "config/FEI4/Module.hh"

namespace FEI4{
  
  AnalogMaskStaging::AnalogMaskStaging(FEI4::Module* mod, Masks masks, std::string type)
    :MaskStaging<FEI4::Module>(mod, masks, type), m_oldDcolStage(-1){
    if(type=="FEI4_COL_ANL_40x8")m_nStages=6;
    else m_nStages=3;
  }
  
  void AnalogMaskStaging::setupMaskStageHW(int maskStage){
    int dcolStage=maskStage/m_nStages;
    int stage=maskStage%m_nStages;
    if(m_initialized==false || maskStage==0 || dcolStage!=m_oldDcolStage){
      clearBitsHW();
      m_initialized=true;
      m_oldDcolStage=dcolStage;
    }
    PixelRegister* pixel=m_module->pixel();
    GlobalRegister* global=m_module->global();
    global->setField("Colpr_Mode", 0, GlobalRegister::SW); 
    for(int i=0;i<PixelRegister::N_PIXEL_REGISTER_BITS;i++){
      if(m_masks.iMask&(1<<i)){
	pixel->setupMaskStageCol(dcolStage*2+1, i, stage, m_nStages);
	if(dcolStage==39)pixel->setupMaskStageCol(80, i, stage, m_nStages);
	m_module->writeDoubleColumnHW(i, i, dcolStage, dcolStage); 
	if(dcolStage!=0){
	  pixel->setupMaskStageCol(dcolStage*2, i, stage, m_nStages);
	  m_module->writeDoubleColumnHW(i, i, dcolStage-1, dcolStage-1); 
	  global->setField("Colpr_Addr", dcolStage, GlobalRegister::HW); 
	}
      }
    }
  }
}
