#ifndef FEI4NOISEMASKSTAGING_HH
#define FEI4NOISEMASKSTAGING_HH

#include "config/Masks.hh"
#include "config/MaskStaging.hh"

namespace FEI4{

  class Module;
  
  class NoiseMaskStaging: public MaskStaging<FEI4::Module>{
  public:
    NoiseMaskStaging(FEI4::Module* module, Masks masks, std::string type);
    void setupMaskStageHW(int maskStage);
  private:
    
  };
  
}
#endif
