#include "config/FEI4/MaskStaging26880.hh"
#include "config/FEI4/Module.hh"

namespace FEI4{
  
  MaskStaging26880::MaskStaging26880(FEI4::Module* mod, Masks masks, std::string type)
    :MaskStaging<FEI4::Module>(mod, masks, type){
  }
  
  void MaskStaging26880::setupMaskStageHW(int maskStage){
    if(m_initialized==false || maskStage==0){
      clearBitsHW();
      m_initialized=true;
    }
    PixelRegister* pixel=m_module->pixel();
    GlobalRegister* global=m_module->global();
    unsigned rowstage=maskStage%PixelRegister::N_ROWS;
    unsigned colstage=maskStage/PixelRegister::N_ROWS;
    global->setField("Colpr_Mode", 0, GlobalRegister::SW); 
    for(int i=0;i<PixelRegister::N_PIXEL_REGISTER_BITS;i++){
      if(m_masks.iMask&(1<<i)){
	if(rowstage==0&&colstage!=0){
	  pixel->setBitCol(colstage, i, 0);
	  m_module->writeDoubleColumnHW(i, i, (colstage-1)/2, (colstage-1)/2); 
	}
	pixel->setupMaskStageCol(colstage+1, i, rowstage, PixelRegister::N_ROWS);
	//	  m_pixel->dumpDoubleColumn(i,colstage/2,std::cout);
	m_module->writeDoubleColumnHW(i, i, colstage/2, colstage/2); 
      }
    }
    global->setField("Colpr_Mode", 3, GlobalRegister::SW); 
  }
}
