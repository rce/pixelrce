#ifndef FEI4BCONFIGFILE_HH
#define FEI4BCONFIGFILE_HH

#include "PixelFEI4BConfig.hh"
#include <string>
#include <util/json.hpp>
#include <map>

class FEI4BConfigFile {
public:
  FEI4BConfigFile(){}
  ~FEI4BConfigFile();
  void readModuleConfig(ipc::PixelFEI4BConfig* cfg, std::string filename);
  void writeModuleConfig(ipc::PixelFEI4BConfig* cfg, const std::string &base, const std::string &confdir, const std::string &configname, const std::string &key);
  void writeMaskFile(const long int bit, ipc::PixelFEI4BConfig* config, const std::string &filename);
  void writeDacFile(unsigned char trim[][ipc::IPC_N_I4_PIXEL_ROWS] , const std::string filename);
  void dump(const ipc::PixelFEI4BConfig& cfg);
  std::string getHitbusMaskFile(){return m_hitbusfile;}
  std::string getEnableMaskFile(){return m_enablefile;}
  void toJSON(const ipc::PixelFEI4BConfig &config,json &j);
  void fromJSON(const json &j,ipc::PixelFEI4BConfig &);
private:
  std::string getFullPath(std::string relPath);
  void setupDAC(unsigned char trim[][ipc::IPC_N_I4_PIXEL_ROWS] , std::string par);
  std::string setupMaskBit(const long int bit, ipc::PixelFEI4BConfig *cfg, std::string par);
  void dumpMask(const long int bit, const ipc::PixelFEI4BConfig& cfg);
  unsigned short lookupToUShort(std::string par);
  int convertToUShort(std::string par, unsigned short& val);
  float lookupToFloat(std::string par);
  void dumpDac(const unsigned char trim[][ipc::IPC_N_I4_PIXEL_ROWS]);
  std::string m_moduleCfgFilePath;
  std::ifstream *m_moduleCfgFile;
  std::map<std::string, std::string> m_params;
  std::string m_enablefile, m_hitbusfile;
};

#endif
