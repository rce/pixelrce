
#include "config/FEI4/FEI4BGlobalRegister.hh"
#include "config/FEI4/FECommands.hh"
#include "config/FEI4/Utils.hh"
#include "HW/SerialIF.hh"
#include <iostream>
#include <stdio.h>


namespace FEI4{

  RegisterDef FEI4BGlobalRegister::m_def;
  bool FEI4BGlobalRegister::m_initialized=false;

  FEI4BGlobalRegister::FEI4BGlobalRegister(FECommands* commands): 
    GlobalRegister(commands) {
    m_rd=&m_def;
    m_def.LOW_REG=1;
    m_def.N_REG_REGS=36; 
    m_def.N_W_REGS=36; 
    m_def.N_R_REGS=43; 
    m_def.SRAB_BITS=288; 
    m_def.SRC_BITS=144;    
    // initialize static field dictionary
    if(!m_initialized)initialize();
    //set up size parameters
    
    //create registers
    m_regw=new unsigned short[m_def.N_W_REGS];
    m_regr=new unsigned short[m_def.N_R_REGS];
    m_srab=new unsigned short[m_def.SRAB_BITS/16];
    m_src=new unsigned short[m_def.SRC_BITS/16];
    //Clear all registers
    for(int i=0;i<m_def.N_W_REGS;i++)m_regw[i]=0;
    for(int i=0;i<m_def.N_R_REGS;i++)m_regr[i]=0;
    for(int i=0;i<m_def.SRAB_BITS/16;i++)m_srab[i]=0;
    for(int i=0;i<m_def.SRC_BITS/16;i++)m_src[i]=0;
  }
  FEI4BGlobalRegister::~FEI4BGlobalRegister(){
    delete [] m_regw;
    delete [] m_regr;
    delete [] m_srab;
    delete [] m_src;
  }

  void FEI4BGlobalRegister::initialize(){
    addField("TrigCnt", 2, 12, -1, -1, 4, false);
    addField("Conf_AddrEnable", 2, 11, -1, -1, 1, false);
    addField("Reg2Spare", 2, 0, -1, -1, 11, false);
    addField("ErrMask0", 3, 0, -1, -1, 16, false);
    addField("ErrMask1", 4, 0, -1, -1, 16, false);
    addField("PrmpVbpRight", 5, 8, 152, -1, 8, true);
    addField("BufVgOpAmp", 5, 0, 144, -1, 8, true);
    addField("Reg6Spare", 6, 8, -1, -1, 8, true);
    addField("PrmpVbp", 6, 0, 160, -1, 8, true);
    addField("TdacVbp", 7, 8, 184, -1, 8, true);
    addField("DisVbn", 7, 0, 176, -1, 8, true);
    addField("Amp2Vbn", 8, 8, 200, -1, 8, true);
    addField("Amp2VbpFol", 8,0,192, -1, 8, true);
    addField("Reg9Spare", 9, 8, -1, -1, 8, true);
    addField("Amp2Vbp", 9, 0, 208, -1, 8, true);
    addField("FdacVbn", 10, 8, 232, -1, 8, true);
    addField("Amp2Vbpf", 10, 0, 224, -1, 8, true);
    addField("PrmpVbnFol", 11, 8, 248, -1, 8, true);
    addField("PrmpVbpLeft", 11, 0, 240, -1, 8, true);
    addField("PrmpVbpf", 12, 8, 264, -1, 8, true);
    addField("PrmpVbnLcc", 12, 0, 256, -1, 8, true);
    addField("Reg13Spare", 13, 0, -1, -1, 1, false);
    addField("PxStrobes", 13, 1, 274, -1, 13, true);
    addField("S0", 13, 14, 273, -1, 1, false);
    addField("S1", 13, 15, 272, -1, 1, false);
    addField("LVDSDrvIref", 14, 8, 8, -1, 8, true);
    addField("GADCOpAmp", 14, 0, 0, -1, 8, true);
    addField("PllIbias", 15, 8, 24, -1, 8, true);
    addField("LVDSDrvVos", 15, 0, 16, -1, 8, true);
    addField("TempSensBias", 16, 8, 40, -1, 8, true);
    addField("PllIcp", 16, 0, 32, -1, 8, true);
    addField("Reg17Spare", 17, 8, -1, -1, 8, true);
    addField("PlsrIdacRamp", 17, 0, 48, -1, 8, true);
    addField("VrefDigTune", 18, 8, 72, -1, 8, true);
    addField("PlsrVgOPamp", 18, 0, 64, -1, 8, true);
    addField("PlsrDacBias", 19, 8, 88, -1, 8, true);
    addField("VrefAnTune", 19, 0, 80, -1, 8, true);
    addField("Vthin_AltCoarse", 20, 8, 104, -1, 8, true);
    addField("Vthin_AltFine", 20, 0, 96, -1, 8, true);
    addField("PlsrDAC", 21, 0, 112, -1, 10, true);
    addField("DIGHITIN_Sel", 21, 10, 143, -1, 1, false);
    addField("DINJ_Override", 21, 11, 142, -1, 1, false);
    addField("HITLD_In", 21, 12, 141, -1, 1, false);
    addField("Reg21Spare", 21, 13, -1, -1, 3, false);
    addField("Reg22Spare2", 22, 0, -1, -1, 2, false);
    addField("Colpr_Addr", 22, 2, 130, -1, 6, true);
    addField("Colpr_Mode", 22, 8, 128, -1, 2, true);
    addField("Reg22Spare1", 22, 10, -1, -1, 6, false);
    addField("DisableColumnCnfg0", 23, 0, -1, 0, 16, false);
    addField("DisableColumnCnfg1", 24, 0, -1, 16, 16, false);
    addField("DisableColumnCnfg2", 25, 0, -1, 32, 8, false);
    addField("TrigLat", 25, 8, -1, 40, 8, false);
    addField("CMDcnt", 26, 3, -1, 51, 14, false);
    addField("StopModeCnfg", 26, 2, -1, 50, 1, false);
    addField("HitDiscCnfg", 26, 0, -1, 48, 2, false);
    addField("EN_PLL", 27, 15, -1, 79, 1, false);
    addField("Efuse_sense", 27, 14, -1, 78, 1, false);
    addField("Stop_Clk", 27, 13, -1, 77, 1, false);
    addField("ReadErrorReq", 27, 12, -1, 76, 1, false);
    addField("Reg27Spare1", 27, 11, -1, -1, 1, false);
    addField("GADC_Enable", 27, 10, -1, 74, 1, false);
    addField("ShiftReadBack", 27, 9, -1, 73, 1, false);
    addField("Reg27Spare2", 27, 6, -1, -1, 3, false);
    addField("GateHitOr", 27, 5, -1, 69, 1, false);
    addField("CalEn", 27, 4, -1, 68, 1, false);
    addField("SR_clr", 27, 3, -1, 67, 1, false);
    addField("Latch_en", 27, 2, -1, 66, 1, false);
    addField("SR_Clock", 27, 1, -1, 65, 1, false);
    addField("LVDSDrvSet06", 28, 15, -1, 95, 1, false);
    addField("Reg28Spare", 28, 10, -1, -1, 5, false);
    addField("EN40M", 28, 9, -1, 89, 1, false);
    addField("EN80M", 28, 8, -1, 88, 1, false);
    addField("CLK1", 28, 5, -1, 85, 3, false);
    addField("CLK0", 28, 2, -1, 82, 3, false);
    addField("EN160M", 28, 1, -1, 81, 1, false);
    addField("EN320M", 28, 0, -1, 80, 1, false);
    addField("Reg29Spare1", 29, 14, -1, -1, 2, false);
    addField("no8b10b", 29, 13, -1, 109, 1, false);
    addField("Clk2OutCnfg", 29, 12, -1, 108, 1, false);
    addField("EmptyRecord", 29, 4, -1, 100, 8, true);
    addField("Reg29Spare2", 29, 3, -1, -1, 1, false);
    addField("LVDSDrvEn", 29, 2, -1, 98, 1, false);
    addField("LVDSDrvSet30", 29, 1, -1, 97, 1, false);
    addField("LVDSDrvSet12", 29, 0, -1, 96, 1, false);
    addField("TempSensDiodeSel", 30, 14, -1, 126, 2, true);
    addField("TempSensDisable", 30, 13, -1, 125, 1, false);
    addField("IleakRange", 30, 12, -1, 124, 1, false);
    addField("Reg30Spare", 30, 0, -1, -1, 12, false);
    addField("PlsrRiseUpTau", 31, 13, -1, 141, 3, false);
    addField("PlsrPwr", 31, 12, -1, 140, 1, false);
    addField("PlsrDelay", 31, 6, -1, 134, 6, true);
    addField("ExtDigCalSW", 31, 5, -1, 133, 1, false);
    addField("ExtAnaCalSW", 31, 4, -1, 132, 1, false);
    addField("Reg31Spare", 31, 3, -1, -1, 1, false);
    addField("GADCSel", 31, 3, -1, 128, 1, false);
    addField("SELB0", 32, 0, -1, -1, 16, false);
    addField("SELB1", 33, 0, -1, -1, 16, false);
    addField("SELB2", 34, 8, -1, -1, 8, false);
    addField("Reg34Spare1", 34, 5, -1, -1, 3, false);
    addField("PrmpVbpMsnEn", 34, 4, -1, -1, 1, false);
    addField("Reg34Spare2", 34, 0, -1, -1, 4, false);
    addField("Chip_SN", 35, 0, -1, -1, 16, false);
    addField("Reg1Spare", 1, 9, -1, -1, 7, false);
    addField("SmallHitErase", 1, 8, -1, -1, 1, false);
    addField("Eventlimit", 1, 0, -1, -1, 8, true);

    // alternative definitions
    addField("CMDcnt_width", 26, 3, -1, 51, 8, false);
    addField("CMDcnt_delay", 26, 11, -1, 59, 6, false);
    addField("ERRORMASK", 3, 0, -1, -1, 32, false);
    addField("DisableColumnCnfg01", 23, 0, -1, 0, 32, false);
    addField("SELB01", 32, 0, -1, -1, 32, false);
    addField("CLK0_S2", 28, 2, -1, 82, 1, false);
    addField("CLK0_S1", 28, 3, -1, 83, 1, false);
    addField("CLK0_S0", 28, 4, -1, 84, 1, false);
    addField("CLK1_S2", 28, 5, -1, 85, 1, false);
    addField("CLK1_S1", 28, 6, -1, 86, 1, false);
    addField("CLK1_S0", 28, 7, -1, 87, 1, false);
    
    
    // Translation from scan parameter to Global register field
    addParameter("LATENCY", "TrigLat");
    addParameter("GDAC", "Vthin_AltFine");
    addParameter("GDAC_COARSE", "Vthin_AltCoarse");
    addParameter("VCAL", "PlsrDAC");
    addParameter("STROBE_DELAY", "PlsrDelay");
    addParameter("IF", "PrmpVbpf");
    addParameter("TEMP", "TempSensDiodeSel");

    //Translation from CLKx_S012 parameter to transmission rate
    m_def.m_rate[0]=SerialIF::M40;
    m_def.m_rate[4]=SerialIF::M320;
    m_def.m_rate[2]=SerialIF::M40;
    m_def.m_rate[6]=SerialIF::M320;
    m_def.m_rate[1]=SerialIF::M160;
    m_def.m_rate[5]=SerialIF::M80;
    m_def.m_rate[3]=SerialIF::M40;
    m_def.m_rate[7]=SerialIF::M160; //Aux clk

    m_def.m_params8b10b=m_def.m_fields["no8b10b"];
    m_def.m_paramsDrate=m_def.m_fields["CLK0"];
    m_def.m_colpr_reg=m_def.m_fields["Colpr_Addr"].reg;
    m_def.m_colpr_disable=5<<2;

    m_initialized=true;
  }
  void FEI4BGlobalRegister::printFields(std::ostream &os){
    os<<"Global Register Fields"<<std::endl;
    os<<"======================"<<std::endl;
    os<<"TrigCnt "<<getField("TrigCnt")<<std::endl;
    os<<"Conf_AddrEnable "<<getField("Conf_AddrEnable")<<std::endl;
    os<<"ErrMask0 "<<getField("ErrMask0")<<std::endl;
    os<<"ErrMask1 "<<getField("ErrMask1")<<std::endl;
    os<<"PrmpVbpRight "<<getField("PrmpVbpRight")<<std::endl;
    os<<"BufVgOpAmp "<<getField("BufVgOpAmp")<<std::endl;
    os<<"PrmpVbp "<<getField("PrmpVbp")<<std::endl;
    os<<"TdacVbp "<<getField("TdacVbp")<<std::endl;
    os<<"DisVbn "<<getField("DisVbn")<<std::endl;
    os<<"Amp2Vbn "<<getField("Amp2Vbn")<<std::endl;
    os<<"Amp2VbpFol "<<getField("Amp2VbpFol")<<std::endl;
    os<<"Amp2Vbp "<<getField("Amp2Vbp")<<std::endl;
    os<<"FdacVbn "<<getField("FdacVbn")<<std::endl;
    os<<"Amp2Vbpf "<<getField("Amp2Vbpf")<<std::endl;
    os<<"PrmpVbnFol "<<getField("PrmpVbnFol")<<std::endl;
    os<<"PrmpVbpLeft "<<getField("PrmpVbpLeft")<<std::endl;
    os<<"PrmpVbpf "<<getField("PrmpVbpf")<<std::endl;
    os<<"PrmpVbnLcc "<<getField("PrmpVbnLcc")<<std::endl;
    os<<"PxStrobes "<<getField("PxStrobes")<<std::endl;
    os<<"S0 "<<getField("S0")<<std::endl;
    os<<"S1 "<<getField("S1")<<std::endl;
    os<<"LVDSDrvIref "<<getField("LVDSDrvIref")<<std::endl;
    os<<"GADCOpAmp "<<getField("GADCOpAmp")<<std::endl;
    os<<"PllIbias "<<getField("PllIbias")<<std::endl;
    os<<"LVDSDrvVos "<<getField("LVDSDrvVos")<<std::endl;
    os<<"TempSensBias "<<getField("TempSensBias")<<std::endl;
    os<<"PllIcp "<<getField("PllIcp")<<std::endl;
    os<<"PlsrIdacRamp "<<getField("PlsrIdacRamp")<<std::endl;
    os<<"VrefDigTune "<<getField("VrefDigTune")<<std::endl;
    os<<"PlsrVgOPamp "<<getField("PlsrVgOPamp")<<std::endl;
    os<<"PlsrDacBias "<<getField("PlsrDacBias")<<std::endl;
    os<<"VrefAnTune "<<getField("VrefAnTune")<<std::endl;
    os<<"Vthin_AltCoarse "<<getField("Vthin_AltCoarse")<<std::endl;
    os<<"Vthin_AltFine "<<getField("Vthin_AltFine")<<std::endl;
    os<<"PlsrDAC "<<getField("PlsrDAC")<<std::endl;
    os<<"DIGHITIN_Sel "<<getField("DIGHITIN_Sel")<<std::endl;
    os<<"DINJ_Override "<<getField("DINJ_Override")<<std::endl;
    os<<"HITLD_In "<<getField("HITLD_In")<<std::endl;
    os<<"Colpr_Addr "<<getField("Colpr_Addr")<<std::endl;
    os<<"Colpr_Mode "<<getField("Colpr_Mode")<<std::endl;
    os<<"DisableColumnCnfg0 "<<getField("DisableColumnCnfg0")<<std::endl;
    os<<"DisableColumnCnfg1 "<<getField("DisableColumnCnfg1")<<std::endl;
    os<<"DisableColumnCnfg2 "<<getField("DisableColumnCnfg2")<<std::endl;
    os<<"TrigLat "<<getField("TrigLat")<<std::endl;
    os<<"CMDcnt "<<getField("CMDcnt")<<std::endl;
    os<<"StopModeCnfg "<<getField("StopModeCnfg")<<std::endl;
    os<<"HitDiscCnfg "<<getField("HitDiscCnfg")<<std::endl;
    os<<"EN_PLL "<<getField("EN_PLL")<<std::endl;
    os<<"Efuse_sense "<<getField("Efuse_sense")<<std::endl;
    os<<"Stop_Clk "<<getField("Stop_Clk")<<std::endl;
    os<<"ReadErrorReq "<<getField("ReadErrorReq")<<std::endl;
    os<<"GADC_Enable "<<getField("GADC_Enable")<<std::endl;
    os<<"ShiftReadBack "<<getField("ShiftReadBack")<<std::endl;
    os<<"GateHitOr "<<getField("GateHitOr")<<std::endl;
    os<<"CalEn "<<getField("CalEn")<<std::endl;
    os<<"SR_clr "<<getField("SR_clr")<<std::endl;
    os<<"Latch_en "<<getField("Latch_en")<<std::endl;
    os<<"SR_Clock "<<getField("SR_Clock")<<std::endl;
    os<<"LVDSDrvSet06 "<<getField("LVDSDrvSet06")<<std::endl;
    os<<"EN40M "<<getField("EN40M")<<std::endl;
    os<<"EN80M "<<getField("EN80M")<<std::endl;
    os<<"CLK1 "<<getField("CLK1")<<std::endl;
    os<<"CLK0 "<<getField("CLK0")<<std::endl;
    os<<"EN160M "<<getField("EN160M")<<std::endl;
    os<<"EN320M "<<getField("EN320M")<<std::endl;
    os<<"no8b10b "<<getField("no8b10b")<<std::endl;
    os<<"Clk2OutCnfg "<<getField("Clk2OutCnfg")<<std::endl;
    os<<"EmptyRecord "<<getField("EmptyRecord")<<std::endl;
    os<<"LVDSDrvEn "<<getField("LVDSDrvEn")<<std::endl;
    os<<"LVDSDrvSet30 "<<getField("LVDSDrvSet30")<<std::endl;
    os<<"LVDSDrvSet12 "<<getField("LVDSDrvSet12")<<std::endl;
    os<<"TempSensDiodeSel "<<getField("TempSensDiodeSel")<<std::endl;
    os<<"TempSensDisable "<<getField("TempSensDisable")<<std::endl;
    os<<"IleakRange "<<getField("IleakRange")<<std::endl;
    os<<"PlsrRiseUpTau "<<getField("PlsrRiseUpTau")<<std::endl;
    os<<"PlsrPwr "<<getField("PlsrPwr")<<std::endl;
    os<<"PlsrDelay "<<getField("PlsrDelay")<<std::endl;
    os<<"ExtDigCalSW "<<getField("ExtDigCalSW")<<std::endl;
    os<<"ExtAnaCalSW "<<getField("ExtAnaCalSW")<<std::endl;
    os<<"GADCSel "<<getField("GADCSel")<<std::endl;
    os<<"SELB0 "<<getField("SELB0")<<std::endl;
    os<<"SELB1 "<<getField("SELB1")<<std::endl;
    os<<"SELB2 "<<getField("SELB2")<<std::endl;
    os<<"PrmpVbpMsnEn "<<getField("PrmpVbpMsnEn")<<std::endl;
    os<<"Chip_SN "<<getField("Chip_SN")<<std::endl;
    os<<"SmallHitErase "<<getField("SmallHitErase")<<std::endl;
    os<<"Eventlimit "<<getField("Eventlimit")<<std::endl;

    os<<"Alternative Field definitions:"<<std::endl;
    // alternative definitions
    os<<"CMDcnt_width "<<getField("CMDcnt_width")<<std::endl;
    os<<"CMDcnt_delay "<<getField("CMDcnt_delay")<<std::endl;
    os<<"ERRORMASK "<<getField("ERRORMASK")<<std::endl;
    os<<"DisableColumnCnfg01 "<<getField("DisableColumnCnfg01")<<std::endl;
    os<<"SELB01 "<<getField("SELB01")<<std::endl;
    os<<"CLK0_S2 "<<getField("CLK0_S2")<<std::endl;
    os<<"CLK0_S1 "<<getField("CLK0_S1")<<std::endl;
    os<<"CLK0_S0 "<<getField("CLK0_S0")<<std::endl;
    os<<"CLK1_S2 "<<getField("CLK1_S2")<<std::endl;
    os<<"CLK1_S1 "<<getField("CLK1_S1")<<std::endl;
    os<<"CLK1_S0 "<<getField("CLK1_S0")<<std::endl;
  }

};
