
#include "config/MeasurementTrigger.hh"
#include <boost/property_tree/ptree.hpp>
#include <regex>
#include "config/TriggerReceiverIF.hh"
#include <iostream>
#include <fstream>
#include <stdlib.h>


MeasurementTrigger::MeasurementTrigger():AbsTrigger(){
    //std::cout<<"Measurement trigger"<<std::endl;
  }
  
int MeasurementTrigger::configureScan(boost::property_tree::ptree* scanOptions){
  // number of triggers
  m_i=0;
  return 0;
}
  
int MeasurementTrigger::sendTrigger(){
  //trigger the measurement
  TriggerReceiverIF::receive(0, 0);
  m_i++;
  return 0;
}


