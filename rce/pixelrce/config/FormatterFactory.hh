#ifndef FORMATTERFACTORY_HH
#define FORMATTERFACTORY_HH

class AbsFormatter;

class FormatterFactory {
public:
  AbsFormatter* createFormatter(const char* formatter, int id);
};


#endif
