#ifndef ABSFORMATTER_HH
#define ABSFORMATTER_HH

#include <vector>
#include <string>
#include <boost/property_tree/ptree_fwd.hpp>

class AbsFormatter{
public:
  AbsFormatter(int id, const char* name=""):m_rb(0), m_name(name), m_id(id){}
  virtual ~AbsFormatter(){}
  virtual int decode(const unsigned* data, int size, unsigned* parsedData, int &parsedsize, int &nL1A)=0;
  virtual void configure(boost::property_tree::ptree* scanOptions){};
  virtual void setReadbackPointer(std::vector<unsigned>* p){m_rb=p;}
  const char* getName(){return m_name.c_str();}
  int getId(){return m_id;}
protected:
  std::vector<unsigned> *m_rb;
  std::string m_name;
  int m_id;
};
#endif
