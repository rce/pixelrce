#include "config/RCFModuleFactory.hh"
#include "config/FormatterFactory.hh"
#include "config/FEI3/Module.hh"
#include "config/FEI3/RCFModule.hh"
#include "config/FEI4/RCFFEI4AModule.hh"
#include "config/FEI4/RCFFEI4BModule.hh"
#include "config/hitbus/RCFModule.hh"
#include "config/afp-hptdc/RCFModule.hh"
#include "config/Trigger.hh"
#include "config/MultiTrigger.hh"
#include "config/EventFromFileTrigger.hh"
#include "config/MeasurementTrigger.hh"
#include "config/MultiShotTrigger.hh"
#include "config/HitorTrigger.hh"
#include "config/FEI4/TemperatureTrigger.hh"
#include "config/FEI4/MonleakTrigger.hh"
#include "config/FEI4/SNTrigger.hh"
#include "config/FEI4/Fei4RegisterTestTrigger.hh"
#include "util/exceptions.hh"
#include "stdio.h"
#include <iostream>

RCFModuleFactory::RCFModuleFactory(RCF::RcfServer &server):ModuleFactory(), m_server(server){
  m_modulegroups.push_back(&m_modgroupi3);
  m_modulegroups.push_back(&m_modgroupi4a);
  m_modulegroups.push_back(&m_modgroupi4b);
  m_modulegroups.push_back(&m_modgrouphitbus);
  m_modulegroups.push_back(&m_modgroupafphptdc);
}


AbsModule* RCFModuleFactory::createModule(const char* name, const char* type, unsigned id, unsigned inLink, unsigned outLink, const char* formatter){
  FormatterFactory ff;
  AbsFormatter* theformatter=ff.createFormatter(formatter, id);
  if(std::string(type)=="FEI3"){
    FEI3::Module *mod= new FEI3::Module(name, id, inLink, outLink, theformatter);
    m_modgroupi3.addModule(mod);
    return mod;
  }
  if(std::string(type)=="RCF_FEI3"){
    FEI3::Module *mod= new FEI3::RCFModule(m_server, name,id, inLink, outLink, theformatter);
    m_modgroupi3.addModule(mod);
    return mod;
  }
  if(std::string(type)=="RCF_FEI4A"){
    FEI4::Module *mod= new FEI4::RCFFEI4AModule(m_server, name,id, inLink, outLink, theformatter);
    m_modgroupi4a.addModule(mod);
    return mod;
  }
  if(std::string(type)=="RCF_FEI4B"){
    FEI4::Module *mod= new FEI4::RCFFEI4BModule(m_server, name,id, inLink, outLink, theformatter);
    m_modgroupi4b.addModule(mod);
    return mod;
  }
  if(std::string(type)=="RCF_Hitbus"){
    Hitbus::Module *mod= new Hitbus::RCFModule(m_server, name,id, inLink, outLink, theformatter);
    m_modgrouphitbus.addModule(mod);
    return mod;
  }
  if(std::string(type)=="RCF_AFPHPTDC"){
    afphptdc::Module *mod= new afphptdc::RCFModule(m_server, name,id, inLink, outLink, theformatter);
    m_modgroupafphptdc.addModule(mod);
    return mod;
  }
  std::cout<<"Unknown module type "<<type<<std::endl;
  rcecalib::Unknown_Module_Type issue;
  throw issue;
  return 0;
} 

AbsTrigger* RCFModuleFactory::createTriggerIF(const char* type, ConfigIF* cif){
  //  std::cout << "Creating trigger of type " << type << std::endl;
  if(std::string(type)=="default") return new FEI3::Trigger;
  else if(std::string(type)=="FEI3")return new FEI3::Trigger;
  else if(std::string(type)=="EventFromFile")return new EventFromFileTrigger;
  else if(std::string(type)=="MultiShot")return new MultiShotTrigger;
  else if(std::string(type)=="MultiTrigger")return new MultiTrigger;
  else if(std::string(type)=="Hitor")return new HitorTrigger(cif);
  else if(std::string(type)=="Temperature")return new FEI4::TemperatureTrigger;
  else if(std::string(type)=="Monleak")return new FEI4::MonleakTrigger;
  else if(std::string(type)=="SerialNumber")return new FEI4::SNTrigger;
  else if(std::string(type)=="Fei4RegisterTest")return new FEI4::Fei4RegisterTestTrigger;
#ifdef __rtems__
  else if(std::string(type)=="Measurement")return new MeasurementTrigger;
#endif
  char msg[128];
  printf("No Trigger type %s",type);
  assert(0); 
}
