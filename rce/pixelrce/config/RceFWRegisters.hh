#ifndef RCEFWREGISTERS_HH
#define RCEFWREGISTERS_HH

#include "config/FWRegisters.hh"


class RceFWRegisters: public FWRegisters{
public:
  virtual void writeRegister(int rce, unsigned reg, unsigned val);
  virtual unsigned readRegister(int rce, unsigned reg);
  virtual void sendCommand(int rce, unsigned opcode);
};
#endif
