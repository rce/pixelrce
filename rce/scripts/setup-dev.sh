#!/bin/bash
_basedir=$(dirname ${BASH_SOURCE})/..
_basedir=$(cd ${_basedir}/> /dev/null 2>&1 && pwd)
source ${_basedir}/scripts/setup-env.sh $*
pushd .
TOOLCHAIN=arm-centos7
AXI_VER=""
if [ $# -eq 1 ] &&  [  $1 = "oldsdk" ]  ; then
    AXI_VER="-DOLD_AXIS_DRIVER"  
    TOOLCHAIN=arm-archlinux
else
export SDK_RCE_ROOT=/opt/rce/rootfs/centos7
fi


if [ -d ${_basedir}/scripts ]; then
  if [ ! -d ${_basedir}/$arm ]; then
     echo Creating ${_basedir}/rce/$arm
     mkdir ${_basedir}/$arm
  fi
  if [ ! -d ${_basedir}/$slc ]; then
     echo Creating ${_basedir}/$slc
     mkdir ${_basedir}/$slc
  fi
  export MAKEFLAGS="-j12 QUICK=1"
  echo cd ${_basedir}/$arm
  cd ${_basedir}/$arm
  cmake3  -DCMAKE_BUILD_TYPE="$debug"    -DCMAKE_TOOLCHAIN_FILE=${_basedir}/pixelrce/toolchain/${TOOLCHAIN} -DCMAKE_CXX_FLAGS="-DENABLE_PROFILING ${AXI_VER}" ${_basedir}/pixelrce  
  echo cd ${_basedir}/$slc
  cd ${_basedir}/$slc
  cmake3  -DCMAKE_BUILD_TYPE="$debug"   -DCMAKE_TOOLCHAIN_FILE=${_basedir}/pixelrce/toolchain/linux-gcc ${_basedir}/pixelrce 
else 
  echo "${_basedir}/scripts does not exist"
fi
popd
unset _basedir
unset AXI_VER
