#-------------------------------------------------------------------------------
#-- Title         : Common DTM Core Constraints
#-- File          : DtmCore.xdc
#-- Author        : Ryan Herbst, rherbst@slac.stanford.edu
#-- Created       : 11/14/2013
#-------------------------------------------------------------------------------
#-- Description:
#-- Common top level constraints for DTM
#-------------------------------------------------------------------------------
#-- Copyright [c] 2014 by Ryan Herbst. All rights reserved.
#-------------------------------------------------------------------------------
#-- Modification history:
#-- 11/14/2013: created.
#-- 01/20/2014: Added GMII-To-RGMII Switch
#-------------------------------------------------------------------------------


#########################################################
# Pin Locations. All Defined Here
#########################################################

set_property PACKAGE_PIN AA19 [get_ports led[0]]
set_property PACKAGE_PIN T15  [get_ports led[1]]

set_property PACKAGE_PIN AB13 [get_ports i2cScl]
set_property PACKAGE_PIN AA10 [get_ports i2cSda]

#100 MHz
set_property PACKAGE_PIN U6  [get_ports mgtRefClkP[0]]
set_property PACKAGE_PIN U5  [get_ports mgtRefClkM[0]]
# 250 MHz
set_property PACKAGE_PIN W6  [get_ports mgtRefClkP[1]]
set_property PACKAGE_PIN W5  [get_ports mgtRefClkM[1]]

set_property PACKAGE_PIN AA6 [get_ports mgtRxP[0]]
set_property PACKAGE_PIN AA5 [get_ports mgtRxM[0]]
set_property PACKAGE_PIN AB4 [get_ports mgtTxP[0]]
set_property PACKAGE_PIN AB3 [get_ports mgtTxM[0]]

set_property PACKAGE_PIN Y4  [get_ports mgtRxP[1]]
set_property PACKAGE_PIN Y3  [get_ports mgtRxM[1]]
set_property PACKAGE_PIN AA2  [get_ports mgtTxP[1]]
set_property PACKAGE_PIN AA1  [get_ports mgtTxM[1]]

set_property PACKAGE_PIN V4  [get_ports mgtRxP[2]]
set_property PACKAGE_PIN V3  [get_ports mgtRxM[2]]
set_property PACKAGE_PIN W2  [get_ports mgtTxP[2]]
set_property PACKAGE_PIN W1  [get_ports mgtTxM[2]]

set_property PACKAGE_PIN T4  [get_ports mgtRxP[3]]
set_property PACKAGE_PIN T3  [get_ports mgtRxM[3]]
set_property PACKAGE_PIN U2  [get_ports mgtTxP[3]]
set_property PACKAGE_PIN U1  [get_ports mgtTxM[3]]

set_property PACKAGE_PIN U9   [get_ports dpmClkP[2]]
set_property PACKAGE_PIN U8   [get_ports dpmClkM[2]]
set_property PACKAGE_PIN W9   [get_ports dpmClkP[1]]
set_property PACKAGE_PIN Y8   [get_ports dpmClkM[1]]
set_property PACKAGE_PIN AA14 [get_ports dpmClkP[0]]
set_property PACKAGE_PIN AB14 [get_ports dpmClkM[0]]

set_property PACKAGE_PIN R22  [get_ports idpmFbP[3]]
set_property PACKAGE_PIN T22  [get_ports idpmFbM[3]]
set_property PACKAGE_PIN N21  [get_ports idpmFbP[2]]
set_property PACKAGE_PIN N22  [get_ports idpmFbM[2]]
set_property PACKAGE_PIN W19  [get_ports idpmFbP[1]]
set_property PACKAGE_PIN W20  [get_ports idpmFbM[1]]
set_property PACKAGE_PIN V21  [get_ports idpmFbP[0]]
set_property PACKAGE_PIN V22  [get_ports idpmFbM[0]]
set_property PACKAGE_PIN W21  [get_ports odpmFbP[3]]
set_property PACKAGE_PIN Y22  [get_ports odpmFbM[3]]
set_property PACKAGE_PIN AA20 [get_ports odpmFbP[2]]
set_property PACKAGE_PIN AB20 [get_ports odpmFbM[2]]
set_property PACKAGE_PIN Y21  [get_ports odpmFbP[1]]
set_property PACKAGE_PIN AA21 [get_ports odpmFbM[1]]
set_property PACKAGE_PIN AA22 [get_ports odpmFbP[0]]
set_property PACKAGE_PIN AB22 [get_ports odpmFbM[0]]

set_property PACKAGE_PIN W18 [get_ports clkSelA]
set_property PACKAGE_PIN V17 [get_ports clkSelB]

#set_property PACKAGE_PIN AA12 [get_ports bpClkIn[5]]
#set_property PACKAGE_PIN Y16  [get_ports bpClkIn[4]]
set_property PACKAGE_PIN W11  [get_ports bpClkIn[3]]
set_property PACKAGE_PIN Y14  [get_ports bpClkIn[2]]
set_property PACKAGE_PIN Y12  [get_ports bpClkIn[1]]
set_property PACKAGE_PIN W14  [get_ports bpClkIn[0]]

#set_property PACKAGE_PIN T12 [get_ports bpClkOut[5]]
#set_property PACKAGE_PIN V16 [get_ports bpClkOut[4]]
set_property PACKAGE_PIN U13 [get_ports bpClkOut[3]]
set_property PACKAGE_PIN U15 [get_ports bpClkOut[2]]
set_property PACKAGE_PIN V12 [get_ports bpClkOut[1]]
set_property PACKAGE_PIN T11 [get_ports bpClkOut[0]]

set_property PACKAGE_PIN T10 [get_ports oDtmToIpmiP]
set_property PACKAGE_PIN T9  [get_ports oDtmToIpmiM]
set_property PACKAGE_PIN Y9  [get_ports iDtmToIpmiP]
set_property PACKAGE_PIN AA9 [get_ports iDtmToIpmiM]

set_property PACKAGE_PIN V8  [get_ports odtmToRtmLsP[1]]
set_property PACKAGE_PIN W8  [get_ports odtmToRtmLsM[1]]
set_property PACKAGE_PIN U10 [get_ports odtmToRtmLsP[0]]
set_property PACKAGE_PIN V10 [get_ports odtmToRtmLsM[0]]
set_property PACKAGE_PIN T20 [get_ports idtmToRtmLsP[3]]
set_property PACKAGE_PIN U20 [get_ports idtmToRtmLsM[3]]
set_property PACKAGE_PIN R17 [get_ports idtmToRtmLsP[2]]
set_property PACKAGE_PIN R18 [get_ports idtmToRtmLsM[2]]
set_property PACKAGE_PIN R19 [get_ports idtmToRtmLsP[1]]
set_property PACKAGE_PIN T19 [get_ports idtmToRtmLsM[1]]
set_property PACKAGE_PIN T17 [get_ports idtmToRtmLsP[0]]
set_property PACKAGE_PIN U17 [get_ports idtmToRtmLsM[0]]

#########################################################
# Common IO Types
#########################################################

set_property IOSTANDARD LVCMOS25 [get_ports i2cScl]
set_property IOSTANDARD LVCMOS25 [get_ports i2cSda]


set_property IOSTANDARD LVCMOS25 [get_ports clkSelA]
set_property IOSTANDARD LVCMOS25 [get_ports clkSelB]

set_property IOSTANDARD LVDS_25 [get_ports idtmToRtmLsP]
set_property IOSTANDARD LVDS_25 [get_ports idtmToRtmLsM]
set_property IOSTANDARD LVDS_25 [get_ports odtmToRtmLsP]
set_property IOSTANDARD LVDS_25 [get_ports odtmToRtmLsM]

set_property IOSTANDARD LVDS_25         [get_ports dpmClkP]
set_property IOSTANDARD LVDS_25         [get_ports dpmClkM]
set_property IOSTANDARD LVDS_25         [get_ports idpmFbP]
set_property IOSTANDARD LVDS_25         [get_ports idpmFbM]
set_property IOSTANDARD LVDS_25         [get_ports odpmFbP]
set_property IOSTANDARD LVDS_25         [get_ports odpmFbM]
set_property IOSTANDARD LVCMOS25        [get_ports led]
set_property IOSTANDARD LVCMOS25        [get_ports bpClkIn]
set_property IOSTANDARD LVCMOS25        [get_ports bpClkOut]
set_property IOSTANDARD LVDS_25        [get_ports iDtmToIpmiP]
set_property IOSTANDARD LVDS_25        [get_ports iDtmToIpmiM]
set_property IOSTANDARD LVDS_25        [get_ports oDtmToIpmiP]
set_property IOSTANDARD LVDS_25        [get_ports oDtmToIpmiM]


### Timing Constraints ###
# MGT Clocks
create_clock -period 10.000 -name mgtRefClk0_CC [get_ports mgtRefClkP[0]]
create_clock -period 4.000  -name mgtRefClk1_CC [get_ports mgtRefClkP[1]]
create_clock -period 8.000 -name sysclock_cc [get_pins -hierarchical -filter { NAME =~ "*BUFR_Inst/O*" }]

set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks mgtRefClk0_CC] -group [get_clocks -include_generated_clocks mgtRefClk1_CC] -group [get_clocks -include_generated_clocks sysclock_cc]

# StdLib
set_property ASYNC_REG TRUE [get_cells -hierarchical *crossDomainSyncReg_reg*]
