library ieee;
use ieee.std_logic_1164.all;
use work.stdrtlpkg.all;
use work.all;

library unisim;
use unisim.vcomponents.all;

entity TestClkGen is
  port (
    mgtRefClkP: in   slv(1 downto 0);
    mgtRefClkM: in   slv(1 downto 0);

    sys_clk_out:   out sl;
    sys_rst_out:   out sl;
    refclk_out:    out slv(1 downto 0);
    pgpClk_out:    out sl;
    clk100_out:    out sl
  );
end TestClkGen;

architecture TestClkGen of TestClkGen is
  signal sys_clk: sl;
  signal sys_rst: sl;
  signal refclk: slv(1 downto 0);
  signal clk: slv(1 downto 0);

begin
   -- Power Up Reset      
  PwrUpRst_Inst : entity work.PwrUpRst
    port map (
      clk    => sys_clk,
      rstOut => sys_rst);
  sys_rst_out<=sys_rst;

  gen_gte2: for I in 0 to 1 generate
    IBUFDS_GTE2_inst0 : IBUFDS_GTE2
      port map (
        I     => mgtRefClkP(I),
        IB    => mgtRefClkM(I),
        CEB   => '0',
        ODIV2 => open,
        O     => refclk(I)
        );
    refclk_out(I)<=refclk(I);
  end generate gen_gte2;
  
  bufgen: for I in 0 to 1 generate
    BUFG_Inst : BUFG
      port map (
        I => refclk(I),
        O => clk(I)
        );
  end generate bufgen;
  clk100_out<=clk(0);
  pgpClk_out<=clk(1);
  clockDivider_inst: entity work.ClockDivider
  generic map (
    BUFR_DIVIDE_G => "2")
  port map(
    clkIn => clk(1),
    clkOut => sys_clk);
  sys_clk_out<=sys_clk;
  
end TestClkGen;
