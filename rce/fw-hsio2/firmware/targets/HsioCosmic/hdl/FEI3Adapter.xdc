# Downgrade UCIO-1 and NSTD-1 (FIXME)
set_property SEVERITY {WARNING} [get_drc_checks UCIO-1]
set_property SEVERITY {WARNING} [get_drc_checks NSTD-1]


# Eudet TLU
set_property PACKAGE_PIN Y3  [get_ports {oExttrgclkP}]
set_property PACKAGE_PIN Y2  [get_ports {oExttrgclkN}]
set_property PACKAGE_PIN W1  [get_ports {oExtbusyP}]
set_property PACKAGE_PIN Y1  [get_ports {oExtbusyN}]
set_property PACKAGE_PIN R8  [get_ports {iExttriggerP}]
set_property PACKAGE_PIN R7  [get_ports {iExttriggerN}]
set_property PACKAGE_PIN T3  [get_ports {iExtrstP}]
set_property PACKAGE_PIN T2  [get_ports {iExtrstN}]

set_property IOSTANDARD LVDS_25 [get_ports oExttrgclkP]
set_property IOSTANDARD LVDS_25 [get_ports oExttrgclkN]
set_property IOSTANDARD LVDS_25  [get_ports {oExtbusyP}]
set_property IOSTANDARD LVDS_25  [get_ports {oExtbusyN}]
set_property IOSTANDARD LVDS_25 [get_ports iExttriggerP]
set_property IOSTANDARD LVDS_25 [get_ports iExttriggerN]
set_property IOSTANDARD LVDS_25 [get_ports iExtrstP]
set_property IOSTANDARD LVDS_25 [get_ports iExtrstN]

#Discriminators
set_property PACKAGE_PIN AD26 [get_ports {discinP[0]}]
set_property PACKAGE_PIN AE26 [get_ports {discinP[1]}]
set_property PACKAGE_PIN AC26 [get_ports {discinP[2]}]
set_property PACKAGE_PIN AC27 [get_ports {discinP[3]}]

set_property IOSTANDARD LVCMOS33 [get_ports {discinP[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {discinP[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {discinP[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {discinP[3]}]

#output clocks
#set_property PACKAGE_PIN AA2 [get_ports refclk_n]
set_property PACKAGE_PIN AG24 [get_ports refclk_p]
set_property PACKAGE_PIN AB1 [get_ports xclk_n]

set_property IOSTANDARD LVCMOS33 [get_ports refclk_p]
set_property SLEW FAST [get_ports refclk_p]
#set_property IOSTANDARD LVDS_25 [get_ports refclk_n]
set_property IOSTANDARD LVDS_25 [get_ports xclk_p]
set_property IOSTANDARD LVDS_25 [get_ports xclk_n]

# FEI3 adapter board
#Output to Frontends
set_property PACKAGE_PIN AH24 [get_ports {serialout_p[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {serialout_p[0]}]
set_property SLEW FAST [get_ports {serialout_p[0]}]
#Input from Frontends
set_property PACKAGE_PIN AF25 [get_ports {serialin_p[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {serialin_p[0]}]

