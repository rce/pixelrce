-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : HeartbeatTb.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2013-09-26
-- Last update: 2015-01-07
-- Platform   : ISE 14.5
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2013 SLAC National Accelerator Laboratory
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.all;
use work.StdRtlPkg.all;

entity prbstesttb is end prbstesttb;

architecture testbed of prbstesttb is
   signal clk  : sl;
   signal rst  : sl;
   signal counter: integer range 0 to 63:=0;
   signal err: slv(31 downto 0);
   signal len: slv(31 downto 0) := x"00000008";
   signal dataloop: sl;
   signal busy, done: sl;
   signal fail: sl;
   signal trig: sl:='0';
   signal datainvalid, dataoutvalid: sl;
   signal datain, dataout: slv(31 downto 0);
   type na4 is array (natural range 0 to 3) of NaturalArray(0 to 3);
   signal taps:            na4:=
    ((0 => 31, 1 => 6, 2 => 2, 3 => 1), 
    (0 => 30, 1 => 7, 2 => 4, 3 => 2), 
    (0 => 29, 1 => 8, 2 => 3, 3 => 1), 
    (0 => 28, 1 => 9, 2 => 6, 3 => 3)); 
   
begin
   CLK_0 : entity work.ClkRst
      generic map (
         CLK_PERIOD_G      => 25 ns,
         RST_START_DELAY_G => 0 ns,  -- Wait this long into simulation before asserting reset
         RST_HOLD_TIME_G   => 50 ns)  -- Hold reset for this long)
      port map (
         clkP => clk,
         clkN => open,
         rst  => rst,
         rstL => open);


   process begin
   wait until rising_edge(clk);
   if(rst='0')then
     counter<=counter+1;
   end if;
   if(counter=5 )then
     trig<='1';
   else
     trig<='0';
   end if;
   end process;

   prbstest_inst: entity work.prbstest
     generic map( PRBS_TAPS_G => taps(3))
     port map(
        clk => clk,
        rst => rst,
	err => err,
        fail => fail,
        trig => trig,
        done => done,
        len => len,
        dataind => datain,
        dataoutd => dataout,
        datainvalidd => datainvalid,
        dataoutvalidd => dataoutvalid,
        busy => busy,
        d_in => dataloop,
        d_out => dataloop
        );

end testbed;
