# StdLib

# FPGA Hardware Configuration
set_property CFGBVS VCCO [current_design]
set_property CONFIG_VOLTAGE 2.5 [current_design]

### HSIO User I/O ###
# Green HSIO LEDs
set_property PACKAGE_PIN AM9  [get_ports {led[0]}]
set_property PACKAGE_PIN AM11 [get_ports {led[1]}]
set_property PACKAGE_PIN AN11 [get_ports {led[2]}]
set_property PACKAGE_PIN AJ11 [get_ports {led[3]}]
set_property PACKAGE_PIN AK11 [get_ports {led[4]}]
set_property PACKAGE_PIN AL10 [get_ports {led[5]}]
set_property PACKAGE_PIN AM10 [get_ports {led[6]}]
set_property PACKAGE_PIN AL8  [get_ports {led[7]}]

set_property IOSTANDARD LVCMOS25 [get_ports {led[*]}]

# push switches
set_property PACKAGE_PIN W24  [get_ports {sw_softreset}]
set_property IOSTANDARD LVCMOS25 [get_ports {sw_softreset}]
set_property PACKAGE_PIN G34  [get_ports {sw_dtmboot}]
set_property IOSTANDARD LVCMOS33 [get_ports {sw_dtmboot}]

# DIP switches
set_property PACKAGE_PIN AN6  [get_ports {dip_sw[0]}]
set_property PACKAGE_PIN AN9  [get_ports {dip_sw[1]}]
set_property PACKAGE_PIN AP9  [get_ports {dip_sw[2]}]
set_property PACKAGE_PIN AJ10 [get_ports {dip_sw[3]}]
set_property PACKAGE_PIN AK10 [get_ports {dip_sw[4]}]
set_property PACKAGE_PIN AP11 [get_ports {dip_sw[5]}]
set_property PACKAGE_PIN AP10 [get_ports {dip_sw[6]}]
set_property PACKAGE_PIN AL9  [get_ports {dip_sw[7]}]

set_property IOSTANDARD LVCMOS25 [get_ports {dip_sw[*]}]

set_property PACKAGE_PIN AB31 [get_ports {lemoout}]
set_property IOSTANDARD LVCMOS25 [get_ports {lemoout}]

set_property PACKAGE_PIN AJ29 [get_ports {lemoin[0]}]
set_property PACKAGE_PIN AK30 [get_ports {lemoin[1]}]
set_property PACKAGE_PIN AL30 [get_ports {lemoin[2]}]
set_property PACKAGE_PIN AM30 [get_ports {lemoin[3]}]
set_property PACKAGE_PIN Y31 [get_ports {lemoin[4]}]
set_property PACKAGE_PIN AA30 [get_ports {lemoin[5]}]
set_property PACKAGE_PIN AB30 [get_ports {lemoin[6]}]

set_property IOSTANDARD LVCMOS33 [get_ports {lemoin[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {lemoin[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {lemoin[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {lemoin[3]}]
set_property IOSTANDARD LVCMOS25 [get_ports {lemoin[4]}]
set_property IOSTANDARD LVCMOS25 [get_ports {lemoin[5]}]
set_property IOSTANDARD LVCMOS25 [get_ports {lemoin[6]}]

set_property SLEW FAST [get_ports {lemoout}]

set_property PULLDOWN true [get_ports {lemoin}]


### FT232 UART ###
set_property PACKAGE_PIN L23  [get_ports {i232DceTx}]
set_property PACKAGE_PIN M24  [get_ports {o232DceRx}]
set_property PACKAGE_PIN L24  [get_ports {i232DceRts}]
set_property PACKAGE_PIN K23  [get_ports {o232DceCts}]
set_property PACKAGE_PIN G24  [get_ports {o232DceDsr}]
set_property PACKAGE_PIN J23  [get_ports {i232DceDtr}]
set_property PACKAGE_PIN G25  [get_ports {o232DceCd}]
set_property PACKAGE_PIN K25  [get_ports {i232DceRi}]

set_property IOSTANDARD LVCMOS33 [get_ports {i232DceTx}]
set_property IOSTANDARD LVCMOS33 [get_ports {o232DceRx}]
set_property IOSTANDARD LVCMOS33 [get_ports {i232DceRts}]
set_property IOSTANDARD LVCMOS33 [get_ports {o232DceCts}]
set_property IOSTANDARD LVCMOS33 [get_ports {o232DceDsr}]
set_property IOSTANDARD LVCMOS33 [get_ports {i232DceDtr}]
set_property IOSTANDARD LVCMOS33 [get_ports {o232DceCd}]
set_property IOSTANDARD LVCMOS33 [get_ports {i232DceRi}]


# Display I2C
set_property PACKAGE_PIN M26 [get_ports {displayI2cScl}]
set_property PACKAGE_PIN L34 [get_ports {displayI2cSda}]
#
set_property IOSTANDARD LVCMOS33 [get_ports {displayI2cScl}]
set_property IOSTANDARD LVCMOS33 [get_ports {displayI2cSda}]

# DTM
set_property PACKAGE_PIN R31 [get_ports {dtm_ps0_l}]
set_property IOSTANDARD LVCMOS25 [get_ports {dtm_ps0_l}]
set_property PULLUP TRUE [get_ports {dtm_ps0_l}]  
set_property PACKAGE_PIN P31 [get_ports {dtm_en_l}]
set_property IOSTANDARD LVCMOS25 [get_ports {dtm_en_l}]
set_property PACKAGE_PIN L29 [get_ports {dtmI2cScl}]
set_property PACKAGE_PIN L30 [get_ports {dtmI2cSda}]
#
set_property IOSTANDARD LVCMOS33 [get_ports {dtmI2cScl}]
set_property IOSTANDARD LVCMOS33 [get_ports {dtmI2cSda}]


### MGTs ###
# 250 MHz   clk 0 on Bank 113 (West)
set_property PACKAGE_PIN AG14 [get_ports iRefClk0B113P]
set_property PACKAGE_PIN AH14 [get_ports iRefClk0B113M]
# 312.5 MHz clk 1 on Bank 113 (West)
set_property PACKAGE_PIN AG16 [get_ports iRefClk1B113P]
set_property PACKAGE_PIN AH16 [get_ports iRefClk1B113M]
# 250 MHz   clk 0 on Bank 116 (West)
set_property PACKAGE_PIN H16  [get_ports iRefClk0B116P]
set_property PACKAGE_PIN G16  [get_ports iRefClk0B116M]
# 312.5 MHz clk 1 on Bank 116 (West)
set_property PACKAGE_PIN H14  [get_ports iRefClk1B116P]
set_property PACKAGE_PIN G14  [get_ports iRefClk1B116M]
# clk 1 on Bank 216 (East)
set_property PACKAGE_PIN H20  [get_ports iRefClk1B216P]
set_property PACKAGE_PIN G20  [get_ports iRefClk1B216M]
# clk 1 on Bank 213 (East)
set_property PACKAGE_PIN AG18  [get_ports iRefClk1B213P]
set_property PACKAGE_PIN AH18  [get_ports iRefClk1B213M]



set_property PACKAGE_PIN AG10 [get_ports {lvdso_p[0]}]
set_property PACKAGE_PIN AG9  [get_ports {lvdso_n[0]}]
set_property PACKAGE_PIN AD11 [get_ports {lvdsi_p[0]}]
set_property PACKAGE_PIN AE11 [get_ports {lvdsi_n[0]}]
set_property PACKAGE_PIN AG11 [get_ports {lvdso_p[1]}]
set_property PACKAGE_PIN AH11 [get_ports {lvdso_n[1]}]
set_property PACKAGE_PIN AD10 [get_ports {lvdsi_p[1]}]
set_property PACKAGE_PIN AE10 [get_ports {lvdsi_n[1]}]
set_property PACKAGE_PIN AB7  [get_ports {lvdso_p[2]}]
set_property PACKAGE_PIN AB6  [get_ports {lvdso_n[2]}]
set_property PACKAGE_PIN AC9  [get_ports {lvdsi_p[2]}]
set_property PACKAGE_PIN AC8  [get_ports {lvdsi_n[2]}]
set_property PACKAGE_PIN AA10 [get_ports {lvdso_p[3]}]
set_property PACKAGE_PIN AA9  [get_ports {lvdso_n[3]}]
set_property PACKAGE_PIN AB10 [get_ports {lvdsi_p[3]}]
set_property PACKAGE_PIN AB9  [get_ports {lvdsi_n[3]}]
set_property PACKAGE_PIN U10  [get_ports {lvdso_p[4]}]
set_property PACKAGE_PIN T10  [get_ports {lvdso_n[4]}]
set_property PACKAGE_PIN U7   [get_ports {lvdsi_p[4]}]
set_property PACKAGE_PIN U6   [get_ports {lvdsi_n[4]}]
set_property PACKAGE_PIN T8   [get_ports {lvdso_p[5]}]
set_property PACKAGE_PIN T7   [get_ports {lvdso_n[5]}]
set_property PACKAGE_PIN U9   [get_ports {lvdsi_p[5]}]
set_property PACKAGE_PIN T9   [get_ports {lvdsi_n[5]}]
set_property PACKAGE_PIN K1   [get_ports {lvdso_p[6]}]
set_property PACKAGE_PIN J1   [get_ports {lvdso_n[6]}]
set_property PACKAGE_PIN H1   [get_ports {lvdsi_p[6]}]
set_property PACKAGE_PIN G1   [get_ports {lvdsi_n[6]}]
set_property PACKAGE_PIN M2   [get_ports {lvdso_p[7]}]
set_property PACKAGE_PIN L2   [get_ports {lvdso_n[7]}]
set_property PACKAGE_PIN K3   [get_ports {lvdsi_p[7]}]
set_property PACKAGE_PIN K2   [get_ports {lvdsi_n[7]}] 

set_property PACKAGE_PIN AN1 [get_ports {lvdso_p[8]}]
set_property PACKAGE_PIN AP1 [get_ports {lvdso_n[8]}]
set_property PACKAGE_PIN AK2 [get_ports {lvdsi_p[8]}]
set_property PACKAGE_PIN AK1 [get_ports {lvdsi_n[8]}]
set_property PACKAGE_PIN AM2 [get_ports {lvdso_p[9]}]
set_property PACKAGE_PIN AN2 [get_ports {lvdso_n[9]}]
set_property PACKAGE_PIN AL2 [get_ports {lvdsi_p[9]}]
set_property PACKAGE_PIN AM1 [get_ports {lvdsi_n[9]}]
set_property PACKAGE_PIN AN3 [get_ports {lvdso_p[10]}]
set_property PACKAGE_PIN AP3 [get_ports {lvdso_n[10]}]
set_property PACKAGE_PIN AK3 [get_ports {lvdsi_p[10]}]
set_property PACKAGE_PIN AL3 [get_ports {lvdsi_n[10]}]
set_property PACKAGE_PIN AN4 [get_ports {lvdso_p[11]}]
set_property PACKAGE_PIN AP4 [get_ports {lvdso_n[11]}]
set_property PACKAGE_PIN AJ5 [get_ports {lvdsi_p[11]}]
set_property PACKAGE_PIN AK5 [get_ports {lvdsi_n[11]}]
set_property PACKAGE_PIN AP6 [get_ports {lvdso_p[12]}]
set_property PACKAGE_PIN AP5 [get_ports {lvdso_n[12]}]
set_property PACKAGE_PIN AL4 [get_ports {lvdsi_p[12]}]
set_property PACKAGE_PIN AM4 [get_ports {lvdsi_n[12]}]
set_property PACKAGE_PIN AL5 [get_ports {lvdso_p[13]}]
set_property PACKAGE_PIN AM5 [get_ports {lvdso_n[13]}]
set_property PACKAGE_PIN AJ6 [get_ports {lvdsi_p[13]}]
set_property PACKAGE_PIN AK6 [get_ports {lvdsi_n[13]}]
set_property PACKAGE_PIN AK7 [get_ports {lvdso_p[14]}]
set_property PACKAGE_PIN AL7 [get_ports {lvdso_n[14]}]
set_property PACKAGE_PIN AM7 [get_ports {lvdsi_p[14]}]
set_property PACKAGE_PIN AM6 [get_ports {lvdsi_n[14]}]
set_property PACKAGE_PIN AJ8 [get_ports {lvdso_p[15]}]
set_property PACKAGE_PIN AK8 [get_ports {lvdso_n[15]}]
set_property PACKAGE_PIN AN8 [get_ports {lvdsi_p[15]}]
set_property PACKAGE_PIN AP8 [get_ports {lvdsi_n[15]}] 

set_property IOSTANDARD LVCMOS25 [get_ports {lvdso_p[*]}]  
set_property IOSTANDARD LVCMOS25 [get_ports {lvdso_n[*]}] 
set_property IOSTANDARD LVCMOS25 [get_ports {lvdsi_p[*]}]  
set_property IOSTANDARD LVCMOS25 [get_ports {lvdsi_n[*]}] 

set_property PULLDOWN TRUE [get_ports {lvdsi_p[*]}]  
set_property PULLDOWN TRUE [get_ports {lvdsi_n[*]}] 

#zone 3

#output clocks
set_property PACKAGE_PIN L3 [get_ports clkA_n]
set_property PACKAGE_PIN G2 [get_ports clkB_n]

set_property IOSTANDARD LVDS_25 [get_ports clkA_p]
set_property IOSTANDARD LVDS_25 [get_ports clkA_n]
set_property IOSTANDARD LVDS_25 [get_ports clkB_p]
set_property IOSTANDARD LVDS_25 [get_ports clkB_n]

# Eudet TLU
set_property PACKAGE_PIN AD25  [get_ports {oExttrgclk}]
set_property PACKAGE_PIN AG25  [get_ports {oExtbusy}]
set_property PACKAGE_PIN AD24  [get_ports {iExttrigger}]
set_property PACKAGE_PIN AF25  [get_ports {iExtrst}]

set_property IOSTANDARD LVCMOS33 [get_ports oExttrgclk]
set_property IOSTANDARD LVCMOS33  [get_ports {oExtbusy}]
set_property IOSTANDARD LVCMOS33 [get_ports iExttrigger]
set_property IOSTANDARD LVCMOS33 [get_ports iExtrst]


#Output to Frontends A
set_property PACKAGE_PIN AC6 [get_ports {serialout_n[0]}]
set_property PACKAGE_PIN AA7 [get_ports {serialout_n[1]}]
set_property PACKAGE_PIN AC3 [get_ports {serialout_n[2]}]
set_property PACKAGE_PIN AC1 [get_ports {serialout_n[3]}]
set_property PACKAGE_PIN AA2 [get_ports {serialout_n[4]}]
set_property PACKAGE_PIN AB1 [get_ports {serialout_n[5]}]
set_property PACKAGE_PIN AB4 [get_ports {serialout_n[6]}]
set_property PACKAGE_PIN AA4 [get_ports {serialout_n[7]}]
set_property PACKAGE_PIN Y5 [get_ports {serialout_n[8]}]
#Output to Frontends B
set_property PACKAGE_PIN Y10 [get_ports {serialout_n[9]}]
set_property PACKAGE_PIN AF8 [get_ports {serialout_n[10]}]
set_property PACKAGE_PIN AE7 [get_ports {serialout_n[11]}]
set_property PACKAGE_PIN AD8 [get_ports {serialout_n[12]}]
set_property PACKAGE_PIN AG7 [get_ports {serialout_n[13]}]
set_property PACKAGE_PIN AE6 [get_ports {serialout_n[14]}]
set_property PACKAGE_PIN AF5 [get_ports {serialout_n[15]}]
set_property PACKAGE_PIN AG5 [get_ports {serialout_n[16]}]
set_property PACKAGE_PIN AE3 [get_ports {serialout_n[17]}]

set_property IOSTANDARD LVDS_25 [get_ports {serialout_p}]
set_property IOSTANDARD LVDS_25 [get_ports {serialout_n}]

#Input from Frontends A
set_property PACKAGE_PIN W4 [get_ports {serialin_n[0]}]
set_property PACKAGE_PIN W3 [get_ports {serialin_n[1]}]
set_property PACKAGE_PIN Y2 [get_ports {serialin_n[2]}]
set_property PACKAGE_PIN V1 [get_ports {serialin_n[3]}]
set_property PACKAGE_PIN Y1 [get_ports {serialin_n[4]}]
set_property PACKAGE_PIN Y6 [get_ports {serialin_n[5]}]
set_property PACKAGE_PIN Y7 [get_ports {serialin_n[6]}]
set_property PACKAGE_PIN V6 [get_ports {serialin_n[7]}]
set_property PACKAGE_PIN W8 [get_ports {serialin_n[8]}]
#Input from Frontends B
set_property PACKAGE_PIN AJ4 [get_ports {serialin_n[9]}]
set_property PACKAGE_PIN AD4 [get_ports {serialin_n[10]}]
set_property PACKAGE_PIN AG4 [get_ports {serialin_n[11]}]
set_property PACKAGE_PIN AJ3 [get_ports {serialin_n[12]}]
set_property PACKAGE_PIN AG2 [get_ports {serialin_n[13]}]
set_property PACKAGE_PIN AF2 [get_ports {serialin_n[14]}]
set_property PACKAGE_PIN AJ1 [get_ports {serialin_n[15]}]
set_property PACKAGE_PIN AE1 [get_ports {serialin_n[16]}]
set_property PACKAGE_PIN AH1 [get_ports {serialin_n[17]}]

set_property IOSTANDARD LVDS_25 [get_ports {serialin_p}]
set_property IOSTANDARD LVDS_25 [get_ports {serialin_n}]

# LS lines to DTM
set_property PACKAGE_PIN P24  [get_ports idpmFbP[3]]
set_property PACKAGE_PIN N24  [get_ports idpmFbM[3]]
set_property PACKAGE_PIN U25  [get_ports idpmFbP[2]]
set_property PACKAGE_PIN T25  [get_ports idpmFbM[2]]
set_property PACKAGE_PIN N26  [get_ports idpmFbP[1]]
set_property PACKAGE_PIN M27  [get_ports idpmFbM[1]]
set_property PACKAGE_PIN R26  [get_ports idpmFbP[0]]
set_property PACKAGE_PIN P26  [get_ports idpmFbM[0]]
set_property PACKAGE_PIN N27  [get_ports odpmFbP[3]]
set_property PACKAGE_PIN N28  [get_ports odpmFbM[3]]
set_property PACKAGE_PIN T27 [get_ports odpmFbP[2]]
set_property PACKAGE_PIN R27 [get_ports odpmFbM[2]]
set_property PACKAGE_PIN R25  [get_ports odpmFbP[1]]
set_property PACKAGE_PIN P25 [get_ports odpmFbM[1]]
set_property PACKAGE_PIN U26 [get_ports odpmFbP[0]]
set_property PACKAGE_PIN U27 [get_ports odpmFbM[0]]

set_property IOSTANDARD LVDS_25         [get_ports idpmFbP]
set_property IOSTANDARD LVDS_25         [get_ports idpmFbM]
set_property IOSTANDARD LVDS_25         [get_ports odpmFbP]
set_property IOSTANDARD LVDS_25         [get_ports odpmFbM]

#clock lines DTM

set_property PACKAGE_PIN U29   [get_ports dpmClkP[2]]
set_property PACKAGE_PIN T29   [get_ports dpmClkM[2]]
set_property PACKAGE_PIN P28   [get_ports dpmClkP[1]]
set_property PACKAGE_PIN P29   [get_ports dpmClkM[1]]
set_property PACKAGE_PIN R30 [get_ports dpmClkP[0]]
set_property PACKAGE_PIN P30 [get_ports dpmClkM[0]]

set_property IOSTANDARD LVDS_25         [get_ports dpmClkP]
set_property IOSTANDARD LVDS_25         [get_ports dpmClkM]

# I2C to TTC board
set_property PACKAGE_PIN AM31   [get_ports ttc_scl]
set_property PACKAGE_PIN AN32   [get_ports ttc_sda]
set_property IOSTANDARD LVCMOS33   [get_ports ttc_scl]
set_property IOSTANDARD LVCMOS33   [get_ports ttc_sda]

# data from TTC board
set_property PACKAGE_PIN T28   [get_ports ttc_data_p]
set_property PACKAGE_PIN R28   [get_ports ttc_data_n]
set_property IOSTANDARD LVDS_25   [get_ports ttc_data_p]
set_property IOSTANDARD LVDS_25   [get_ports ttc_data_n]


### Timing Constraints ###
# MGT Clocks
create_clock -period 4.000 -name iRefClk0B113P_CC [get_ports iRefClk0B113P]
create_clock -period 3.200 -name iRefClk1B113P_CC [get_ports iRefClk1B113P]
create_clock -period 4.000 -name iRefClk0B116P_CC [get_ports iRefClk0B116P]
create_clock -period 3.200 -name iRefClk1B116P_CC [get_ports iRefClk1B116P]
create_clock -period 6.238 -name iRefClk1B213P_CC [get_ports iRefClk1B213P]
create_clock -period 8.000 -name sysclock_cc [get_pins -hierarchical -filter { NAME =~ "*BUFR_Inst/O*" }]
create_clock -period 6.238 -name recoveredClock_CC [get_ports  dpmClkP[2]]

set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks iRefClk0B113P_CC] -group [get_clocks -include_generated_clocks iRefClk1B113P_CC] -group [get_clocks -include_generated_clocks iRefClk0B116P_CC] -group [get_clocks -include_generated_clocks iRefClk1B116P_CC] -group [get_clocks -include_generated_clocks sysclock_cc] -group [get_clocks -include_generated_clocks recoveredClock_CC] -group [get_clocks -include_generated_clocks iRefClk1B213P_CC]

set_property ASYNC_REG true [get_cells -hierarchical *crossDomainSyncReg_reg*]

set_input_delay -clock sysclock_cc 8 [get_ports serialin_p]
set_input_delay -clock sysclock_cc 8 [get_ports serialin_n]
set_output_delay -clock sysclock_cc 1 [get_ports serialout_p]
set_output_delay -clock sysclock_cc 1 [get_ports serialout_n]
set_input_delay -clock sysclock_cc 6 [get_ports serialin_p[18]]
set_input_delay -clock sysclock_cc 6 [get_ports serialin_n[18]]
