library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

use work.stdrtlpkg.all;

package HsioTestPkg is
  type HsioMgtTestState is record
    PrbsTrigIn          : sl;
    PrbsRxBusy          : sl;
    PrbsTxBusy          : sl;
    PrbsUpdatedResults  : sl;
    PrbsErrMissedPacket : sl;
    PrbsErrLength       : sl;
    PrbsErrDataBus      : sl;
    PrbsErrEofe         : sl;
    PrbsErrWordCnt      : slv(31 downto 0);
    PrbsErrbitCnt       : slv(31 downto 0);
    PrbsPacketRate      : slv(31 downto 0);
    PrbsPacketLength    : slv(31 downto 0);
    PrbsRxPacketLength  : slv(31 downto 0);
    PrbsSysReset        : sl;
    txPowerDown         : slv(1 downto 0);
    rxPowerDown         : slv(1 downto 0);
    feSysRst            : sl;
    trigIn_d1           : sl;
    trig                : sl;
    updatedResults      : sl;
  end record;

  type HsioMgtTestStateArray is array(natural range<>) of HsioMgtTestState;

  type HsioMgtTestQpll is record
    QPllRefClk  : slv(1 downto 0);
    QPllClk     : slv(1 downto 0);
    QPllLock       : slv(1 downto 0);
    QPllRefClkLost : slv(1 downto 0);
    QPllReset      : slv(1 downto 0);
  end record;

  type HsioMgtTestQpllArray is array(natural range<>) of HsioMgtTestQpll;

  type HsioMgtTestRxTx is record
    gtTxP            : sl;
    gtTxN            : sl;
    gtRxP            : sl;
    gtRxN            : sl;
  end record;

  type HsioMgtTestRxTxArray is array(natural range<>) of HsioMgtTestRxTx;

   -- PGP Configurations
   constant PGP_RATE_C : real := 5.000E+9;  -- 5.00 Gbps

   -- MGT Configurations
   constant CLK_DIV_C        : integer    := 1;
   constant CLK25_DIV_C      : integer    := 10;                        -- Set by wizard   
   constant RX_OS_CFG_C      : bit_vector := "0000010000000";           -- Set by wizard
   constant RXCDR_CFG_C      : bit_vector := x"0001107FE206021041010";  -- Set by wizard
   constant RXLPM_INCM_CFG_C : bit        := '1';                       -- Set by wizard???
   constant RXLPM_IPCM_CFG_C : bit        := '0';                       -- Set by wizard       

   -- Quad PLL Configurations
   constant QPLL_FBDIV_IN_C      : integer := 2;
   constant QPLL_FBDIV_45_IN_C   : integer := 5;
   constant QPLL_REFCLK_DIV_IN_C : integer := 1;

   -- Clock indices
  constant CLK0B113: integer := 0;
  constant CLK1B113: integer := 1;
  constant CLK0B116: integer := 2;
  constant CLK1B116: integer := 3;
  constant CLK1B213: integer := 4;
  constant CLK1B216: integer := 5;

  constant BANK113: integer :=0;
  constant BANK213: integer :=1;
  constant BANK116: integer :=2;
  constant BANK216: integer :=3;

end HsioTestPkg;
