
# IO Types
#set_property IOSTANDARD LVDS_25  [get_ports dtmToRtmLsP]
#set_property IOSTANDARD LVDS_25  [get_ports dtmToRtmLsM]
#set_property IOSTANDARD LVCMOS25 [get_ports plSpareP]
#set_property IOSTANDARD LVCMOS25 [get_ports plSpareM]

# PGP Clocks
create_clock -name locRefClk -period 4.0 [get_ports locRefClkP]

#create_generated_clock -name pgpClk250 -source [get_ports locRefClkP] \
##    -multiply_by 5 -divide_by 8 [get_pins U_HsioPgpLane/U_PgpClkGen/CLKOUT0]

set_clock_groups -asynchronous \
      -group [get_clocks -include_generated_clocks fclk0] \
      -group [get_clocks -include_generated_clocks locRefClk]

set_false_path -to [get_pins U_HsioCosmicCore1/CLKFEI490/CE1]
create_clock -name sysClk40Unbuf_cc -period 25 [get_pins U_HsioCosmicCore1/CLKFEI4/I0]
create_clock -name sysClk40Unbuf90_cc -period 25 [get_pins U_HsioCosmicCore1/CLKFEI490/I0]
create_clock -name sysClk160Unbuf_cc -period 6.25 [get_pins U_HsioCosmicCore1/CLKFEI4/I1]
create_clock -name sysClk160Unbuf90_cc -period 6.25 [get_pins U_HsioCosmicCore1/CLKFEI490/I1]
create_clock -period 25 -name clk40 [get_pins U_ClkGen/SysClkGen_Inst/U0/mmcm_adv_inst/CLKOUT0]

set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks clk40] -group [get_clocks -include_generated_clocks sysClk40Unbuf_cc]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks clk40] -group [get_clocks -include_generated_clocks sysClk40Unbuf90_cc]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks clk40] -group [get_clocks -include_generated_clocks sysClk160Unbuf_cc]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks clk40] -group [get_clocks -include_generated_clocks sysClk160Unbuf90_cc]

set_clock_groups -logically_exclusive -group [get_clocks -include_generated_clocks sysClk40Unbuf_cc] -group [get_clocks -include_generated_clocks sysClk160Unbuf_cc]
set_clock_groups -logically_exclusive -group [get_clocks -include_generated_clocks sysClk40Unbuf90_cc] -group [get_clocks -include_generated_clocks sysClk160Unbuf90_cc]

set_property PACKAGE_PIN P18 [get_ports discin[0]]
set_property PACKAGE_PIN P19 [get_ports discin[1]]
set_property PACKAGE_PIN U18 [get_ports discin[2]]
set_property PACKAGE_PIN V18 [get_ports discin[3]]

set_property IOSTANDARD LVCMOS25 [get_ports discin]
