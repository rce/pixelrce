--------------------------------------------------------------
-- Write a register in the GBT
-- Martin Kocian 04/2016
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.StdRtlPkg.all;
use work.all;

--------------------------------------------------------------

entity icwritegbtregister is
port(	sysClk40: 	    in std_logic;
        gbtaddr:            in slv(6 downto 0);
        regaddress:         in slv(15 downto 0);
        value:              in slv(7 downto 0);
        ic:                 out slv(1 downto 0);
        done:               out sl:='0';
        bs:                 out slv(127 downto 0);
        go:                 in sl
);
end icwritegbtregister;

--------------------------------------------------------------

architecture CBP of icwritegbtregister is

   type state_type is (idle, run, stuff);
   signal state: state_type:=idle;
   signal bitstream: slv(127 downto 0):=(others => '0');
   signal index : integer range 0 to 79 :=0;
   signal runind : integer range 0 to 127 :=0;
   signal numwords: slv(7 downto 0):=x"01";

begin
  bs<=bitstream;
  process begin
    wait until rising_edge(sysClk40);
    if(state=idle)then
      ic<="00";
      if(go='1')then
        -- load the bitstream
        -- frame delimiters
        bitstream(7 downto 0)<="01111110";
        bitstream(79 downto 72)<="01111110";
        --chip address
        bitstream(71 downto 65)<=gbtaddr;
        bitstream(64)<='0'; -- WRITE
        --command
        bitstream(63 downto 56)<=x"00";
        --number of data words = 1
        bitstream(55 downto 48)<=numwords;
        bitstream(47 downto 40)<=x"00";
        --register address
        bitstream(39 downto 32)<=regaddress(7 downto 0);
        bitstream(31 downto 24)<=regaddress(15 downto 8);
        --value
        bitstream(23 downto 16)<=value;
        --parity
        for I in 0 to 7 loop
          bitstream(8+I)<=value(I) xor regaddress(I) xor regaddress(8+I) xor numwords(I);
        end loop;
        -- fill the rest with zeros
        bitstream(127 downto 80)<=(others=>'0');
        state<=stuff;
        index<=66;
        done<='0';
      end if;
    elsif(state=stuff)then
      index<=index-1;
      if(index=7)then --done
        state<=run;
        runind<=126;
      elsif(bitstream(index+5 downto index)="111111")then
        --bitstream(127 downto index+2)<=bitstream(126 downto index+1); Vivado
        --doesn't know how to synthesize this.
        for i in 8 to 66 loop
          if(index=i)then
            bitstream(127 downto i+2)<=bitstream(126 downto i+1);
            bitstream(i+1)<='0'; -- stuff with 0
          end if;
        end loop;
      end if;
    elsif(state=run)then
      ic<=bitstream(runind+1 downto runind);
      if(runind=0)then
        done<='1';
        state<=idle;
      else
        runind<=runind-2;
      end if;
   end if;
  end process;

end CBP;

--------------------------------------------------------------
