--------------------------------------------------------------
-- Write a register in the GBT
-- Martin Kocian 04/2016
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.StdRtlPkg.all;
use work.all;

--------------------------------------------------------------

entity icwritephytx is
port(	sysClk40: 	    in std_logic;
        sysRst40:           in sl;
        gbtaddr:            in slv(6 downto 0);
        regaddress:         in slv(15 downto 0);
        value:              in slv(7 downto 0);
        ic:                 out slv(1 downto 0);
        done:               out sl:='0';
        go:                 in sl
);
end icwritephytx;

--------------------------------------------------------------

architecture CBP of icwritephytx is

  component PHY_HDLC_tx_FPGA port (
    tx_data:              in  slv(7 downto 0);
    tx_dvalid:            in  sl;
    tx_dstrobe:           out sl;
    tx_sd:                out slv(1 downto 0);
    rx_clk:               in  sl;
    resetb:               in  sl;
    forceSCFSM_reset:     in  sl);
   end component;
   type state_type is (idle, run);
   signal state: state_type:=idle;
   signal index : integer range 0 to 15 :=0;
   signal data: Slv8Array(7 downto 0):=(others=>(others=>'0'));
   signal dvalid: sl:='0';
   signal strobe: sl;
   signal dataword: slv(7 downto 0):=(others=>'0');
   signal command: slv(7 downto 0):=x"01";
   signal datawords: slv(7 downto 0):=x"01";
   signal resetb: sl;

begin
  process begin
    wait until rising_edge(sysClk40);
    if(state=idle)then
      if(go='1')then
        -- load the bitstream
        -- frame delimiters
        --chip address
        data(0)(7 downto 1)<=gbtaddr;
        data(0)(0)<='0'; -- WRITE
        --command
        data(1)<=command;
        --number of data words = 1
        data(2)<=datawords;
        data(3)<=x"00";
        --register address
        data(4)<=regaddress(7 downto 0);
        data(5)<=regaddress(15 downto 8);
        --value
        data(6)<=value;
        --parity
        for I in 0 to 7 loop
          data(7)(I)<=command(I) xor datawords(I) xor regaddress(I) xor regaddress(I+8) xor value(I);
        end loop;
        done<='0';
        index<=0;
        dvalid<='1';
        dataword<=x"00";
        state<=run;
      end if;
    elsif(state=run)then
      if(strobe='1')then
        if(index=8)then --done
          dvalid<='0';
          done<='1';
          state<=idle;
        else
          dataword<=data(index);
          index<=index+1;
        end if;
      end if;
   end if;
  end process;
  resetb <= not sysRst40;
  phy_tx_inst: PHY_HDLC_tx_FPGA
    port map(
    tx_data => dataword,
    tx_dvalid => dvalid,
    tx_dstrobe => strobe,
    tx_sd => ic,
    rx_clk => sysClk40,
    resetb => resetb,
    forceSCFSM_reset => '0');

end CBP;

--------------------------------------------------------------
