library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

use work.i2cpkg.all;
use work.I2cRegPCA9535DriverPkg.all;
use work.all;

entity I2cRegPCA9535Driver is
  generic 
  (
    addr: std_logic_vector(2 downto 0) := "100"
  );
  port
  (
    clk: in std_logic;
    rst: in std_logic;

    I2cRegMasterIn:  out I2cRegMasterInType;
    I2cRegMasterOut: in  I2cRegMasterOutType;
  
    I2cRegPCA9535DriverOut: out I2cRegPCA9535DriverOutType;
    I2cRegPCA9535DriverIn:  in  I2cRegPCA9535DriverInType
  );
end I2cRegPCA9535Driver;

architecture I2cRegPCA9535Driver of I2cRegPCA9535Driver is
  constant I2C_REG_MASTER_IN_INIT_PCA9535DRIVER_C : I2cRegMasterInType := (
    i2cAddr     => "0000100" & addr,
    tenbit      => '0',
    regAddr     => (others => '0'),
    regWrData   => (others => '0'),
    regOp       => '1',
    regAddrSkip => '0',
    regAddrSize => (others => '0'),
    regDataSize => (others => '0'),
    regReq      => '0',
    endianness  => '1'
  );

  type fsm_state_t is (
    idle, 
    configure_send_c1, configure_wait_c1, configure_send_c2, configure_wait_c2, 
    write_send_c1, write_wait_c1, write_send_c2, write_wait_c2);
  signal fsm_state: fsm_state_t := configure_send_c1;

  signal write_int: std_logic_vector(15 downto 0) := (others => '0');
  signal I2cRegMasterIn_int: I2cRegMasterInType := I2C_REG_MASTER_IN_INIT_PCA9535DRIVER_C;
  
  subtype busy_timeout_counter_t is integer range 0 to 250000000;
  signal busy_timeout_counter: busy_timeout_counter_t := busy_timeout_counter_t'low;
begin
  I2cRegMasterIn <= I2cRegMasterIn_int;

  process begin
    wait until rising_edge(clk);

    I2cRegPCA9535DriverOut.busy <= '1';
    I2cRegMasterIn_int.regReq <= '0';
    I2cRegPCA9535DriverOut.ack <= '0';
    
    busy_timeout_counter <= busy_timeout_counter + 1;

    case fsm_state is
      when configure_send_c1 => 
        I2cRegMasterIn_int.regWrData(7 downto 0)<= (others => '0');
        I2cRegMasterIn_int.regAddr(7 downto 0)<= x"06";
        I2cRegMasterIn_int.regReq<='1';
        fsm_state <= configure_wait_c1;
      when configure_wait_c1 =>
        if I2cRegMasterOut.regFail = '1' then
          fsm_state <= idle;
          I2cRegPCA9535DriverOut.fail <= '1';
        elsif I2cRegMasterOut.regAck = '1' then
          fsm_state <= configure_send_c2;
        end if;
      when configure_send_c2 => 
        I2cRegMasterIn_int.regWrData(7 downto 0)<= (others => '0');
        I2cRegMasterIn_int.regAddr(7 downto 0)<= x"07";
        I2cRegMasterIn_int.regReq<='1';
        fsm_state <= configure_wait_c2;
      when configure_wait_c2 =>
        if I2cRegMasterOut.regFail = '1' then
          fsm_state <= idle;
          I2cRegPCA9535DriverOut.fail <= '1';
        elsif I2cRegMasterOut.regAck = '1' then
          fsm_state <= idle;
          I2cRegPCA9535DriverOut.ack <= '1';
        end if;
      when write_send_c1 => 
        I2cRegMasterIn_int.regWrData(7 downto 0)<= write_int(7 downto 0);
        I2cRegMasterIn_int.regAddr(7 downto 0)<= x"02";
        I2cRegMasterIn_int.regReq<='1';
        fsm_state <= write_wait_c1;
      when write_wait_c1 =>
        if I2cRegMasterOut.regFail = '1' then
          fsm_state <= idle;
          I2cRegPCA9535DriverOut.fail <= '1';
        elsif I2cRegMasterOut.regAck = '1' then
          fsm_state <= write_send_c2;
        end if;
      when write_send_c2 => 
        I2cRegMasterIn_int.regWrData(7 downto 0)<= write_int(15 downto 8);
        I2cRegMasterIn_int.regAddr(7 downto 0)<= x"03";
        I2cRegMasterIn_int.regReq<='1';
        fsm_state <= write_wait_c2;
      when write_wait_c2 =>
        if I2cRegMasterOut.regFail = '1' then
          fsm_state <= idle;
          I2cRegPCA9535DriverOut.fail <= '1';
        elsif I2cRegMasterOut.regAck = '1' then
          fsm_state <= idle;
          I2cRegPCA9535DriverOut.ack <= '1';
        end if;
      when others =>
        if (I2cRegPCA9535DriverIn.configure = '1') then
          fsm_state <= configure_send_c1;
        elsif (I2cRegPCA9535DriverIn.write = '1') then
          write_int <= I2cRegPCA9535DriverIn.writeValue;
          fsm_state <= write_send_c1;
        else
          I2cRegPCA9535DriverOut.busy <= '0';
          busy_timeout_counter <= busy_timeout_counter_t'low;
        end if;
    end case;
    if (busy_timeout_counter = busy_timeout_counter_t'high) then
      fsm_state <= idle;
      busy_timeout_counter <= busy_timeout_counter_t'low;
    end if;
    if (rst = '1') then
      fsm_state <= idle;
    end if;
  end process;

end I2cRegPCA9535Driver;
