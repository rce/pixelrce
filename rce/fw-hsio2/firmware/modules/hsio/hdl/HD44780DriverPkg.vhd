library ieee;
use ieee.std_logic_1164.all;

package HD44780DriverPkg is 
  type HD44780GpioOutType is record
    update: std_logic;
    db: std_logic_vector(7 downto 0);
    rw: std_logic;
    rs: std_logic;
    e: std_logic;
  end record;
  
  constant HD44780DRIVER_GPIO_OUT_INIT : HD44780GpioOutType := (
    update => '0',
    db => (others => '0'),
    rw => '0',
    rs => '0',
    e => '0'
  );

  type HD44780GpioInType is record
    ack: std_logic;
    fail: std_logic;
  end record;
  
  constant HD44780DRIVER_GPIO_IN_INIT : HD44780GpioInType := (
    ack => '0',
    fail => '0'
  );

  type HD44780DriverDisplayBufferType is array(0 to 39) of std_logic_vector (7 downto 0);
end HD44780DriverPkg;

