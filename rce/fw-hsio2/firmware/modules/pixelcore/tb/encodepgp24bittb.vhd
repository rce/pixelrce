-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : HeartbeatTb.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2013-09-26
-- Last update: 2015-01-14
-- Platform   : ISE 14.5
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2013 SLAC National Accelerator Laboratory
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.all;
use work.StdRtlPkg.all;

entity encodepgp24bittb is end encodepgp24bittb;

architecture testbed of encodepgp24bittb is
   signal clk  : sl;
   signal rst  : sl;
   signal isrunning: sl;
   signal eof, sof, overflow : sl;
   signal ldout, valid, dvalid, dnextvalid: sl;
   signal marker : sl := '0';
   signal ldin : sl := '0';
   signal wen: sl := '0';
   signal d_in: slv(24 downto 0);
   signal d_next: slv(24 downto 0) ;
   signal datain: slv(24 downto 0):=(others => '0');
   signal d_inv: Slv25Array(5 downto 0) :=(0=>'1'& x"010203",
                                          1=>'1'& x"040506",
                                          2=>'1'& x"070809",
                                          3=>'1'& x"0a0b0c",
                                          4=>'1'& x"0d0e0f",
                                          5=>'1'& x"101112");
   signal d_out: slv(15 downto 0);
   signal counter: integer range 0 to 63:=0;
   signal datawaiting: std_logic;
   signal moredatawaiting: std_logic;
   signal parity: std_logic_vector(1 downto 0);
   signal oldeofin: std_logic:='0';
begin
   CLK_0 : entity work.ClkRst
      generic map (
         CLK_PERIOD_G      => 25 ns,
         RST_START_DELAY_G => 0 ns,  -- Wait this long into simulation before asserting reset
         RST_HOLD_TIME_G   => 50 ns)  -- Hold reset for this long)
      port map (
         clkP => clk,
         clkN => open,
         rst  => rst,
         rstL => open);


   process begin
   wait until rising_edge(clk);
   oldeofin<=eof;
   if(rst='0')then
     counter<=counter+1;
   end if;
   if(counter >= 1 and counter<=1)then
     datain<=d_inv(counter-1);
     wen<='1';
   elsif(counter<10)then
     ldin<='0';
     wen<='0';
   elsif(counter=33)then
     ldin<='1';
   elsif(counter=34)then
     ldin<='1';
   elsif(eof='1' and oldeofin='0')then
     ldin<='0';
   else
     ldin<='1';
   end if;
   end process;
     
   fei4fifo : entity work.lookaheadfifo
     generic map(DATA_WIDTH_G => 25,
                 ADDR_WIDTH_G => 10,
                 FULL_THRES_G => 800)
     port map (
       din => datain,
       wr_clk => '0',
       rd_clk => clk,
       rd_en => ldout,
       rst => rst,
       wr_en => wen,
       dout => d_in,
       dnext => d_next,
       valid => dvalid,
       dnextvalid => dnextvalid);

  pgpackdataflag: entity work.dataflagff
    port map(
      eofin=>datain(24),
      ldin=>wen,
      eofout=>d_next(24),
      ldout=>ldout,
      datawaiting=> datawaiting,
      moredatawaiting=> moredatawaiting,
      clkin=>clk,
      clkout=>clk,
      rst=>rst
      );

   encode: entity work.encodepgp24bit
    port map(clk => clk,
         rst => rst,
         enabled => '1',
         isrunning => isrunning,
         d_in => d_in,
         d_next => d_next,
         marker => marker,
         maxlength => 100,
         d_out(15 downto 0) => d_out,
         d_out(16) => sof,
         d_out(17) => eof,
         ldin => ldin,
         ldout => ldout,
         dnextvalid => dnextvalid,
         dvalid => dvalid,
         datawaiting=> datawaiting,
         moredatawaiting=> moredatawaiting,
         overflow => overflow,
         parout => parity,
         valid => valid
);

end testbed;
