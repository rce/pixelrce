--------------------------------------------------------------
-- DTM startup
-- Martin Kocian 12/2014
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.StdRtlPkg.all;
use work.I2cPkg.all;
use work.all;

--------------------------------------------------------------

entity startDtm is
generic( PRESCALE_G: integer range 0 to 65535 := 79);
port(	clk: 	    in std_logic;
        rst:        in std_logic;
	done:	    out sl:='0';
	fail:	    out sl:='0';
        detectL:    in std_logic;
        powerupL:    out sl;
        sw_dtmbootL: in sl;
        scl:        inout std_logic;
        sda:        inout std_logic
);
end startDtm;

--------------------------------------------------------------

architecture STARTDTM of startDtm is

  type state_type is (idle, pause, req, ack, finished);
  signal state: state_type:=idle;
  signal adcI2cIn   : i2c_in_type;
  signal adcI2cOut  : i2c_out_type;
  signal i2cregmasterin: I2cRegMasterInType := I2C_REG_MASTER_IN_INIT_C;
  signal i2cregmasterout: I2cRegMasterOutType;
  signal secondwrite: sl:='0';
  signal counter: slv(24 downto 0):=(others =>'1');
begin
   sda    <= adcI2cOut.sda when adcI2cOut.sdaoen = '0' else 'Z';
   adcI2cIn.sda <= to_x01z(sda);
   scl    <= adcI2cOut.scl when adcI2cOut.scloen = '0' else 'Z';
   adcI2cIn.scl <= to_x01z(scl);

   i2cregmasterin.regDataSize<="00";
   i2cregmasterin.i2cAddr<="0000100000";

   I2cRegMaster_Inst: entity work.I2cRegMaster
     generic map(
       PRESCALE_G => PRESCALE_G)
     port map(
       clk => clk,
       regIn => i2cregmasterin,
       regOut => i2cregmasterout,
       i2ci => adcI2cIn,
       i2co => adcI2cOut);

   powerupL<= detectL or not sw_dtmbootL;
   process begin
     wait until (rising_edge(clk));
     if(state=idle)then
       if(rst='0' and detectL='0') then
         secondwrite<='0';
         state<=pause;
       end if;
     elsif (state=pause)then
       counter<=unsigned(counter)-1;
       if(counter='0'&x"000000")then
         state<=req;
       end if;
     elsif (state=req)then
       i2cregmasterin.regOp<='1';
       if(secondwrite='0')then
         i2cregmasterin.regAddr(7 downto 0) <= x"18";
         i2cregmasterin.regWrData(7 downto 0) <= x"ef";
       else
         i2cregmasterin.regAddr(7 downto 0) <= x"08";
         i2cregmasterin.regWrData(7 downto 0) <= x"10";
       end if;
       i2cregmasterin.regReq<='1';
       state<=ack;
     elsif(state=ack)then
       i2cregmasterin.regReq<='0';
       if(i2cregmasterout.regAck='1')then
         fail<=i2cregmasterout.regFail;
         secondwrite<='1';
         if(secondwrite='1') then
           state<=finished;
         else
           state<=req;
         end if;
       end if;
     elsif(state=finished)then
       done<= '1';
     end if;
   end process;
       
end STARTDTM;

--------------------------------------------------------------
