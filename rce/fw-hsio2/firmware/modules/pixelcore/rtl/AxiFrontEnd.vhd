-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : PgpFrontEnd.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2014-01-29
-- Last update: 2016-02-06
-- Platform   : Vivado 2014.1
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Wrapper for front end logic connection to the PGP card.
-------------------------------------------------------------------------------
-- Copyright (c) 2014 SLAC National Accelerator Laboratory
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

use work.StdRtlPkg.all;
use work.AxiStreamPkg.all;
use work.SsiPkg.all;
use work.SsiCmdMasterPkg.all;
use work.AxiLitePkg.all;
use work.Pgp2bPkg.all;
use work.RceG3Pkg.all;

library unisim;
use unisim.vcomponents.all;

entity AxiFrontEnd is
   generic (
      TPD_G               : time                       := 1 ns;
      CASCADE_SIZE_G      : integer range 1 to (2**24) := 1;
      SLAVE_AXI_CONFIG_G  : AxiStreamConfigType;
      MASTER_AXI_CONFIG_G : AxiStreamConfigType;        
      atlasafp : std_logic:='0');
   port (
      -- DMA streams
      txMaster            : in  AxiStreamMasterType;
      txSlave             : out AxiStreamSlaveType;
      rxMaster            : out AxiStreamMasterType;
      rxSlave             : in  AxiStreamSlaveType;
      -- Axi Master Interface - Registers (sysClk domain)
      mAxiLiteReadMaster  : out AxiLiteReadMasterType;
      mAxiLiteReadSlave   : in  AxiLiteReadSlaveType;
      mAxiLiteWriteMaster : out AxiLiteWriteMasterType;
      mAxiLiteWriteSlave  : in  AxiLiteWriteSlaveType;
      -- Streaming Links (sysClk domain)      
      sAxisMaster         : in  AxiStreamMasterType;
      sAxisSlave          : out AxiStreamSlaveType;
      mAxisMaster         : out AxiStreamMasterType;
      mAxisSlave          : in  AxiStreamSlaveType;
      -- Streaming Links (receiveclk domain)      
      sAxisMasterEfb      : in AxiStreamMasterType;
      sAxisSlaveEfb       : out  AxiStreamSlaveType:=AXI_STREAM_SLAVE_INIT_C;
      -- PGP CMD interface
      pgpCmd              : out SsiCmdMasterType;
      -- Clock, Resets, and Status Signals
      sysReset            : in  sl;
      sysClk              : in  sl;
      pgpClk              : in  sl;
      pgpReset            : in  sl;
      sysClk100           : in  sl;
      sysRst100           : in  sl;
      axiClk40            : in  sl;
      axiRst40            : in  sl;
      receiveclock        : in  sl;
      receiverst          : in  sl;
      txReady             : out sl;
      rxReady             : out sl;
      sendOverflow        : out sl;
      locOverflow         : out sl;
      txErr               : out sl);
end AxiFrontEnd;

-- Define architecture
architecture mapping of AxiFrontEnd is


   -- Frame Transmit Interface - 1 Lane, Array of 4 VCs
   signal pgpTxMasters : AxiStreamMasterArray(3 downto 0);
   signal pgpTxSlaves  : AxiStreamSlaveArray(3 downto 0);
   signal muxTxMaster  : AxiStreamMasterType;
   signal muxTxSlave  :  AxiStreamSlaveType;
   signal muxRxMaster  : AxiStreamMasterType;
   signal muxRxSlave  :  AxiStreamSlaveType;

   -- Frame Receive Interface - 1 Lane, Array of 4 VCs
   signal pgpRxMasters : AxiStreamMasterArray(3 downto 0);
   signal pgpRxSlaves  : AxiStreamSlaveArray(3 downto 0);
   signal pgpRxCtrl    : AxiStreamCtrlArray(3 downto 0);
   signal sAxisCtrl    : AxiStreamCtrlType;
   signal sAxisCtrlEfb    : AxiStreamCtrlType;


   attribute KEEP_HIERARCHY : string;
   attribute KEEP_HIERARCHY of
      VCRX0, VCTX0,
      VCRX1_VCTX1,
      VCRX3: label is "TRUE";
   
begin
   U_TxFifo : entity work.AxiStreamFifo
      generic map (
         TPD_G               => TPD_G,
         PIPE_STAGES_G       => 1,
         SLAVE_READY_EN_G    => true,
         VALID_THOLD_G       => 1,
         BRAM_EN_G           => true,
         XIL_DEVICE_G        => "7SERIES",
         USE_BUILT_IN_G      => false,
         GEN_SYNC_FIFO_G     => false,
         ALTERA_SYN_G        => false,
         ALTERA_RAM_G        => "M9K",
         CASCADE_SIZE_G      => 1,
         FIFO_ADDR_WIDTH_G   => 9,
         FIFO_FIXED_THRESH_G => true,
         FIFO_PAUSE_THRESH_G => 255,
         SLAVE_AXI_CONFIG_G  => RCEG3_AXIS_DMA_CONFIG_C,
         MASTER_AXI_CONFIG_G => SSI_PGP2B_CONFIG_C)
      port map (
         sAxisClk        => pgpClk,
         sAxisRst        => pgpReset,
         sAxisMaster     => txMaster,
         sAxisSlave      => txSlave,
         sAxisCtrl       => open,
         fifoPauseThresh => (others => '1'),
         mAxisClk        => pgpClk,
         mAxisRst        => pgpReset,
         mAxisMaster     => muxTxMaster,
         mAxisSlave      => muxTxSlave);

   U_PgpTxMux : entity work.AxiStreamDeMux 
      generic map (
         TPD_G         => TPD_G,
         NUM_MASTERS_G => 4
      ) port map (
         axisClk      => pgpClk,
         axisRst      => pgpReset,
         sAxisMaster  => muxTxMaster, 
         sAxisSlave   => muxTxSlave, 
         mAxisMasters => pgpRxMasters,
         mAxisSlaves  => pgpRxSlaves
      );
   U_RxFifo : entity work.AxiStreamFifo
      generic map (
         TPD_G               => TPD_G,
         PIPE_STAGES_G       => 1,
         SLAVE_READY_EN_G    => true,
         VALID_THOLD_G       => 1,
         BRAM_EN_G           => true,
         XIL_DEVICE_G        => "7SERIES",
         USE_BUILT_IN_G      => false,
         GEN_SYNC_FIFO_G     => false,
         ALTERA_SYN_G        => false,
         ALTERA_RAM_G        => "M9K",
         CASCADE_SIZE_G      => 1,
         FIFO_ADDR_WIDTH_G   => 9,
         FIFO_FIXED_THRESH_G => true,
         FIFO_PAUSE_THRESH_G => 255,
         SLAVE_AXI_CONFIG_G  => SSI_PGP2B_CONFIG_C,
         MASTER_AXI_CONFIG_G => RCEG3_AXIS_DMA_CONFIG_C)
      port map (
         sAxisClk        => pgpClk,
         sAxisRst        => pgpReset,
         sAxisMaster     => muxRxMaster,
         sAxisSlave      => muxRxSlave,
         sAxisCtrl       => open,
         fifoPauseThresh => (others => '1'),
         mAxisClk        => pgpClk,
         mAxisRst        => pgpReset,
         mAxisMaster     => rxMaster,
         mAxisSlave      => rxSlave);
   U_PgpRxMux : entity work.AxiStreamMux 
      generic map (
         TPD_G         => TPD_G,
         NUM_SLAVES_G => 4
      ) port map (
         axisClk      => pgpClk,
         axisRst      => pgpReset,
         mAxisMaster  => muxRxMaster, 
         mAxisSlave   => muxRxSlave, 
         sAxisMasters => pgpTxMasters,
         sAxisSlaves  => pgpTxSlaves
      );

   -- Lane 0, VC0 RX, Command processor
   VCRX0 : entity work.SsiCmdMaster
      generic map (
         AXI_STREAM_CONFIG_G => SSI_PGP2B_CONFIG_C )   
      port map (
         -- Streaming Data Interface
         axisClk     => pgpClk,
         axisRst     => pgpReset,
         sAxisMaster => pgpRxMasters(0),
         sAxisSlave  => pgpRxSlaves(0),
         sAxisCtrl   => open,
         -- Command signals
         cmdClk      => sysClk,
         cmdRst      => sysReset,
         cmdMaster   => pgpCmd);     

   -- VC0 TX, Upstream Data Buffer
   VCTX0 : entity work.AxiStreamFifo
      generic map (
         -- General Configurations
         TPD_G               => TPD_G,
         PIPE_STAGES_G       => 1,
         SLAVE_READY_EN_G    => false,
         VALID_THOLD_G       => 1,
         -- FIFO configurations
         BRAM_EN_G           => true,
         XIL_DEVICE_G        => "7SERIES",
         USE_BUILT_IN_G      => false,
         GEN_SYNC_FIFO_G     => false,
         CASCADE_SIZE_G      => 1,
         FIFO_ADDR_WIDTH_G   => 11,
         FIFO_FIXED_THRESH_G => true,
         FIFO_PAUSE_THRESH_G => 512,
         CASCADE_PAUSE_SEL_G => 0,
         -- AXI Stream Port Configurations
         SLAVE_AXI_CONFIG_G  => SLAVE_AXI_CONFIG_G,
         MASTER_AXI_CONFIG_G => SSI_PGP2B_CONFIG_C)
      port map (
         -- Slave Port
         sAxisClk    => sysClk,
         sAxisRst    => sysReset,
         sAxisMaster => sAxisMaster,
         sAxisSlave  => open,
         sAxisCtrl   => sAxisCtrl,
         -- Master Port
         mAxisClk    => pgpClk,
         mAxisRst    => pgpReset,
         mAxisMaster => pgpTxMasters(0),
         mAxisSlave  => pgpTxSlaves(0));  


   -- Lane 0, VC1 RX/TX, Register access control        
   VCRX1_VCTX1 : entity work.SsiAxiLiteMaster
      generic map (
         USE_BUILT_IN_G      => false,
         AXI_STREAM_CONFIG_G => SSI_PGP2B_CONFIG_C)
      port map (
         -- Streaming Slave (Rx) Interface (sAxisClk domain) 
         sAxisClk            => pgpClk,
         sAxisRst            => pgpReset,
         sAxisMaster         => pgpRxMasters(1),
         sAxisSlave          => pgpRxSlaves(1),
         sAxisCtrl           => open,
         -- Streaming Master (Tx) Data Interface (mAxisClk domain)
         mAxisClk            => pgpClk,
         mAxisRst            => pgpReset,
         mAxisMaster         => pgpTxMasters(1),
         mAxisSlave          => pgpTxSlaves(1),
         -- AXI Lite Bus (axiLiteClk domain)
         axiLiteClk          => axiClk40,
         axiLiteRst          => axiRst40,
         mAxiLiteReadMaster  => mAxiLiteReadMaster,
         mAxiLiteReadSlave   => mAxiLiteReadSlave,
         mAxiLiteWriteMaster => mAxiLiteWriteMaster,
         mAxiLiteWriteSlave  => mAxiLiteWriteSlave);

   sAxisSlave.tReady <= not(sAxisCtrl.pause);
   sendOverflow <= sAxisCtrl.overflow;
   
   efbgen: if(atlasafp='1')generate 
   attribute KEEP_HIERARCHY of VCTX2: label is "TRUE";
   begin
   -- VC2 TX, Upstream Data Buffer EFB monitoring
   VCTX2 : entity work.AxiStreamFifo
      generic map (
         -- General Configurations
         TPD_G               => TPD_G,
         PIPE_STAGES_G       => 1,
         SLAVE_READY_EN_G    => false,
         VALID_THOLD_G       => 1,
         -- FIFO configurations
         BRAM_EN_G           => true,
         XIL_DEVICE_G        => "7SERIES",
         USE_BUILT_IN_G      => false,
         GEN_SYNC_FIFO_G     => false,
         CASCADE_SIZE_G      => 1,
         FIFO_ADDR_WIDTH_G   => 14,
         FIFO_FIXED_THRESH_G => true,
         FIFO_PAUSE_THRESH_G => 8192,
         CASCADE_PAUSE_SEL_G => 0,
         -- AXI Stream Port Configurations
         SLAVE_AXI_CONFIG_G  => ssiAxiStreamConfig(4),
         MASTER_AXI_CONFIG_G => SSI_PGP2B_CONFIG_C)
      port map (
         -- Slave Port
         sAxisClk    => receiveclock,
         sAxisRst    => receiverst,
         sAxisMaster => sAxisMasterEfb,
         sAxisSlave  => open,
         sAxisCtrl   => sAxisCtrlEfb,
         -- Master Port
         mAxisClk    => pgpClk,
         mAxisRst    => pgpReset,
         mAxisMaster => pgpTxMasters(2),
         mAxisSlave  => pgpTxSlaves(2));  
   sAxisSlaveEfb.tReady <= not(sAxisCtrlEfb.pause);
   end generate efbgen;
   efbnogen: if(atlasafp='0')generate
     pgpTxMasters(2) <= AXI_STREAM_MASTER_INIT_C;
   end generate efbnogen;
	
   -- Lane 0, VC3 RX Downstream Data Buffer
   VCRX3 : entity work.AxiStreamFifo
      generic map (
         TPD_G               => TPD_G,
         PIPE_STAGES_G       => 0,
         SLAVE_READY_EN_G    => false,
         VALID_THOLD_G       => 1,

         -- FIFO configurations
         BRAM_EN_G           => true,
         XIL_DEVICE_G        => "7SERIES",
         USE_BUILT_IN_G      => false,
         GEN_SYNC_FIFO_G     => false,
         CASCADE_SIZE_G      => 1,
         FIFO_ADDR_WIDTH_G   => 9,
         FIFO_FIXED_THRESH_G => true,
         FIFO_PAUSE_THRESH_G => 128,
         CASCADE_PAUSE_SEL_G => 0,
         -- AXI Stream Port Configurations
         SLAVE_AXI_CONFIG_G  => SSI_PGP2B_CONFIG_C,
         MASTER_AXI_CONFIG_G => MASTER_AXI_CONFIG_G) 
      port map (
         -- Slave Port
         sAxisClk    => pgpClk,
         sAxisRst    => pgpReset,
         sAxisMaster => pgpRxMasters(3),
         sAxisSlave  => pgpRxSlaves(3),
         sAxisCtrl   => open,
         -- Master Port
         mAxisClk    => sysClk,
         mAxisRst    => sysReset,
         mAxisMaster => mAxisMaster,
         mAxisSlave  => mAxisSlave);    
	-- Lane 0, VC3 TX Unused 
   pgpTxMasters(3) <= AXI_STREAM_MASTER_INIT_C;
   pgpRxMasters(2) <= AXI_STREAM_MASTER_INIT_C;


   txReady<='1';
   rxReady<='1';
   txErr<='0';
   locOverflow<='0';
end mapping;
