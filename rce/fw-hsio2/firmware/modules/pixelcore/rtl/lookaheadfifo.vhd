--------------------------------------------------------------
-- Look ahead fifo
-- Martin Kocian 01/2009
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.StdRtlPkg.all;
use work.all;

--------------------------------------------------------------

entity lookaheadfifo is
generic( 
      ASYNC_FIFO_G   : boolean := true;
      DATA_WIDTH_G   : integer range 1 to (2**24) := 16;
      ADDR_WIDTH_G   : integer range 4 to 48      := 4;
      FULL_THRES_G   : integer range 1 to (2**24) := 1);
port(	wr_clk:	    in std_logic;
        rd_clk:     in std_logic;
	rst:	    in std_logic;
	din:	    in std_logic_vector(DATA_WIDTH_G-1 downto 0);
	dout:	    out std_logic_vector(DATA_WIDTH_G-1 downto 0):=(others =>'0');
	dnext:	    out std_logic_vector(DATA_WIDTH_G-1 downto 0);
        wr_en:      in std_logic;
        rd_en:      in std_logic;
        dnextvalid: out std_logic;
        empty:      out std_logic;
        full:       out std_logic;
        overflow:   out std_logic;
        prog_full:  out std_logic;
        underflow:  out std_logic;
        valid:      out std_logic := '0'
);
end lookaheadfifo;

--------------------------------------------------------------

architecture LOOKAHEADFIFO of lookaheadfifo is

signal frd_en: std_logic:='0';
signal buffervalid: std_logic:='0';
signal doutvalid: std_logic:='0';
signal fdout: slv(DATA_WIDTH_G-1 downto 0);

begin
    dnextvalid<=buffervalid;
    dnext<=fdout;
    frd_en<=buffervalid and rd_en;
    valid<=doutvalid;
    process (rst, rd_clk)begin
    if(rst='1')then
      doutvalid<='0';
    elsif rising_edge(rd_clk) then
      if(frd_en='1')then
        dout<=fdout;
        doutvalid<='1';
      elsif(rd_en='1')then
        if(doutvalid='1')then
          doutvalid<='0';
        end if;
      end if;
    end if;
    end process;		

    hr1: if(ASYNC_FIFO_G=false) generate
      fei4fifo : entity work.FifoSync
        generic map(
          DATA_WIDTH_G => DATA_WIDTH_G,
          ADDR_WIDTH_G => ADDR_WIDTH_G,
          FWFT_EN_G => true,
          FULL_THRES_G => FULL_THRES_G) 
        port map (
          din => din,
          clk => rd_clk,
          rd_en => frd_en,
          rst => rst,
          wr_en => wr_en,
          dout => fdout,
          empty => empty,
          full => full,
          overflow => overflow,
          prog_full => prog_full,
          underflow => underflow,
          valid => buffervalid);
    end generate hr1;       
    hr2: if(ASYNC_FIFO_G=true) generate
      fei4fifo : entity work.FifoAsync
        generic map(
          DATA_WIDTH_G => DATA_WIDTH_G,
          ADDR_WIDTH_G => ADDR_WIDTH_G,
          FWFT_EN_G => true,
          FULL_THRES_G => FULL_THRES_G) 
        port map (
          din => din,
          rd_clk => rd_clk,
          rd_en => frd_en,
          rst => rst,
          wr_clk => wr_clk,
          wr_en => wr_en,
          dout => fdout,
          empty => empty,
          full => full,
          overflow => overflow,
          prog_full => prog_full,
          underflow => underflow,
          valid => buffervalid);
    end generate hr2;       

end LOOKAHEADFIFO;

--------------------------------------------------------------
