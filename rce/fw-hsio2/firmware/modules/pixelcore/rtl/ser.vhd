--------------------------------------------------------------
-- Serializer for High Speed I/O board (ATLAS Pixel teststand)
-- Martin Kocian 01/2009
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.all;

--------------------------------------------------------------

entity ser is

port(	clk: 	    in std_logic;
        ld:         out std_logic;
        l1a:        in std_logic;
        ecr:        in std_logic;
        bcr:        in std_logic;
        go:         in std_logic;
        busy:       out std_logic;
        stop:       in std_logic;
	rst:	    in std_logic;
        blockEcrBcr:in std_logic_vector(1 downto 0);
	d_in:	    in std_logic_vector(31 downto 0);
	d_out:	    out std_logic
);
end ser;

--------------------------------------------------------------

architecture SER of ser is

constant maxcountl1a: std_logic_vector(3 downto 0):="0101";
constant regl1a: std_logic_vector(31 downto 0):=x"e8000000";
constant maxcountecr: std_logic_vector(3 downto 0):="1001";
constant regecr: std_logic_vector(31 downto 0):=x"b1000000";
constant maxcountbcr: std_logic_vector(3 downto 0):="1001";
constant regbcr: std_logic_vector(31 downto 0):=x"b0800000";
signal regcmd: std_logic_vector(31 downto 0);
signal reg : std_logic_vector(31 downto 0);
signal busys: std_logic;
signal counter: std_logic_vector(4 downto 0);
signal cmdcounter: std_logic_vector(3 downto 0);
signal maxcount: std_logic_vector(3 downto 0);
signal going: std_logic;
signal oldl1a: std_logic;
signal oldecr: std_logic;
signal oldbcr: std_logic;

begin

  busy<=busys;

    process(rst, clk)

    begin
        if(rst='1') then
          reg<=x"00000000";
          d_out<='0';
          going<='0';
          ld<='0';
          cmdcounter<=x"0";
          maxcount<=x"f";
          regcmd<=(others => '0');
          oldl1a<='0';
          oldecr<='0';
          oldbcr<='0';
          busys<='0';
        elsif (clk'event and clk='1') then
            if (go='1' and stop='0' and busys='0')then
              going<='1';
              counter<="00010";
              busys<='1';
            else
              if (stop='1' and counter="00011") then
               going<='0';  
              end if;      
              if(counter="00010" and going='1') then  -- it takes 2 cycles to
                ld<='1';                               -- get data from the FIFO;
              elsif(counter="00001" and going='1') then
                ld<='0';                -- Request is only 1 cycle long.
              end if;
              if  (counter="00000" and going='1') then
               reg<=d_in; 
              elsif (cmdcounter=maxcount)then
                reg<=regcmd;
              else 
               reg(31 downto 1)<=reg(30 downto 0);
               reg(0)<='0';
              end if;
              if(cmdcounter="0010")then
                busys<='0';
              elsif(going='0' and counter="00000" and cmdcounter="0000") then
                busys<='0';
              end if;
              if(l1a='1' and oldl1a='0' and busys='0')then
                cmdcounter<=maxcountl1a;
                maxcount<=maxcountl1a;
                regcmd<=regl1a;
                busys<='1';
              elsif(ecr='1' and oldecr='0' and busys='0' and blockEcrBcr(1)='0')then
                cmdcounter<=maxcountecr;
                maxcount<=maxcountecr;
                regcmd<=regecr;
                busys<='1';
              elsif(bcr='1' and oldbcr='0' and busys='0' and blockEcrBcr(0)='0')then
                cmdcounter<=maxcountbcr;
                maxcount<=maxcountbcr;
                regcmd<=regbcr;
                busys<='1';
              elsif(cmdcounter/="0000")then
                cmdcounter<=unsigned(cmdcounter)-1;
              end if;
              counter<=unsigned(counter)-1;
              d_out<=reg(31);
            end if;
            oldl1a<=l1a;
            oldecr<=ecr;
            oldbcr<=bcr;
 	end if;
    
    end process;		

end SER;

--------------------------------------------------------------
