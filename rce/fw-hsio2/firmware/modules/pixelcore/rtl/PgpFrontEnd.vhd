-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : PgpFrontEnd.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2014-01-29
-- Last update: 2016-02-06
-- Platform   : Vivado 2014.1
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Wrapper for front end logic connection to the PGP card.
-------------------------------------------------------------------------------
-- Copyright (c) 2014 SLAC National Accelerator Laboratory
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

use work.StdRtlPkg.all;
use work.AxiStreamPkg.all;
use work.SsiPkg.all;
use work.SsiCmdMasterPkg.all;
use work.AxiLitePkg.all;
use work.Pgp2bPkg.all;

library unisim;
use unisim.vcomponents.all;

entity PgpFrontEnd is
   generic (
      TPD_G               : time                       := 1 ns;
      CASCADE_SIZE_G      : integer range 1 to (2**24) := 1;
      SLAVE_AXI_CONFIG_G  : AxiStreamConfigType;
      MASTER_AXI_CONFIG_G : AxiStreamConfigType;        
      atlasafp : std_logic:='0';
      -- MGT Configurations
      CLK_DIV_G        : integer;
      CLK25_DIV_G      : integer;
      RX_OS_CFG_G      : bit_vector;
      RXCDR_CFG_G      : bit_vector;
      RXLPM_INCM_CFG_G : bit;
      RXLPM_IPCM_CFG_G : bit);    
   port (
      -- Axi Master Interface - Registers (sysClk domain)
      mAxiLiteReadMaster  : out AxiLiteReadMasterType;
      mAxiLiteReadSlave   : in  AxiLiteReadSlaveType;
      mAxiLiteWriteMaster : out AxiLiteWriteMasterType;
      mAxiLiteWriteSlave  : in  AxiLiteWriteSlaveType;
      -- Streaming Links (sysClk domain)      
      sAxisMaster         : in  AxiStreamMasterType;
      sAxisSlave          : out AxiStreamSlaveType;
      mAxisMaster         : out AxiStreamMasterType;
      mAxisSlave          : in  AxiStreamSlaveType;
      -- Streaming Links (receiveclk domain)      
      sAxisMasterEfb      : in AxiStreamMasterType;
      sAxisSlaveEfb       : out  AxiStreamSlaveType:=AXI_STREAM_SLAVE_INIT_C;
      -- PGP CMD interface
      pgpCmd              : out SsiCmdMasterType;
      -- Clock, Resets, and Status Signals
      sysReset            : in  sl;
      sysClk              : in  sl;
      pgpClk              : in  sl;
      pgpReset            : in  sl;
      sysClk100           : in  sl;
      sysRst100           : in  sl;
      axiClk40            : in  sl;
      axiRst40            : in  sl;
      receiveclock        : in  sl;
      receiverst          : in  sl;
      txReady             : out sl;
      rxReady             : out sl;
      sendOverflow        : out sl;
      locOverflow         : out sl;
      txErr               : out sl;
      -- QPLL
      gtQPllOutRefClk  : in  slv(1 downto 0);
      gtQPllOutClk     : in  slv(1 downto 0);
      gtQPllLock       : in  slv(1 downto 0);
      gtQPllRefClkLost : in  slv(1 downto 0);
      gtQPllReset      : out slv(1 downto 0);
      -- GT Pins
      gtTxP               : out sl;
      gtTxN               : out sl;
      gtRxP               : in  sl;
      gtRxN               : in  sl);        
end PgpFrontEnd;

-- Define architecture
architecture mapping of PgpFrontEnd is

   -- Non VC Rx Signals
   signal pgpRxIn  : Pgp2bRxInType;
   signal pgpRxOut : Pgp2bRxOutType;

   -- Non VC Tx Signals
   signal pgpTxIn  : Pgp2bTxInType;
   signal pgpTxOut : Pgp2bTxOutType;

   -- Frame Transmit Interface - 1 Lane, Array of 4 VCs
   signal pgpTxMasters : AxiStreamMasterArray(3 downto 0);
   signal pgpTxSlaves  : AxiStreamSlaveArray(3 downto 0);

   -- Frame Receive Interface - 1 Lane, Array of 4 VCs
   signal pgpRxMasters : AxiStreamMasterArray(3 downto 0);
   signal pgpRxCtrl    : AxiStreamCtrlArray(3 downto 0);
   signal sAxisCtrl    : AxiStreamCtrlType;
   signal sAxisCtrlEfb    : AxiStreamCtrlType;


   attribute KEEP_HIERARCHY : string;
   attribute KEEP_HIERARCHY of
      Pgp2bGtp7MultiLane_Inst,
      VCRX0, VCTX0,
      VCRX1_VCTX1,
      VCRX3: label is "TRUE";
   
begin

   Pgp2bGtp7MultiLane_Inst : entity work.Pgp2bGtp7MultiLane
      generic map (
         VC_INTERLEAVE_G    => 0,
         -- CPLL Settings -
         RXOUT_DIV_G        => CLK_DIV_G,
         TXOUT_DIV_G        => CLK_DIV_G,
         RX_CLK25_DIV_G     => CLK25_DIV_G,
         TX_CLK25_DIV_G     => CLK25_DIV_G,
         RX_OS_CFG_G        => RX_OS_CFG_G,
         RXCDR_CFG_G        => RXCDR_CFG_G,
         RXLPM_INCM_CFG_G   => RXLPM_INCM_CFG_G,
         RXLPM_IPCM_CFG_G   => RXLPM_IPCM_CFG_G,
         -- Configure PLL sources
         TX_PLL_G           => "PLL1",
         RX_PLL_G           => "PLL1")            
      port map (
         -- GT Clocking
         stableClk        => pgpClk,
         gtQPllOutRefClk  => gtQPllOutRefClk,
         gtQPllOutClk     => gtQPllOutClk,
         gtQPllLock       => gtQPllLock,
         gtQPllRefClkLost => gtQPllRefClkLost,
         gtQPllReset      => gtQPllReset,
         -- Gt Serial IO
         gtTxP(0)         => gtTxP,
         gtTxN(0)         => gtTxN,
         gtRxP(0)         => gtRxP,
         gtRxN(0)         => gtRxN,
         -- Tx Clocking
         pgpTxReset       => pgpReset,
         pgpTxClk         => pgpClk,
         pgpTxMmcmReset   => open,
         pgpTxMmcmLocked  => '1',
         -- Rx clocking
         pgpRxReset       => pgpReset,
         pgpRxRecClk      => open,
         pgpRxClk         => pgpClk,
         pgpRxMmcmReset   => open,
         pgpRxMmcmLocked  => '1',
         -- Non VC Rx Signals
         pgpRxIn          => pgpRxIn,
         pgpRxOut         => pgpRxOut,
         -- Non VC Tx Signals
         pgpTxIn          => pgpTxIn,
         pgpTxOut         => pgpTxOut,
         -- Frame Transmit Interface - 1 Lane, Array of 4 VCs
         pgpTxMasters     => pgpTxMasters,
         pgpTxSlaves      => pgpTxSlaves,
         -- Frame Receive Interface - 1 Lane, Array of 4 VCs
         pgpRxMasters     => pgpRxMasters,
         pgpRxMasterMuxed => open,
         pgpRxCtrl        => pgpRxCtrl);    

   -- Lane 0, VC0 RX, Command processor
   VCRX0 : entity work.SsiCmdMaster
      generic map (
         AXI_STREAM_CONFIG_G => SSI_PGP2B_CONFIG_C)   
      port map (
         -- Streaming Data Interface
         axisClk     => pgpClk,
         axisRst     => pgpReset,
         sAxisMaster => pgpRxMasters(0),
         sAxisSlave  => open,
         sAxisCtrl   => pgpRxCtrl(0),
         -- Command signals
         cmdClk      => sysClk,
         cmdRst      => sysReset,
         cmdMaster   => pgpCmd);     

   -- VC0 TX, Upstream Data Buffer
   VCTX0 : entity work.AxiStreamFifo
      generic map (
         -- General Configurations
         TPD_G               => TPD_G,
         PIPE_STAGES_G       => 1,
         SLAVE_READY_EN_G    => false,
         VALID_THOLD_G       => 1,
         -- FIFO configurations
         BRAM_EN_G           => true,
         XIL_DEVICE_G        => "7SERIES",
         USE_BUILT_IN_G      => false,
         GEN_SYNC_FIFO_G     => false,
         CASCADE_SIZE_G      => 1,
         FIFO_ADDR_WIDTH_G   => 11,
         FIFO_FIXED_THRESH_G => true,
         FIFO_PAUSE_THRESH_G => 512,
         CASCADE_PAUSE_SEL_G => 0,
         -- AXI Stream Port Configurations
         SLAVE_AXI_CONFIG_G  => SLAVE_AXI_CONFIG_G,
         MASTER_AXI_CONFIG_G => SSI_PGP2B_CONFIG_C)
      port map (
         -- Slave Port
         sAxisClk    => sysClk,
         sAxisRst    => sysReset,
         sAxisMaster => sAxisMaster,
         sAxisSlave  => open,
         sAxisCtrl   => sAxisCtrl,
         -- Master Port
         mAxisClk    => pgpClk,
         mAxisRst    => pgpReset,
         mAxisMaster => pgpTxMasters(0),
         mAxisSlave  => pgpTxSlaves(0));  


   -- Lane 0, VC1 RX/TX, Register access control        
   VCRX1_VCTX1 : entity work.SsiAxiLiteMaster
      generic map (
         USE_BUILT_IN_G      => false,
         AXI_STREAM_CONFIG_G => SSI_PGP2B_CONFIG_C)
      port map (
         -- Streaming Slave (Rx) Interface (sAxisClk domain) 
         sAxisClk            => pgpClk,
         sAxisRst            => pgpReset,
         sAxisMaster         => pgpRxMasters(1),
         sAxisSlave          => open,
         sAxisCtrl           => pgpRxCtrl(1),
         -- Streaming Master (Tx) Data Interface (mAxisClk domain)
         mAxisClk            => pgpClk,
         mAxisRst            => pgpReset,
         mAxisMaster         => pgpTxMasters(1),
         mAxisSlave          => pgpTxSlaves(1),
         -- AXI Lite Bus (axiLiteClk domain)
         axiLiteClk          => axiClk40,
         axiLiteRst          => axiRst40,
         mAxiLiteReadMaster  => mAxiLiteReadMaster,
         mAxiLiteReadSlave   => mAxiLiteReadSlave,
         mAxiLiteWriteMaster => mAxiLiteWriteMaster,
         mAxiLiteWriteSlave  => mAxiLiteWriteSlave);

   sAxisSlave.tReady <= not(sAxisCtrl.pause);
   sendOverflow <= sAxisCtrl.overflow;
   
   efbgen: if(atlasafp='1')generate 
   attribute KEEP_HIERARCHY of VCTX2: label is "TRUE";
   begin
   -- VC2 TX, Upstream Data Buffer EFB monitoring
   VCTX2 : entity work.AxiStreamFifo
      generic map (
         -- General Configurations
         TPD_G               => TPD_G,
         PIPE_STAGES_G       => 1,
         SLAVE_READY_EN_G    => false,
         VALID_THOLD_G       => 1,
         -- FIFO configurations
         BRAM_EN_G           => true,
         XIL_DEVICE_G        => "7SERIES",
         USE_BUILT_IN_G      => false,
         GEN_SYNC_FIFO_G     => false,
         CASCADE_SIZE_G      => 1,
         FIFO_ADDR_WIDTH_G   => 14,
         FIFO_FIXED_THRESH_G => true,
         FIFO_PAUSE_THRESH_G => 8192,
         CASCADE_PAUSE_SEL_G => 0,
         -- AXI Stream Port Configurations
         SLAVE_AXI_CONFIG_G  => ssiAxiStreamConfig(4),
         MASTER_AXI_CONFIG_G => SSI_PGP2B_CONFIG_C)
      port map (
         -- Slave Port
         sAxisClk    => receiveclock,
         sAxisRst    => receiverst,
         sAxisMaster => sAxisMasterEfb,
         sAxisSlave  => open,
         sAxisCtrl   => sAxisCtrlEfb,
         -- Master Port
         mAxisClk    => pgpClk,
         mAxisRst    => pgpReset,
         mAxisMaster => pgpTxMasters(2),
         mAxisSlave  => pgpTxSlaves(2));  
   sAxisSlaveEfb.tReady <= not(sAxisCtrlEfb.pause);
   end generate efbgen;
   efbnogen: if(atlasafp='0')generate
     pgpTxMasters(2) <= AXI_STREAM_MASTER_INIT_C;
   end generate efbnogen;
	
   -- Lane 0, VC3 RX Downstream Data Buffer
   VCRX3 : entity work.AxiStreamFifo
      generic map (
         TPD_G               => TPD_G,
         PIPE_STAGES_G       => 0,
         SLAVE_READY_EN_G    => false,
         VALID_THOLD_G       => 1,

         -- FIFO configurations
         BRAM_EN_G           => true,
         XIL_DEVICE_G        => "7SERIES",
         USE_BUILT_IN_G      => false,
         GEN_SYNC_FIFO_G     => false,
         CASCADE_SIZE_G      => 1,
         FIFO_ADDR_WIDTH_G   => 9,
         FIFO_FIXED_THRESH_G => true,
         FIFO_PAUSE_THRESH_G => 128,
         CASCADE_PAUSE_SEL_G => 0,
         -- AXI Stream Port Configurations
         SLAVE_AXI_CONFIG_G  => SSI_PGP2B_CONFIG_C,
         MASTER_AXI_CONFIG_G => MASTER_AXI_CONFIG_G) 
      port map (
         -- Slave Port
         sAxisClk    => pgpClk,
         sAxisRst    => pgpReset,
         sAxisMaster => pgpRxMasters(3),
         sAxisSlave  => open,
         sAxisCtrl   => pgpRxCtrl(3),
         -- Master Port
         mAxisClk    => sysClk,
         mAxisRst    => sysReset,
         mAxisMaster => mAxisMaster,
         mAxisSlave  => mAxisSlave);    
	-- Lane 0, VC3 TX Unused 
   pgpTxMasters(3) <= AXI_STREAM_MASTER_INIT_C;
   pgpRxMasters(2) <= AXI_STREAM_MASTER_INIT_C;

   -- Misc. PGP signals
   pgpRxIn <= PGP2B_RX_IN_INIT_C;
   pgpTxIn <= PGP2B_TX_IN_INIT_C;

   SyncOut_Inst : entity work.SynchronizerVector
      generic map (
         WIDTH_G => 4)    
      port map (
         clk        => sysClk,
         dataIn(3)  => pgpRxOut.remOverflow(0),
         dataIn(2)  => pgpTxOut.locOverflow(0),
         dataIn(1)  => pgpTxOut.linkReady,
         dataIn(0)  => pgpRxOut.linkReady,
         dataOut(3) => txErr,
         dataOut(2) => locOverflow,
         dataOut(1) => txReady,
         dataOut(0) => rxReady);   

end mapping;
