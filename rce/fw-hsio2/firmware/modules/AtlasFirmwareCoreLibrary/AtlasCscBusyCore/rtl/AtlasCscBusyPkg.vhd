-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : AtlasCscBusyPkg.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2014-06-30
-- Last update: 2017-03-16
-- Platform   : Vivado 2014.3
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description:
-------------------------------------------------------------------------------
-- This file is part of 'ATLAS NRC CSC DEV'.
-- It is subject to the license terms in the LICENSE.txt file found in the 
-- top-level directory of this distribution and at: 
--    https://confluence.slac.stanford.edu/display/ppareg/LICENSE.html. 
-- No part of 'ATLAS NRC CSC DEV', including this file, 
-- may be copied, modified, propagated, or distributed except according to 
-- the terms contained in the LICENSE.txt file.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

use work.StdRtlPkg.all;
use work.AtlasTtcRxPkg.all;

package AtlasCscBusyPkg is

   type AtlasCscBusyStatusType is record
      -- Clock Status (atlasClk160MHz domain)
      refClkLocked : sl;
      freqLocked   : sl;
      clkLocked    : sl;
      cdrLocked    : sl;
      sigLocked    : sl;
      freqMeasured : slv(31 downto 0);  -- Units of Hz
      -- Deserialization Status (atlasClk160MHz domain)
      bpmLocked    : sl;
      bpmErr       : sl;
      deSerErr     : sl;
      -- Hamming Decode Status (atlasClk160MHz domain)
      sBitErrBc    : sl;
      dBitErrBc    : sl;
      -- TTC-RX Output Bus (atlasClk160MHz domain)
      ttcRx        : AtlasTtcRxOutType;
      busyRateCnt  : slv(31 downto 0);
      busyRate     : slv(31 downto 0);
      busyIn       : slv(7 downto 0);
      busyOut      : sl;
      -- IO-Delay Signals (refClk200MHz domain)
      delayOut     : AtlasTTCRxDelayOutType;
   end record;
   constant ATLAS_CSC_BUSY_STATUS_INIT_C : AtlasCscBusyStatusType := (
      -- Clock Status
      refClkLocked => '0',
      freqLocked   => '0',
      clkLocked    => '0',
      cdrLocked    => '0',
      sigLocked    => '0',
      freqMeasured => (others => '0'),
      -- Deserialization Status
      bpmLocked    => '0',
      bpmErr       => '0',
      deSerErr     => '0',
      -- Hamming Decode Status
      sBitErrBc    => '0',
      dBitErrBc    => '0',
      -- TTC-RX Output Bus
      ttcRx        => ATLAS_TTC_RX_OUT_INIT_C,
      busyRateCnt  => (others => '0'),
      busyRate     => (others => '0'),
      busyIn       => (others => '0'),
      busyOut      => '0',
      -- IO-Delay Signals 
      delayOut     => ATLAS_TTC_RX_DELAY_OUT_INIT_C);  

   type AtlasCscBusyConfigType is record
      busyRateRst      : sl;
      forceBusy        : sl;
      invertOutput     : sl;
      serDataEdgeSel   : sl;
      clkSel           : sl;
      emuSel           : sl;
      emuVersionSelect : sl;
      ignoreExtBusyIn  : slv(7 downto 0);
      ignoreSigLocked  : sl;
      ignoreCdrLocked  : sl;
      delayIn          : AtlasTtcRxDelayInType;
   end record;
   constant ATLAS_CSC_BUSY_CONFIG_INIT_C : AtlasCscBusyConfigType := (
      busyRateRst      => '1',
      forceBusy        => '0',
      invertOutput     => '1',
      serDataEdgeSel   => '0',
      clkSel           => '0',
      emuSel           => '1',
      emuVersionSelect => '0',
      ignoreExtBusyIn  => x"FC",
      ignoreSigLocked  => '0',
      ignoreCdrLocked  => '0',
      delayIn          => ATLAS_TTC_RX_DELAY_IN_INIT_C);   

end package AtlasCscBusyPkg;
