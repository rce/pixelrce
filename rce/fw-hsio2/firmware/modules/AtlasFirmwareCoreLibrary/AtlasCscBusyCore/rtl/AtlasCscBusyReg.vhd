-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : AtlasCscBusyReg.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2014-06-30
-- Last update: 2015-02-27
-- Platform   : Vivado 2014.3
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- This file is part of 'ATLAS NRC CSC DEV'.
-- It is subject to the license terms in the LICENSE.txt file found in the 
-- top-level directory of this distribution and at: 
--    https://confluence.slac.stanford.edu/display/ppareg/LICENSE.html. 
-- No part of 'ATLAS NRC CSC DEV', including this file, 
-- may be copied, modified, propagated, or distributed except according to 
-- the terms contained in the LICENSE.txt file.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

use work.StdRtlPkg.all;
use work.AxiLitePkg.all;
use work.AtlasCscBusyPkg.all;

entity AtlasCscBusyReg is
   generic (
      TPD_G              : time                  := 1 ns;
      STATUS_CNT_WIDTH_G : natural range 1 to 32 := 32;
      AXI_ERROR_RESP_G   : slv(1 downto 0)       := AXI_RESP_SLVERR_C;
      IDELAY_VALUE_G     : slv(4 downto 0)       := toSlv(0, 5));
   port (
      -- Status Bus (axiClk domain)
      statusWords    : out Slv64Array(0 to 0);
      statusSend     : out sl;
      -- AXI-Lite Register Interface (axiClk domain)
      axiReadMaster  : in  AxiLiteReadMasterType;
      axiReadSlave   : out AxiLiteReadSlaveType;
      axiWriteMaster : in  AxiLiteWriteMasterType;
      axiWriteSlave  : out AxiLiteWriteSlaveType;
      -- Register Inputs/Outputs (Mixed domains - refer to AtlasCscBusyPkg)
      config         : out AtlasCscBusyConfigType;
      status         : in  AtlasCscBusyStatusType;
      -- Global Signals
      refClk200MHz   : in  sl;
      axiClk         : in  sl;
      axiRst         : in  sl;
      locClk         : in  sl;
      locRst         : out sl);   
end AtlasCscBusyReg;

architecture rtl of AtlasCscBusyReg is

   constant STATUS_SIZE_C : positive := 19;

   type RegType is record
      locReset      : sl;
      cntRst        : sl;
      rollOverEn    : slv(STATUS_SIZE_C-1 downto 0);
      irqEn         : slv(STATUS_SIZE_C-1 downto 0);
      regOut        : AtlasCscBusyConfigType;
      axiReadSlave  : AxiLiteReadSlaveType;
      axiWriteSlave : AxiLiteWriteSlaveType;
   end record RegType;
   
   constant REG_INIT_C : RegType := (
      '1',
      '1',
      (others => '0'),
      (others => '0'),
      ATLAS_CSC_BUSY_CONFIG_INIT_C,
      AXI_LITE_READ_SLAVE_INIT_C,
      AXI_LITE_WRITE_SLAVE_INIT_C);

   signal r   : RegType := REG_INIT_C;
   signal rin : RegType;

   signal regIn  : AtlasCscBusyStatusType := ATLAS_CSC_BUSY_STATUS_INIT_C;
   signal regOut : AtlasCscBusyConfigType := ATLAS_CSC_BUSY_CONFIG_INIT_C;

   signal locReset,
      cntRst : sl;
   signal rollOverEn,
      irqEn : slv(STATUS_SIZE_C-1 downto 0);
   signal cntOut : SlVectorArray(STATUS_SIZE_C-1 downto 0, STATUS_CNT_WIDTH_G-1 downto 0);
   signal busyInCnt7,
      busyInCnt6,
      busyInCnt5,
      busyInCnt4,
      busyInCnt3,
      busyInCnt2,
      busyInCnt1,
      busyInCnt0,
      cdrLockedCnt,
      sigLockedCnt,
      busyOutCnt,
      dBitErrBcCnt,
      sBitErrBcCnt,
      deSerErrCnt,
      bpmErrCnt,
      bpmLockedCnt,
      freqLockedCnt,
      clkLockedCnt,
      refClkLockedCnt : slv(STATUS_CNT_WIDTH_G-1 downto 0);
   signal trigRate,
      trigL1Rate : slv(31 downto 0);
   
begin

   -------------------------------
   -- Configuration Register
   -------------------------------  
   comb : process (axiReadMaster, axiRst, axiWriteMaster, bpmErrCnt, bpmLockedCnt, busyInCnt0,
                   busyInCnt1, busyInCnt2, busyInCnt3, busyInCnt4, busyInCnt5, busyInCnt6,
                   busyInCnt7, busyOutCnt, cdrLockedCnt, clkLockedCnt, dBitErrBcCnt, deSerErrCnt,
                   freqLockedCnt, r, refClkLockedCnt, regIn, sBitErrBcCnt, sigLockedCnt, trigL1Rate) is
      variable v            : RegType;
      variable axiStatus    : AxiLiteStatusType;
      variable axiWriteResp : slv(1 downto 0);
      variable axiReadResp  : slv(1 downto 0);
   begin
      -- Latch the current value
      v := r;

      -- Determine the transaction type
      axiSlaveWaitTxn(axiWriteMaster, axiReadMaster, v.axiWriteSlave, v.axiReadSlave, axiStatus);

      -- Reset strobe signals
      v.regOut.delayIn.load := '0';
      v.regOut.delayIn.rst  := '0';
      v.locReset            := '0';
      v.cntRst              := '0';

      if (axiStatus.writeEnable = '1') then
         -- Check for an out of 32 bit aligned address
         axiWriteResp := ite(axiWriteMaster.awaddr(1 downto 0) = "00", AXI_RESP_OK_C, AXI_ERROR_RESP_G);
         -- Decode address and perform write
         case (axiWriteMaster.awaddr(9 downto 2)) is
            when x"80" =>
               v.regOut.forceBusy := axiWriteMaster.wdata(0);
            when x"81" =>
               v.regOut.emuSel := axiWriteMaster.wdata(0);
            when x"82" =>
               v.regOut.ignoreExtBusyIn := axiWriteMaster.wdata(7 downto 0);
            when x"83" =>
               v.regOut.ignoreSigLocked := axiWriteMaster.wdata(0);
            when x"84" =>
               v.regOut.ignoreCdrLocked := axiWriteMaster.wdata(0);
            when x"85" =>
               v.regOut.invertOutput := axiWriteMaster.wdata(0);
            when x"86" =>
               v.regOut.emuVersionSelect := axiWriteMaster.wdata(0);
            when x"87" =>
               v.regOut.clkSel := axiWriteMaster.wdata(0);
            when x"F0" =>
               v.rollOverEn := axiWriteMaster.wdata(STATUS_SIZE_C-1 downto 0);
            when x"F1" =>
               v.irqEn := axiWriteMaster.wdata(STATUS_SIZE_C-1 downto 0);
            when x"FD" =>
               v                     := REG_INIT_C;
               v.locReset            := '1';
               v.cntRst              := '1';
               v.regOut.delayIn.load := '1';
               v.regOut.delayIn.rst  := '1';
               v.regOut.delayIn.data := IDELAY_VALUE_G;
            when x"FE" =>
               v.locReset := '1';
            when x"FF" =>
               v.cntRst := '1';
            when others =>
               axiWriteResp := AXI_ERROR_RESP_G;
         end case;
         -- Send AXI response
         axiSlaveWriteResponse(v.axiWriteSlave, axiWriteResp);
      end if;

      if (axiStatus.readEnable = '1') then
         -- Check for an out of 32 bit aligned address
         axiReadResp          := ite(axiReadMaster.araddr(1 downto 0) = "00", AXI_RESP_OK_C, AXI_ERROR_RESP_G);
         -- Decode address and assign read data 
         v.axiReadSlave.rdata := (others => '0');
         case (axiReadMaster.araddr(9 downto 2)) is
            when x"00" =>
               v.axiReadSlave.rdata(STATUS_CNT_WIDTH_G-1 downto 0) := refClkLockedCnt;
            when x"01" =>
               v.axiReadSlave.rdata(STATUS_CNT_WIDTH_G-1 downto 0) := clkLockedCnt;
            when x"02" =>
               v.axiReadSlave.rdata(STATUS_CNT_WIDTH_G-1 downto 0) := freqLockedCnt;
            when x"03" =>
               v.axiReadSlave.rdata(STATUS_CNT_WIDTH_G-1 downto 0) := bpmLockedCnt;
            when x"04" =>
               v.axiReadSlave.rdata(STATUS_CNT_WIDTH_G-1 downto 0) := bpmErrCnt;
            when x"05" =>
               v.axiReadSlave.rdata(STATUS_CNT_WIDTH_G-1 downto 0) := deSerErrCnt;
            when x"06" =>
               v.axiReadSlave.rdata(STATUS_CNT_WIDTH_G-1 downto 0) := sBitErrBcCnt;
            when x"07" =>
               v.axiReadSlave.rdata(STATUS_CNT_WIDTH_G-1 downto 0) := dBitErrBcCnt;
            when x"08" =>
               v.axiReadSlave.rdata(STATUS_CNT_WIDTH_G-1 downto 0) := busyOutCnt;
            when x"09" =>
               v.axiReadSlave.rdata(STATUS_CNT_WIDTH_G-1 downto 0) := sigLockedCnt;
            when x"0A" =>
               v.axiReadSlave.rdata(STATUS_CNT_WIDTH_G-1 downto 0) := cdrLockedCnt;
            when x"0B" =>
               v.axiReadSlave.rdata(STATUS_CNT_WIDTH_G-1 downto 0) := busyInCnt0;
            when x"0C" =>
               v.axiReadSlave.rdata(STATUS_CNT_WIDTH_G-1 downto 0) := busyInCnt1;
            when x"0D" =>
               v.axiReadSlave.rdata(STATUS_CNT_WIDTH_G-1 downto 0) := busyInCnt2;
            when x"0E" =>
               v.axiReadSlave.rdata(STATUS_CNT_WIDTH_G-1 downto 0) := busyInCnt3;
            when x"0F" =>
               v.axiReadSlave.rdata(STATUS_CNT_WIDTH_G-1 downto 0) := busyInCnt4;
            when x"10" =>
               v.axiReadSlave.rdata(STATUS_CNT_WIDTH_G-1 downto 0) := busyInCnt5;
            when x"11" =>
               v.axiReadSlave.rdata(STATUS_CNT_WIDTH_G-1 downto 0) := busyInCnt6;
            when x"12" =>
               v.axiReadSlave.rdata(STATUS_CNT_WIDTH_G-1 downto 0) := busyInCnt7;
            when x"40" =>
               v.axiReadSlave.rdata(18 downto 11) := regIn.busyIn;
               v.axiReadSlave.rdata(10)           := regIn.cdrLocked;
               v.axiReadSlave.rdata(9)            := regIn.sigLocked;
               v.axiReadSlave.rdata(8)            := regIn.busyOut;
               v.axiReadSlave.rdata(7)            := regIn.dBitErrBc;
               v.axiReadSlave.rdata(6)            := regIn.sBitErrBc;
               v.axiReadSlave.rdata(5)            := regIn.deSerErr;
               v.axiReadSlave.rdata(4)            := regIn.bpmErr;
               v.axiReadSlave.rdata(3)            := regIn.bpmLocked;
               v.axiReadSlave.rdata(2)            := regIn.freqLocked;
               v.axiReadSlave.rdata(1)            := regIn.clkLocked;
               v.axiReadSlave.rdata(0)            := regIn.refClkLocked;
            when x"7C" =>
               v.axiReadSlave.rdata := regIn.freqMeasured;
            when x"7D" =>
               v.axiReadSlave.rdata := trigL1Rate;
            when x"7E" =>
               v.axiReadSlave.rdata := regIn.busyRateCnt;
            when x"7F" =>
               v.axiReadSlave.rdata := regIn.busyRate;
            when x"80" =>
               v.axiReadSlave.rdata(0) := r.regOut.forceBusy;
            when x"81" =>
               v.axiReadSlave.rdata(0) := r.regOut.emuSel;
            when x"82" =>
               v.axiReadSlave.rdata(7 downto 0) := r.regOut.ignoreExtBusyIn;
            when x"83" =>
               v.axiReadSlave.rdata(0) := r.regOut.ignoreSigLocked;
            when x"84" =>
               v.axiReadSlave.rdata(0) := r.regOut.ignoreCdrLocked;
            when x"85" =>
               v.axiReadSlave.rdata(0) := r.regOut.invertOutput;
            when x"86" =>
               v.axiReadSlave.rdata(0) := r.regOut.emuVersionSelect;
            when x"87" =>
               v.axiReadSlave.rdata(0) := r.regOut.clkSel;
            when x"F0" =>
               v.axiReadSlave.rdata(STATUS_SIZE_C-1 downto 0) := r.rollOverEn;
            when x"F1" =>
               v.axiReadSlave.rdata(STATUS_SIZE_C-1 downto 0) := r.irqEn;
            when others =>
               axiReadResp := AXI_ERROR_RESP_G;
         end case;

         -- Send AXI Response
         axiSlaveReadResponse(v.axiReadSlave, axiReadResp);
      end if;

      -- Synchronous Reset
      if axiRst = '1' then
         v                     := REG_INIT_C;
         v.regOut.delayIn.load := '1';
         v.regOut.delayIn.rst  := '1';
         v.regOut.delayIn.data := IDELAY_VALUE_G;
      end if;

      -- Register the variable for next clock cycle
      rin <= v;

      -- Outputs
      axiReadSlave  <= r.axiReadSlave;
      axiWriteSlave <= r.axiWriteSlave;
      regOut        <= r.regOut;
      cntRst        <= r.cntRst;
      rollOverEn    <= r.rollOverEn;
      irqEn         <= r.irqEn;

   end process comb;

   seq : process (axiClk) is
   begin
      if rising_edge(axiClk) then
         r <= rin after TPD_G;
      end if;
   end process seq;

   -------------------------------            
   -- Synchronization: Outputs
   -------------------------------     
   SyncOut_locRst : entity work.RstSync
      generic map (
         TPD_G => TPD_G)   
      port map (
         clk      => locClk,
         asyncRst => r.locReset,
         syncRst  => locReset); 

   locRst <= locReset;

   SyncOut_configBits : entity work.SynchronizerVector
      generic map (
         TPD_G   => TPD_G,
         WIDTH_G => 5)    
      port map (
         clk        => locClk,
         dataIn(0)  => regOut.forceBusy,
         dataIn(1)  => regOut.invertOutput,
         dataIn(2)  => regOut.serDataEdgeSel,
         dataIn(3)  => regOut.ignoreSigLocked,
         dataIn(4)  => regOut.ignoreCdrLocked,
         dataOut(0) => config.forceBusy,
         dataOut(1) => config.invertOutput,
         dataOut(2) => config.serDataEdgeSel,
         dataOut(3) => config.ignoreSigLocked,
         dataOut(4) => config.ignoreCdrLocked);  

   -- Don't sync these signals because locClk may not exist yet
   config.clkSel           <= regOut.clkSel;
   config.emuSel           <= regOut.emuSel;
   config.emuVersionSelect <= regOut.emuVersionSelect;

   SyncOut_ignoreExtBusyIn : entity work.SynchronizerVector
      generic map (
         TPD_G   => TPD_G,
         WIDTH_G => 8)    
      port map (
         clk     => locClk,
         dataIn  => regOut.ignoreExtBusyIn,
         dataOut => config.ignoreExtBusyIn);                 

   SyncOut_delayIn_data : entity work.SynchronizerFifo
      generic map (
         TPD_G        => TPD_G,
         DATA_WIDTH_G => 5)
      port map (
         wr_clk => axiClk,
         din    => regOut.delayIn.data,
         rd_clk => refClk200MHz,
         dout   => config.delayIn.data);    

   SyncOut_delayIn_load : entity work.RstSync
      generic map (
         TPD_G           => TPD_G,
         RELEASE_DELAY_G => 32)   
      port map (
         clk      => refClk200MHz,
         asyncRst => regOut.delayIn.load,
         syncRst  => config.delayIn.load); 

   SyncOut_delayIn_rst : entity work.RstSync
      generic map (
         TPD_G           => TPD_G,
         RELEASE_DELAY_G => 16)   
      port map (
         clk      => refClk200MHz,
         asyncRst => regOut.delayIn.rst,
         syncRst  => config.delayIn.rst);

   SyncOut_busyRateRst : entity work.RstSync
      generic map (
         TPD_G => TPD_G)   
      port map (
         clk      => locClk,
         asyncRst => cntRst,
         syncRst  => config.busyRateRst);           

   -------------------------------
   -- Synchronization: Inputs
   -------------------------------             
   SyncIn_freqMeasured : entity work.SynchronizerFifo
      generic map (
         TPD_G        => TPD_G,
         DATA_WIDTH_G => 32)
      port map (
         wr_clk => locClk,
         din    => status.freqMeasured,
         rd_clk => axiClk,
         dout   => regIn.freqMeasured);  

   SyncIn_busyRate : entity work.SynchronizerFifo
      generic map (
         TPD_G        => TPD_G,
         DATA_WIDTH_G => 32)
      port map (
         wr_clk => locClk,
         din    => status.busyRate,
         rd_clk => axiClk,
         dout   => regIn.busyRate);   

   SyncIn_busyRateCnt : entity work.SynchronizerFifo
      generic map (
         TPD_G        => TPD_G,
         DATA_WIDTH_G => 32)
      port map (
         wr_clk => locClk,
         din    => status.busyRateCnt,
         rd_clk => axiClk,
         dout   => regIn.busyRateCnt);            

   SyncIn_bc : entity work.SynchronizerFifo
      generic map (
         TPD_G        => TPD_G,
         DATA_WIDTH_G => 8)
      port map (
         wr_clk => locClk,
         din    => status.ttcRx.bc.cmdData,
         rd_clk => axiClk,
         dout   => regIn.ttcRx.bc.cmdData);            

   SyncIn_delayOut_data : entity work.SynchronizerFifo
      generic map (
         TPD_G        => TPD_G,
         DATA_WIDTH_G => 5)
      port map (
         wr_clk => refClk200MHz,
         din    => status.delayOut.data,
         rd_clk => axiClk,
         dout   => regIn.delayOut.data);  

   SyncIn_delayOut_rdy : entity work.Synchronizer
      generic map (
         TPD_G => TPD_G)
      port map (
         clk     => axiClk,
         dataIn  => status.delayOut.rdy,
         dataOut => regIn.delayOut.rdy);

   SyncStatusVec_Inst : entity work.SyncStatusVector
      generic map (
         TPD_G          => TPD_G,
         OUT_POLARITY_G => '1',
         CNT_RST_EDGE_G => true,
         CNT_WIDTH_G    => STATUS_CNT_WIDTH_G,
         WIDTH_G        => STATUS_SIZE_C)     
      port map (
         -- Input Status bit Signals (wrClk domain)
         statusIn(18 downto 11)  => status.busyIn,
         statusIn(10)            => status.cdrLocked,
         statusIn(9)             => status.sigLocked,
         statusIn(8)             => status.busyOut,
         statusIn(7)             => status.dBitErrBc,
         statusIn(6)             => status.sBitErrBc,
         statusIn(5)             => status.deSerErr,
         statusIn(4)             => status.bpmErr,
         statusIn(3)             => status.bpmLocked,
         statusIn(2)             => status.freqLocked,
         statusIn(1)             => status.clkLocked,
         statusIn(0)             => status.refClkLocked,
         -- Output Status bit Signals (rdClk domain)           
         statusOut(18 downto 11) => regIn.busyIn,
         statusOut(10)           => regIn.cdrLocked,
         statusOut(9)            => regIn.sigLocked,
         statusOut(8)            => regIn.busyOut,
         statusOut(7)            => regIn.dBitErrBc,
         statusOut(6)            => regIn.sBitErrBc,
         statusOut(5)            => regIn.deSerErr,
         statusOut(4)            => regIn.bpmErr,
         statusOut(3)            => regIn.bpmLocked,
         statusOut(2)            => regIn.freqLocked,
         statusOut(1)            => regIn.clkLocked,
         statusOut(0)            => regIn.refClkLocked,
         -- Status Bit Counters Signals (rdClk domain) 
         cntRstIn                => cntRst,
         rollOverEnIn            => rollOverEn,
         cntOut                  => cntOut,
         -- Interrupt Signals (rdClk domain) 
         irqEnIn                 => irqEn,
         irqOut                  => statusSend,
         -- Clocks and Reset Ports
         wrClk                   => locClk,
         rdClk                   => axiClk);

   busyInCnt7      <= muxSlVectorArray(cntOut, 18);
   busyInCnt6      <= muxSlVectorArray(cntOut, 17);
   busyInCnt5      <= muxSlVectorArray(cntOut, 16);
   busyInCnt4      <= muxSlVectorArray(cntOut, 15);
   busyInCnt3      <= muxSlVectorArray(cntOut, 14);
   busyInCnt2      <= muxSlVectorArray(cntOut, 13);
   busyInCnt1      <= muxSlVectorArray(cntOut, 12);
   busyInCnt0      <= muxSlVectorArray(cntOut, 11);
   cdrLockedCnt    <= muxSlVectorArray(cntOut, 10);
   sigLockedCnt    <= muxSlVectorArray(cntOut, 9);
   busyOutCnt      <= muxSlVectorArray(cntOut, 8);
   dBitErrBcCnt    <= muxSlVectorArray(cntOut, 7);
   sBitErrBcCnt    <= muxSlVectorArray(cntOut, 6);
   deSerErrCnt     <= muxSlVectorArray(cntOut, 5);
   bpmErrCnt       <= muxSlVectorArray(cntOut, 4);
   bpmLockedCnt    <= muxSlVectorArray(cntOut, 3);
   freqLockedCnt   <= muxSlVectorArray(cntOut, 2);
   clkLockedCnt    <= muxSlVectorArray(cntOut, 1);
   refClkLockedCnt <= muxSlVectorArray(cntOut, 0);

   statusWords(0)(31 downto STATUS_SIZE_C) <= (others => '0');  -- Spare

   statusWords(0)(18 downto 11) <= regIn.busyIn;
   statusWords(0)(10)           <= regIn.cdrLocked;
   statusWords(0)(9)            <= regIn.sigLocked;
   statusWords(0)(8)            <= regIn.busyOut;
   statusWords(0)(7)            <= regIn.dBitErrBc;
   statusWords(0)(6)            <= regIn.sBitErrBc;
   statusWords(0)(5)            <= regIn.deSerErr;
   statusWords(0)(4)            <= regIn.bpmErr;
   statusWords(0)(3)            <= regIn.bpmLocked;
   statusWords(0)(2)            <= regIn.freqLocked;
   statusWords(0)(1)            <= regIn.clkLocked;
   statusWords(0)(0)            <= regIn.refClkLocked;

   SyncTrigRate_Inst : entity work.SyncTrigRate
      generic map (
         TPD_G          => TPD_G,
         COMMON_CLK_G   => false,
         IN_POLARITY_G  => '1',
         REF_CLK_FREQ_G => 200.0E+6,
         REFRESH_RATE_G => 1.0E+0,
         USE_DSP48_G    => "no",
         CNT_WIDTH_G    => 32)     
      port map (
         -- Trigger Input (locClk domain)
         trigIn          => status.ttcRx.trigL1,
         -- Trigger Rate Output (locClk domain)
         trigRateUpdated => open,
         trigRateOut     => trigRate,
         -- Clocks
         locClkEn        => '1',
         locClk          => locClk,
         refClk          => refClk200MHz); 

   SyncIn_trigRate : entity work.SynchronizerFifo
      generic map (
         TPD_G        => TPD_G,
         DATA_WIDTH_G => 32)
      port map (
         wr_clk => locClk,
         din    => trigRate,
         rd_clk => axiClk,
         dout   => trigL1Rate);          

end rtl;
