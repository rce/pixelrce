-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : AtlasCscDpmAsmPackCore.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2014-06-03
-- Last update: 2015-03-05
-- Platform   : Vivado 2014.1
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description:  
-------------------------------------------------------------------------------
-- This file is part of 'ATLAS NRC CSC DEV'.
-- It is subject to the license terms in the LICENSE.txt file found in the 
-- top-level directory of this distribution and at: 
--    https://confluence.slac.stanford.edu/display/ppareg/LICENSE.html. 
-- No part of 'ATLAS NRC CSC DEV', including this file, 
-- may be copied, modified, propagated, or distributed except according to 
-- the terms contained in the LICENSE.txt file.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

use work.StdRtlPkg.all;
use work.AxiLitePkg.all;
use work.AxiStreamPkg.all;
use work.AtlasTtcRxPkg.all;

library unisim;
use unisim.vcomponents.all;

entity AtlasCscDpmAsmPackCore is
   generic (
      TPD_G              : time             := 1 ns;
      AXIL_BASEADDR_G    : slv(31 downto 0) := X"A0000000";
      AXI_ERROR_RESP_G   : slv(1 downto 0)  := AXI_RESP_OK_C;
      CASCADE_SIZE_G     : positive         := 1;
      FIFO_FIXED_THES_G  : boolean          := false;
      FIFO_AFULL_THRES_G : positive         := 256); 
   port (
      -- External Axi Bus, 0xA0000000 - 0xAFFFFFFF
      axiClk         : in  sl;
      axiRst         : in  sl;
      axiReadMaster  : in  AxiLiteReadMasterType;
      axiReadSlave   : out AxiLiteReadSlaveType;
      axiWriteMaster : in  AxiLiteWriteMasterType;
      axiWriteSlave  : out AxiLiteWriteSlaveType;
      statusSend     : out slv(3 downto 0);
      statusWords    : out Slv64Array(0 to 3);
      -- FEX Streaming Interface
      mAxisClk       : in  sl;          -- 50 MHz
      mAxisRst       : in  sl;
      mAxisMaster    : out AxiStreamMasterType;
      mAxisSlave     : in  AxiStreamSlaveType;
      -- ROL Streaming Interface
      sAxisClk       : in  sl;          -- 100 MHz
      sAxisRst       : in  sl;
      sAxisMaster    : in  AxiStreamMasterType;
      sAxisSlave     : out AxiStreamSlaveType;
      -- RTM High Speed
      dpmToRtmHsP    : out slv(11 downto 0);
      dpmToRtmHsM    : out slv(11 downto 0);
      rtmToDpmHsP    : in  slv(11 downto 0);
      rtmToDpmHsM    : in  slv(11 downto 0);
      -- DTM Signals
      dtmRefClkP     : in  sl;
      dtmRefClkM     : in  sl;
      dtmClkP        : in  slv(1 downto 0);
      dtmClkM        : in  slv(1 downto 0);
      dtmFbP         : out sl;
      dtmFbM         : out sl;
      -- Reference clocks
      sysClk200      : in  sl;
      sysClk200Rst   : in  sl;
      sysClk125      : in  sl;
      sysClk125Rst   : in  sl;
      -- Debug
      led            : out slv(1 downto 0));      
end AtlasCscDpmAsmPackCore;

architecture rtl of AtlasCscDpmAsmPackCore is

   constant NUM_AXI_MASTERS_C : natural := 4;

   constant TRIG_AXI_INDEX_C : natural := 0;
   constant SCA_AXI_INDEX_C  : natural := 1;
   constant ROL_AXI_INDEX_C  : natural := 3;
   constant FEX_AXI_INDEX_C  : natural := 2;

   constant TRIG_BASE_ADDR_C : slv(31 downto 0) := (AXIL_BASEADDR_G + X"00000000");
   constant SCA_BASE_ADDR_C  : slv(31 downto 0) := (AXIL_BASEADDR_G + X"00001000");
   constant ROL_BASE_ADDR_C  : slv(31 downto 0) := (AXIL_BASEADDR_G + X"00002000");
   constant FEX_BASE_ADDR_C  : slv(31 downto 0) := (AXIL_BASEADDR_G + X"00004000");

   constant AXI_CROSSBAR_MASTERS_CONFIG_C : AxiLiteCrossbarMasterConfigArray(NUM_AXI_MASTERS_C-1 downto 0) := (
      TRIG_AXI_INDEX_C => (
         baseAddr      => TRIG_BASE_ADDR_C,
         addrBits      => 12,
         connectivity  => X"0003"),
      SCA_AXI_INDEX_C  => (
         baseAddr      => SCA_BASE_ADDR_C,
         addrBits      => 12,
         connectivity  => X"0003"),
      ROL_AXI_INDEX_C  => (
         baseAddr      => ROL_BASE_ADDR_C,
         addrBits      => 12,
         connectivity  => X"0003"),
      FEX_AXI_INDEX_C  => (
         baseAddr      => FEX_BASE_ADDR_C,
         addrBits      => 14,
         connectivity  => X"0003"));

   signal mAxiWriteMasters : AxiLiteWriteMasterArray(NUM_AXI_MASTERS_C-1 downto 0);
   signal mAxiWriteSlaves  : AxiLiteWriteSlaveArray(NUM_AXI_MASTERS_C-1 downto 0);
   signal mAxiReadMasters  : AxiLiteReadMasterArray(NUM_AXI_MASTERS_C-1 downto 0);
   signal mAxiReadSlaves   : AxiLiteReadSlaveArray(NUM_AXI_MASTERS_C-1 downto 0);

   signal sysClkLocked,
      atlasClk40MHz,
      atlasClk160MHz,
      atlasClk160MHzEn,
      atlasClk160MHzRst,
      scaWclk,
      scaBusy,
      gtCalTxP,
      gtCalTxN,
      gtCalRxP,
      gtCalRxN,
      cdrHeartbeat,
      toggleL1Trig : sl;

   signal ttcRx : AtlasTTCRxOutType;

   signal axisMaster : AxiStreamMasterType;
   signal axisSlave  : AxiStreamSlaveType;

   signal gtScaTxP,
      gtScaTxN,
      gtScaRxP,
      gtScaRxN : SlVectorArray(0 to 4, 0 to 1);

   -- attribute KEEP_HIERARCHY : string;
   -- attribute KEEP_HIERARCHY of
   -- U_AtlasTtcRx,
   -- U_AsmPackCtrl,
   -- U_AtlasAsmPackFex,
   -- AtlasSLinkLsc_Inst : label is "TRUE";
   
begin

   -- AXI-Lite Crossbar
   AxiLiteCrossbar_Inst : entity work.AxiLiteCrossbar
      generic map (
         NUM_SLAVE_SLOTS_G  => 1,
         NUM_MASTER_SLOTS_G => NUM_AXI_MASTERS_C,
         DEC_ERROR_RESP_G   => AXI_ERROR_RESP_G,
         MASTERS_CONFIG_G   => AXI_CROSSBAR_MASTERS_CONFIG_C)
      port map (
         axiClk              => axiClk,
         axiClkRst           => axiRst,
         sAxiWriteMasters(0) => axiWriteMaster,
         sAxiWriteSlaves(0)  => axiWriteSlave,
         sAxiReadMasters(0)  => axiReadMaster,
         sAxiReadSlaves(0)   => axiReadSlave,
         mAxiWriteMasters    => mAxiWriteMasters,
         mAxiWriteSlaves     => mAxiWriteSlaves,
         mAxiReadMasters     => mAxiReadMasters,
         mAxiReadSlaves      => mAxiReadSlaves);             

   -- TTC-RX Module
   U_AtlasTtcRx : entity work.AtlasTtcRx
      generic map (
         TPD_G              => TPD_G,
         AXI_ERROR_RESP_G   => AXI_ERROR_RESP_G,
         IDELAY_VALUE_G     => toSlv(0, 5),
         STATUS_CNT_WIDTH_G => 32,
         IODELAY_GROUP_G    => "atlas_ttc_rx_delay_group",
         CASCADE_SIZE_G     => 1)      
      port map (
         -- External CDR Ports
         clkP              => dtmClkP(0),
         clkN              => dtmClkM(0),
         dataP             => dtmClkP(1),
         dataN             => dtmClkM(1),
         -- RMB Status Signals
         busyIn            => scaBusy,
         busyP             => dtmFbP,
         busyN             => dtmFbM,
         -- AXI-Lite Register and Status Bus Interface (axiClk domain)
         axiClk            => axiClk,
         axiRst            => axiRst,
         axiReadMaster     => mAxiReadMasters(TRIG_AXI_INDEX_C),
         axiReadSlave      => mAxiReadSlaves(TRIG_AXI_INDEX_C),
         axiWriteMaster    => mAxiWriteMasters(TRIG_AXI_INDEX_C),
         axiWriteSlave     => mAxiWriteSlaves(TRIG_AXI_INDEX_C),
         statusWords(0)    => statusWords(0),
         statusSend        => statusSend(0),
         -- Reference 200 MHz clock
         refClk200MHz      => sysClk200,
         refClkLocked      => sysClkLocked,
         -- Atlas Clocks and trigger interface  (atlasClk160MHz domain)
         atlasTtcRxOut     => ttcRx,
         atlasClk40MHz     => atlasClk40MHz,
         atlasClk160MHz    => atlasClk160MHz,
         atlasClk160MHzEn  => atlasClk160MHzEn,
         atlasClk160MHzRst => atlasClk160MHzRst);   

   -- Create the locked status signal
   sysClkLocked <= not(sysClk200Rst);

   -- SCA Controller 
   U_AsmPackCtrl : entity work.AsmPackCtrl
      generic map (
         TPD_G              => TPD_G,
         AXI_ERROR_RESP_G   => AXI_ERROR_RESP_G,
         STATUS_CNT_WIDTH_G => 32)
      port map (
         -- Trigger Interface (atlasClk160MHz domain)
         ttcRx             => ttcRx,
         busy              => scaBusy,
         -- AXI-Lite Register Interface (axiClk domain)
         axiClk            => axiClk,
         axiRst            => axiRst,
         axiReadMaster     => mAxiReadMasters(SCA_AXI_INDEX_C),
         axiReadSlave      => mAxiReadSlaves(SCA_AXI_INDEX_C),
         axiWriteMaster    => mAxiWriteMasters(SCA_AXI_INDEX_C),
         axiWriteSlave     => mAxiWriteSlaves(SCA_AXI_INDEX_C),
         statusWords(0)    => statusWords(1),
         statusSend        => statusSend(1),
         -- Streaming TX Data Interface (mAxisClk domain) 
         mAxisClk          => mAxisClk,
         mAxisRst          => mAxisRst,
         mAxisMaster       => axisMaster,
         mAxisSlave        => axisSlave,
         -- GTX7 MGT I/O
         gtScaTxP          => gtScaTxP,
         gtScaTxN          => gtScaTxN,
         gtScaRxP          => gtScaRxP,
         gtScaRxN          => gtScaRxN,
         gtCalTxP          => gtCalTxP,
         gtCalTxN          => gtCalTxN,
         gtCalRxP          => gtCalRxP,
         gtCalRxN          => gtCalRxN,
         gtClkP            => dtmRefClkP,
         gtClkN            => dtmRefClkM,
         -- Global Signals
         stableClk         => sysClk125,
         atlasClk40MHz     => atlasClk40MHz,
         atlasClk160MHz    => atlasClk160MHz,
         atlasClk160MHzEn  => atlasClk160MHzEn,
         atlasClk160MHzRst => atlasClk160MHzRst);     

   -- ASM-II[0,0] = MGT[0]
   dpmToRtmHsP(0)  <= gtScaTxP(0, 0);
   dpmToRtmHsM(0)  <= gtScaTxN(0, 0);
   gtScaRxP(0, 0)  <= rtmToDpmHsP(0);
   gtScaRxN(0, 0)  <= rtmToDpmHsM(0);
   -- ASM-II[0,1] = MGT[2]
   dpmToRtmHsP(2)  <= gtScaTxP(0, 1);
   dpmToRtmHsM(2)  <= gtScaTxN(0, 1);
   gtScaRxP(0, 1)  <= rtmToDpmHsP(2);
   gtScaRxN(0, 1)  <= rtmToDpmHsM(2);
   -- ASM-II[1,0] = MGT[1]
   dpmToRtmHsP(1)  <= gtScaTxP(1, 0);
   dpmToRtmHsM(1)  <= gtScaTxN(1, 0);
   gtScaRxP(1, 0)  <= rtmToDpmHsP(1);
   gtScaRxN(1, 0)  <= rtmToDpmHsM(1);
   -- ASM-II[1,1] = MGT[3]
   dpmToRtmHsP(3)  <= gtScaTxP(1, 1);
   dpmToRtmHsM(3)  <= gtScaTxN(1, 1);
   gtScaRxP(1, 1)  <= rtmToDpmHsP(3);
   gtScaRxN(1, 1)  <= rtmToDpmHsM(3);
   -- ASM-II[2,0] = MGT[4]
   dpmToRtmHsP(4)  <= gtScaTxP(2, 0);
   dpmToRtmHsM(4)  <= gtScaTxN(2, 0);
   gtScaRxP(2, 0)  <= rtmToDpmHsP(4);
   gtScaRxN(2, 0)  <= rtmToDpmHsM(4);
   -- ASM-II[2,1] = MGT[5]
   dpmToRtmHsP(5)  <= gtScaTxP(2, 1);
   dpmToRtmHsM(5)  <= gtScaTxN(2, 1);
   gtScaRxP(2, 1)  <= rtmToDpmHsP(5);
   gtScaRxN(2, 1)  <= rtmToDpmHsM(5);
   -- ASM-II[3,0] = MGT[8]
   dpmToRtmHsP(8)  <= gtScaTxP(3, 0);
   dpmToRtmHsM(8)  <= gtScaTxN(3, 0);
   gtScaRxP(3, 0)  <= rtmToDpmHsP(8);
   gtScaRxN(3, 0)  <= rtmToDpmHsM(8);
   -- ASM-II[3,1] = MGT[10]
   dpmToRtmHsP(10) <= gtScaTxP(3, 1);
   dpmToRtmHsM(10) <= gtScaTxN(3, 1);
   gtScaRxP(3, 1)  <= rtmToDpmHsP(10);
   gtScaRxN(3, 1)  <= rtmToDpmHsM(10);
   -- ASM-II[4,0] = MGT[9]
   dpmToRtmHsP(9)  <= gtScaTxP(4, 0);
   dpmToRtmHsM(9)  <= gtScaTxN(4, 0);
   gtScaRxP(4, 0)  <= rtmToDpmHsP(9);
   gtScaRxN(4, 0)  <= rtmToDpmHsM(9);
   -- ASM-II[4,1] = MGT[11]
   dpmToRtmHsP(11) <= gtScaTxP(4, 1);
   dpmToRtmHsM(11) <= gtScaTxN(4, 1);
   gtScaRxP(4, 1)  <= rtmToDpmHsP(11);
   gtScaRxN(4, 1)  <= rtmToDpmHsM(11);
   -- Calibration Trigger = MGT[6]
   dpmToRtmHsP(6)  <= gtCalTxP;
   dpmToRtmHsM(6)  <= gtCalTxN;
   gtCalRxP        <= rtmToDpmHsP(6);
   gtCalRxN        <= rtmToDpmHsM(6);

   AtlasAsmPackFex_Inst : entity work.AtlasAsmPackFex
      generic map(
         TPD_G              => TPD_G,
         FORCE_DBG_HDR_G    => true,
         PPI_NOT_SSI_G      => true,
         QUEUE_SIZE_G       => 4,
         CASCADE_SIZE_G     => CASCADE_SIZE_G,
         STATUS_CNT_WIDTH_G => 32,
         AXI_ERROR_RESP_G   => AXI_ERROR_RESP_G)
      port map (
         -- AXI-Lite Register Interface  (mAxisClk domain) 
         axiClk         => axiClk,
         axiRst         => axiRst,
         axiReadMaster  => mAxiReadMasters(FEX_AXI_INDEX_C),
         axiReadSlave   => mAxiReadSlaves(FEX_AXI_INDEX_C),
         axiWriteMaster => mAxiWriteMasters(FEX_AXI_INDEX_C),
         axiWriteSlave  => mAxiWriteSlaves(FEX_AXI_INDEX_C),
         statusWords(0) => statusWords(2),
         statusSend     => statusSend(2),
         -- RAW Data Interface  (mAxisClk domain) 
         sAxisMaster    => axisMaster,
         sAxisSlave     => axisSlave,
         -- Processed Data Interface  (mAxisClk domain) 
         mAxisMaster    => mAxisMaster,
         mAxisSlave     => mAxisSlave,
         -- ATLAS Timing/Trigger
         ecrDet         => ttcRx.ecrDet,
         -- Processor's Clock and Reset
         axisClk        => mAxisClk,
         axisRst        => mAxisRst);  

   AtlasSLinkLsc_Inst : entity work.AtlasSLinkLsc
      generic map (
         TPD_G              => TPD_G,
         PPI_NOT_SSI_G      => true,
         CASCADE_SIZE_G     => CASCADE_SIZE_G,
         STATUS_CNT_WIDTH_G => 32,
         AXI_ERROR_RESP_G   => AXI_ERROR_RESP_G)
      port map (
         -- AXI-Lite Register Interface (axiClk domain)
         axiClk         => axiClk,
         axiRst         => axiRst,
         axiReadMaster  => mAxiReadMasters(ROL_AXI_INDEX_C),
         axiReadSlave   => mAxiReadSlaves(ROL_AXI_INDEX_C),
         axiWriteMaster => mAxiWriteMasters(ROL_AXI_INDEX_C),
         axiWriteSlave  => mAxiWriteSlaves(ROL_AXI_INDEX_C),
         statusWords(0) => statusWords(3),
         statusSend     => statusSend(3),
         -- Streaming RX Data Interface (sAxisClk domain) 
         sAxisClk       => sAxisClk,
         sAxisRst       => sAxisRst,
         sAxisMaster    => sAxisMaster,
         sAxisSlave     => sAxisSlave,
         -- Reference 100 MHz clock and reset
         sysClk         => sAxisClk,
         sysRst         => sAxisRst,
         -- G-Link's MGT Serial IO
         gtTxP          => dpmToRtmHsP(7),
         gtTxN          => dpmToRtmHsM(7),
         gtRxP          => rtmToDpmHsP(7),
         gtRxN          => rtmToDpmHsM(7));

   -- Debug
   led(1) <= cdrHeartbeat;
   led(0) <= toggleL1Trig;

   U_Heartbeat : entity work.Heartbeat
      generic map (
         TPD_G        => TPD_G,
         USE_DSP48_G  => "no",
         PERIOD_IN_G  => getRealDiv(1, ATLAS_TTC_RX_CDR_CLK_FREQ_C),  --units of seconds
         PERIOD_OUT_G => 1.0E+0)                                      --units of seconds   
      port map(
         clk => atlasClk160MHz,
         o   => cdrHeartbeat);

   process(atlasClk160MHz)
   begin
      if rising_edge(atlasClk160MHz) then
         if ttcRx.trigL1 = '1' then
            -- Toggle the LED every Level-1 Trigger
            if toggleL1Trig = '1' then
               toggleL1Trig <= '0' after TPD_G;
            else
               toggleL1Trig <= '1' after TPD_G;
            end if;
         end if;
      end if;
   end process;

end architecture rtl;
