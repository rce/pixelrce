-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : AtlasCscDpmClk.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2014-06-03
-- Last update: 2016-11-29
-- Platform   : Vivado 2014.1
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description:  
-------------------------------------------------------------------------------
-- This file is part of 'ATLAS NRC CSC DEV'.
-- It is subject to the license terms in the LICENSE.txt file found in the 
-- top-level directory of this distribution and at: 
--    https://confluence.slac.stanford.edu/display/ppareg/LICENSE.html. 
-- No part of 'ATLAS NRC CSC DEV', including this file, 
-- may be copied, modified, propagated, or distributed except according to 
-- the terms contained in the LICENSE.txt file.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

use work.StdRtlPkg.all;

library unisim;
use unisim.vcomponents.all;

entity AtlasCscDpmClk is
   port (
      -- 250 DPM Reference Clock
      gtClkP       : in    sl;
      gtClkN       : in    sl;   
      -- Output Clocks and Resets
      stableClk125 : out sl;
      sysClk200Clk : out sl;
      sysClk200Rst : out sl;
      sysClk100Clk : out sl;
      sysClk100Rst : out sl;      
      sysClk50Clk  : out sl;
      sysClk50Rst  : out sl);      
end AtlasCscDpmClk;

architecture mapping of AtlasCscDpmClk is

   signal gtClkDiv2,
      stableClk,
      stableRst,
      clkOut0,
      clkOut1,
      clkOut2,
      clkFbIn,
      clkFbOut,
      refClk200,
      refClk100,
      refClk50,
      locked : sl := '0';
   
begin

   -- GT Reference Clock
   IBUFDS_GTE2_Inst : IBUFDS_GTE2
      port map (
         I     => gtClkP,-- 250 MHz
         IB    => gtClkN,
         CEB   => '0',
         ODIV2 => gtClkDiv2,
         O     => open);

   BUFG_Inst : BUFG
      port map (
         I => gtClkDiv2,
         O => stableClk);-- 125 MHz
         
   stableClk125<=stableClk;
   PwrUpRst_Inst : entity work.PwrUpRst
      generic map(
         DURATION_G => getTimeRatio(125.0E6,1.0E+3))-- 1 ms power up reset
      port map (
         clk    => stableClk,
         rstOut => stableRst);         
       
   MMCME2_ADV_Inst : MMCME2_ADV
      generic map(
         BANDWIDTH            => "OPTIMIZED",
         CLKOUT4_CASCADE      => FALSE,
         COMPENSATION         => "ZHOLD",
         STARTUP_WAIT         => FALSE,
         DIVCLK_DIVIDE        => 1,
         CLKFBOUT_MULT_F      => 8.000,
         CLKFBOUT_PHASE       => 0.000,
         CLKFBOUT_USE_FINE_PS => FALSE,
         CLKOUT0_DIVIDE_F     => 5.000,
         CLKOUT0_PHASE        => 0.000,
         CLKOUT0_DUTY_CYCLE   => 0.500,
         CLKOUT0_USE_FINE_PS  => FALSE,
         CLKOUT1_DIVIDE       => 10,
         CLKOUT1_PHASE        => 0.000,
         CLKOUT1_DUTY_CYCLE   => 0.500,
         CLKOUT1_USE_FINE_PS  => FALSE,
         CLKOUT2_DIVIDE       => 20,
         CLKOUT2_PHASE        => 0.000,
         CLKOUT2_DUTY_CYCLE   => 0.500,
         CLKOUT2_USE_FINE_PS  => FALSE,
         CLKIN1_PERIOD        => 8.0,
         REF_JITTER1          => 0.010)
      port map (
         -- Output clocks
         CLKFBOUT     => clkFbOut,
         CLKFBOUTB    => open,
         CLKOUT0      => clkOut0,-- 200 MHz
         CLKOUT0B     => open,
         CLKOUT1      => clkOut1,-- 100 MHz
         CLKOUT1B     => open,
         CLKOUT2      => clkOut2,-- 50 MHz
         CLKOUT2B     => open,
         CLKOUT3      => open,
         CLKOUT3B     => open,
         CLKOUT4      => open,
         CLKOUT5      => open,
         CLKOUT6      => open,
         -- Input clock control
         CLKFBIN      => clkFbIn,
         CLKIN1       => stableClk,
         CLKIN2       => '0',
         CLKINSEL     => '1',
         -- Ports for dynamic reconfiguration
         DADDR        => (others => '0'),
         DCLK         => '0',
         DEN          => '0',
         DI           => (others => '0'),
         DO           => open,
         DRDY         => open,
         DWE          => '0',
         -- Ports for dynamic phase shift
         PSCLK        => '0',
         PSEN         => '0',
         PSINCDEC     => '0',
         PSDONE       => open,
         -- Other control and status signals
         LOCKED       => locked,
         CLKINSTOPPED => open,
         CLKFBSTOPPED => open,
         PWRDWN       => '0',
         RST          => stableRst);   

   ClkFbBuf : BUFG
      port map(
         I => clkFbOut,
         O => clkFbIn);
         
   ------------------------------------------------
   sysClk200Clk <= refClk200;
        
   Clk200_BUFG : BUFG
      port map(
         I => clkOut0,
         O => refClk200);         

   RstSync_200 : entity work.RstSync
      generic map (
         IN_POLARITY_G => '0')   
      port map (
         clk      => refClk200,
         asyncRst => locked,
         syncRst  => sysClk200Rst); 

   ------------------------------------------------
   sysClk100Clk <= refClk100;
        
   Clk100_BUFG : BUFG
      port map(
         I => clkOut1,
         O => refClk100);         
         
   RstSync_100 : entity work.RstSync
      generic map (
         IN_POLARITY_G => '0')   
      port map (
         clk      => refClk100,
         asyncRst => locked,
         syncRst  => sysClk100Rst); 

   ------------------------------------------------
   sysClk50Clk <= refClk50;
        
   Clk50_BUFG : BUFG
      port map(
         I => clkOut2,
         O => refClk50);         
         
   RstSync_50 : entity work.RstSync
      generic map (
         IN_POLARITY_G => '0')   
      port map (
         clk      => refClk50,
         asyncRst => locked,
         syncRst  => sysClk50Rst);          

end architecture mapping;
