/dts-v1/;

/ {
	model = "Xilinx Zynq RCE";
	compatible = "xlnx,zynq-rce";
	#address-cells = <0x1>;
	#size-cells = <0x1>;
	interrupt-parent = <0x1>;

	memory {
		device_type = "memory";
		reg = <0x0 0x40000000>;
	};

	chosen {
		bootargs = "console=ttyPS0,115200 root=/dev/mmcblk0p3 rw ip=:::::eth0:dhcp earlyprintk";
		linux,stdout-path = "/amba@0/uart@E0001000";
	};

	soc {
		compatible = "xlnx,zynq";
		clock-frequency = <0x1fca055>;
	};

	amba@0 {
		compatible = "simple-bus";
		#address-cells = <0x1>;
		#size-cells = <0x1>;
		ranges;

		axi_stream_dma_0@60000000 {
			compatible = "axi_stream_dma";
			reg = <0x60000000 0x20000>;
			interrupts = <0x0 56 0x4>;
			interrupt-parent = <0x1>;
		};

		axi_stream_dma_1@60020000 {
			compatible = "axi_stream_dma";
			reg = <0x60020000 0x20000>;
			interrupts = <0x0 57 0x4>;
			interrupt-parent = <0x1>;
		};

		axi_stream_dma_2@60040000 {
			compatible = "axi_stream_dma";
			reg = <0x60040000 0x20000>;
			interrupts = <0x0 58 0x4>;
			interrupt-parent = <0x1>;
		};

		axi_stream_dma_3@60060000 {
			compatible = "axi_stream_dma";
			reg = <0x60060000 0x20000>;
			interrupts = <0x0 59 0x4>;
			interrupt-parent = <0x1>;
		};

		intc@f8f01000 {
			interrupt-controller;
			compatible = "arm,cortex-a9-gic";
			#interrupt-cells = <0x3>;
			reg = <0xf8f01000 0x1000 0xf8f00100 0x100>;
			linux,phandle = <0x1>;
			phandle = <0x1>;
		};

		pl310@f8f02000 {
			compatible = "arm,pl310-cache";
			cache-unified;
			cache-level = <0x2>;
			reg = <0xf8f02000 0x1000>;
			arm,data-latency = <0x3 0x2 0x2>;
			arm,tag-latency = <0x2 0x2 0x2>;
		};

		ps7-ddrc@f8006000 {
			compatible = "xlnx,ps7-ddrc-1.00.a", "xlnx,ps7-ddrc";
			reg = <0xf8006000 0x1000>;
			xlnx,has-ecc = <0x0>;
		};

		uart@e0001000 {
			compatible = "xlnx,ps7-uart-1.00.a";
			reg = <0xe0001000 0x1000>;
			interrupts = <0x0 0x32 0x4>;
			interrupt-parent = <0x1>;
			clock = <0x2faf080>;
		};

		slcr@f8000000 {
			compatible = "xlnx,zynq-slcr";
			reg = <0xf8000000 0x1000>;

			clocks {
				#address-cells = <0x1>;
				#size-cells = <0x0>;

				armpll {
					#clock-cells = <0x0>;
					clock-output-names = "armpll";
					clocks = <0x2>;
					compatible = "xlnx,zynq-pll";
					lockbit = <0x0>;
					reg = <0x100 0x110 0x10c>;
				};

				ddrpll {
					#clock-cells = <0x0>;
					clock-output-names = "ddrpll";
					clocks = <0x2>;
					compatible = "xlnx,zynq-pll";
					lockbit = <0x1>;
					reg = <0x104 0x114 0x10c>;
				};

				iopll {
					#clock-cells = <0x0>;
					clock-output-names = "iopll";
					clocks = <0x2>;
					compatible = "xlnx,zynq-pll";
					lockbit = <0x2>;
					reg = <0x108 0x118 0x10c>;
				};

				ps_clk {
					#clock-cells = <0x0>;
					clock-frequency = <0x1fca055>;
					clock-output-names = "ps_clk";
					compatible = "fixed-clock";
					linux,phandle = <0x2>;
					phandle = <0x2>;
				};
			};
		};

		timer@0xf8001000 {
			compatible = "xlnx,ps7-ttc-1.00.a";
			reg = <0xf8001000 0x1000>;
			interrupts = <0x0 0xa 0x4 0x0 0xb 0x4 0x0 0xc 0x4>;
			interrupt-parent = <0x1>;
		};

		timer@f8f00600 {
			compatible = "arm,cortex-a9-twd-timer";
			reg = <0xf8f00600 0x20>;
			interrupts = <0x1 0xd 0x301>;
			interrupt-parent = <0x1>;
		};

		swdt@f8005000 {
			device_type = "watchdog";
			compatible = "xlnx,ps7-wdt-1.00.a";
			reg = <0xf8005000 0x100>;
			interrupts = <0x0 0x9 0x4>;
			interrupt-parent = <0x1>;
			reset = <0x0>;
			timeout = <0xa>;
		};

		scuwdt@f8f00620 {
			device_type = "watchdog";
			compatible = "arm,mpcore_wdt";
			reg = <0xf8f00620 0x20>;
			clock-frequency = <0x13de4355>;
			reset = <0x1>;
		};

		eth@e000b000 {
			compatible = "xlnx,ps7-ethernet-1.00.a";
			reg = <0xe000b000 0x1000>;
			interrupts = <0x0 0x16 0x4>;
			interrupt-parent = <0x1>;
			phy-handle = <0x3>;
			phy-mode = "rgmii-id";
			xlnx,ptp-enet-clock = <0x69f6bc7>;
			xlnx,slcr-div0-1000Mbps = <0x8>;
			xlnx,slcr-div0-100Mbps = <0x8>;
			xlnx,slcr-div0-10Mbps = <0x8>;
			xlnx,slcr-div1-1000Mbps = <0x1>;
			xlnx,slcr-div1-100Mbps = <0x5>;
			xlnx,slcr-div1-10Mbps = <0x32>;
			#address-cells = <0x1>;
			#size-cells = <0x0>;

			mdio {
				#address-cells = <0x1>;
				#size-cells = <0x0>;

				phy@7 {
					compatible = "marvell,88e1116r";
					device_type = "ethernet-phy";
					reg = <0x7>;
					linux,phandle = <0x3>;
					phandle = <0x3>;
				};
			};
		};

		eth@e000c000 {
			compatible = "xlnx,ps7-ethernet-1.00.a";
			reg = <0xe000c000 0x1000>;
			interrupts = <0x0 0x2d 0x4>;
			interrupt-parent = <0x1>;
			phy-handle = <0x4>;
			phy-mode = "rgmii-id";
			xlnx,ptp-enet-clock = <0x69f6bc7>;
			xlnx,slcr-div0-1000Mbps = <0x8>;
			xlnx,slcr-div0-100Mbps = <0x8>;
			xlnx,slcr-div0-10Mbps = <0x8>;
			xlnx,slcr-div1-1000Mbps = <0x1>;
			xlnx,slcr-div1-100Mbps = <0x5>;
			xlnx,slcr-div1-10Mbps = <0x32>;
			#address-cells = <0x1>;
			#size-cells = <0x0>;

			mdio {
				#address-cells = <0x1>;
				#size-cells = <0x0>;

				phy@7 {
					compatible = "marvell,88e1116r";
					device_type = "ethernet-phy";
					reg = <0x7>;
					linux,phandle = <0x4>;
					phandle = <0x4>;
				};
			};
		};

		i2c@e0004000 {
			compatible = "xlnx,ps7-i2c-1.00.a";
			reg = <0xe0004000 0x1000>;
			interrupts = <0x0 0x19 0x4>;
			interrupt-parent = <0x1>;
			bus-id = <0x0>;
			input-clk = <0x69f6bc7>;
			i2c-clk = <0x186a0>;
		};

		sdhci@e0100000 {
			compatible = "xlnx,ps7-sdhci-1.00.a";
			reg = <0xe0100000 0x1000>;
			xlnx,has-cd = <0x1>;
			interrupts = <0x0 0x18 0x4>;
			interrupt-parent = <0x1>;
			clock-frequency = <0x1fc9f08>;
		};

		usb@e0002000 {
			compatible = "xlnx,ps7-usb-1.00.a";
			reg = <0xe0002000 0x1000>;
			interrupts = <0x0 0x15 0x4>;
			interrupt-parent = <0x1>;
			dr_mode = "host";
			phy_type = "ulpi";
		};

		gpio@e000a000 {
			compatible = "xlnx,ps7-gpio-1.00.a";
			reg = <0xe000a000 0x1000>;
			interrupts = <0x0 0x14 0x4>;
			interrupt-parent = <0x1>;
		};
	};
};
